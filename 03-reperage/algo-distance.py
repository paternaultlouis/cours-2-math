from collections import namedtuple
from math import *

Point = namedtuple("Point", ["x", "y"])

def distance(A, B):
    return sqrt((A.x-B.x)**2 + (A.y-B.y)**2)

def isocèle(A, B, C):
    return distance(A, B) == distance(A, C)

A = Point(4, 6)
B = Point(1, 2)
C = Point(7, 2)

print(isocèle(A, B, C))
