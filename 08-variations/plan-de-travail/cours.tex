%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2018 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename
\documentclass[10pt]{article}


\usepackage{1920-pablo}
\usepackage{1920-pablo-cours}
\usepackage{1920-pablo-paternault}
\usepackage[a5paper, margin=.5cm]{geometry}

\begin{document}
\begin{center}
  \textsc{Variations de fonctions ---  Cours}
\end{center}

\section{Variations}

\begin{activite}
 Lire la partie \emph{a. Idée intuitive} de la page 64 du manuel.
 \end{activite}

\begin{definition}
  Soit $I$ un intervalle de $\mathbb R$, et $f$ une fonction définie sur $I$.
  \begin{itemize}
    \item $f$ est dite \emph{croissante} sur $I$ si pour tous réels $a$ et $b$ de $I$ tels que $a<b$, alors $f(a)\leq f(b)$.
    \item $f$ est dite \emph{strictement   croissante} sur $I$ si pour tous réels $a$ et $b$ de $I$ tels que $a<b$, alors $f(a)<f(b)$.
    \item $f$ est dite \emph{décroissante} sur $I$ si pour tous réels $a$ et $b$ de $I$ tels que $a<b$, alors $f(a)\geq f(b)$.
    \item $f$ est dite \emph{strictement décroissante} sur $I$ si pour tous réels $a$ et $b$ de $I$ tels que $a<b$, alors $f(a)>f(b)$.
  \end{itemize}

\begin{center}
  \begin{tabular}{p{.4\textwidth}|p{.4\textwidth}}
  \begin{tikzpicture}[very thick, yscale=.5]
    \draw[dotted,color=gray] (-.5,0) grid (4,4);
    \draw[] (-.5,0) -- (4,0);
    \draw[] (0,0) -- (0,4);
    \draw (0, 0) node[below left]{$\mathcal{O}$};
    \draw[->] (0,0) -- (1,0) node[midway,below]{$\vecteur\imath$};
    \draw[->] (0,0) -- (0,1) node[midway,left]{$\vecteur\jmath$};
    \draw[domain=-.5:4,smooth,blue] plot ({\x},{pow(\x+1, 2)/8+1});
    \draw[dashed] (1.7, 0) node[below]{$a$} -- ++(0, {pow(1.7+1, 2)/8+1}) -- ++(-1.7, 0) node[left]{$f(a)$};
    \draw[dashed] (2.4, 0) node[below]{$b$} -- ++(0, {pow(2.4+1, 2)/8+1}) -- ++(-2.4, 0) node[left]{$f(b)$};
  \end{tikzpicture}
  &
  \begin{tikzpicture}[very thick, yscale=.5]
    \draw[dotted,color=gray] (-.5,0) grid (4,4);
    \draw[] (-.5,0) -- (4,0);
    \draw[] (0,0) -- (0,4);
    \draw[->] (0,0) -- (1,0) node[midway,below]{$\vecteur\imath$};
    \draw[->] (0,0) -- (0,1) node[midway,left]{$\vecteur\jmath$};
    \draw (0, 0) node[below left]{$\mathcal{O}$};
    \draw[domain=-.5:4,smooth,blue] plot ({\x},{4-pow(\x+1, 2)/8});
    \draw[dashed] (1.7, 0) node[below]{$a$} -- ++(0, {4-pow(1.7+1, 2)/8}) -- ++(-1.7, 0) node[left]{$f(a)$};
    \draw[dashed] (2.4, 0) node[below]{$b$} -- ++(0, {4-pow(2.4+1, 2)/8}) -- ++(-2.4, 0) node[left]{$f(b)$};
  \end{tikzpicture}
  \\
  Fonction croissante : $a$ est plus petit que $b$, et l'image de $a$ est plus petite que l'image de $b$.
  &
  Fonction décroissante : $a$ est plus petit que $b$, et l'image de $a$ est plus grande que l'image de $b$.\\
\end{tabular}
\end{center}
\end{definition}

\begin{remarque}
  Graphiquement, la courbe représentative d'une fonction croissante
  «~monte~», tandis que celle d'une fonction décroissante «~descend~».
\end{remarque}

\begin{exemple}
  Montrons que la fonction $f:x\mapsto 3x-1$ est croissante, et que la fonction
  $g:x\mapsto -2x+1$ est décroissante.

  \hrule

  \begin{description}
    \item[Fonction $f$ :] Soient $a$ et $b$ deux nombres, tels que $a<b$. Alors :
      \begin{center}
      \begin{tabular}{rcll}
        $a$ & $<$ & $b$ &  \\
        $3a$ & $<$ & $3b$ & car on a multiplié par un nombre positif. \\
        $3a-1$ & $<$ & $3b-1$ & \\
        $f(a)$ & $<$ & $f(b)$ & car $f(a)=3a-1$ et $f(b)=3b-1$ \\
      \end{tabular}
    \end{center}
      Nous avons montré que si $a<b$, alors $f(a)<f(b)$ : la fonction $f$ est donc strictement croissante.
    \item[Fonction $g$ :] Soient $a$ et $b$ deux nombres, tels que $a<b$. Alors :
    \begin{center}
      \begin{tabular}{rcll}
        $a$ & $<$ & $b$ &  \\
        $-2a$ & $>$ & $-2b$ & car on a multiplié par un nombre négatif. \\
        $-2a+1$ & $>$ & $-2b+1$ & \\
        $g(a)$ & $>$ & $g(b)$ & car $g(a)=-2a+1$ et $g(b)=-2b+1$ \\
      \end{tabular}
    \end{center}
      Nous avons montré que si $a<b$, alors $g(a)>g(b)$ : la fonction $g$ est donc strictement décroissante.
    \end{description}
\end{exemple}

\begin{activite}
 Lire la partie \emph{c. Tableau de variation : exemple} de la page 64 du manuel.
\end{activite}

\begin{definition}[Tableau de variations]
  Un tableau de variations résume les informations connues à propos des variations d'une fonction.
\end{definition}

\begin{exemple}Dresser le tableau de variations de la fonction représentée ci-dessous.

  \begin{center}
  \begin{tikzpicture}[very thick, xscale=.7, yscale=.3]
    \draw[dotted, lightgray, xstep=1, ystep=1] (-5, 8) grid (10, -7);
    \draw[-latex] (-5,0) -- (10,0);
    \draw[-latex] (0,-7) -- (0,8);
    \foreach \x in {-5, -4, ..., 10} {
      \ifthenelse{\equal{\x}{0}}{
      }{
        \draw (\x,0) node[below]{\x};
        \draw (\x,{0.1}) -- (\x,{-0.1});
      }
    }
    \foreach \y in {-7, -6, ..., 8} {
      \ifthenelse{\equal{\y}{0}}{
      }{
        \draw (0,\y) node[left]{\y};
        \draw (-.1, \y) -- (.1, \y);
      }
    }
    \draw [cyan] plot [smooth, tension=0.6] coordinates {
      (-4,5)
      (-3.5, 5.3)
      (-2, 7)
      (0, 5)
      (1, 3)
      (2, 0)
      (3, -3)
      (4, -4)
      (5, -3)
      (6,0)
      (6.5, .75)
      (7.1, .95)
      (7.5, 0)
      (8, -4)
      (9, -6)
    };
    \draw (0,0) node[below left]{$O$};
    \draw (1.5, 1.5) node[above right]{$\mathcal{C}_f$};
  \end{tikzpicture}

    \begin{tikzpicture}[xscale=1,yscale=1]
      \tkzTabInit[lgt=1,espcl=2]
      {$x$ /1,
        $f$ /2
      }
      {-4, -2, 4, 7, 9}%
      \tkzTabVar{-/5, +/7, -/-4, +/1, -/-6}
    \end{tikzpicture}
  \end{center}

\end{exemple}

\section{Extremums}

\begin{activite}
 Lire la partie \emph{a. Idée intuitive} de la page 66 du manuel.
 \end{activite}

\begin{definition}[Extremums]
  Soit $I$ un intervalle de $\mathbb R$, $f$ une fonction définie sur $I$, et
  $a$ un élément de $I$.
  \begin{itemize}
    \item Si pour tout $x\in I$, $f(x)\leq f(a)$, on dit que $f(a)$ est le
      \emph{maximum} de $f$ sur $I$, atteint en $a$.
    \item Si pour tout $x\in I$, $f(x)\geq f(a)$, on dit que $f(a)$ est le
      \emph{minimum} de $f$ sur $I$, atteint en $a$.
    \item On appelle \emph{extremum} un minimum ou un maximum.
  \end{itemize}
\end{definition}

\begin{definition}[Extremum local, global]~
  \begin{itemize}
    \item Le maximum $f(a)$ de $f$ est dit \emph{global} s'il est maximum de $f$ sur tout son ensemble de définition, et \emph{local} sinon.
    \item De même pour le minimum.
  \end{itemize}
\end{definition}

\begin{exemple}
  Sur l'exemple précédent, la fonction $f$ présente :
  \begin{itemize}[$\bullet$]
    \item des maximums 7 et 1, atteints respectivements en $-2$ et 7 ;
    \item des minimums 5, -4 et -6, atteints respectivements en $-4$, 4 et 9.
  \end{itemize}

  Parmi ceux-ci, $7$ est un maximum global (les autres sont locaux), et $-6$ est un minimum global (les autres sont locaux).
\end{exemple}

\end{document}
