%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2015-2021 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename
%$ lualatex $basename

\documentclass[10pt]{article}

\usepackage{2122-pablo}
\usepackage{2122-pablo-paternault}
\usepackage{2122-pablo-math}
\usepackage{2122-pablo-tikz}

\usepackage[includehead, a5paper, margin=7mm]{geometry}
\usepackage{2122-pablo-header}
\fancyhead[L]{\textsc{Mathématiques 2\up{de}{}}}
\fancyhead[C]{\textsc{DM \no4}}
\fancyhead[R]{06/01/22}
\fancyfoot[C]{}

\setlength{\parindent}{0pt}

\begin{document}

\begin{em}
\begin{itemize}
\item Fonctions et équations : Faire un des deux exercices \ref{exo:equation1} ou \ref{exo:equation2} au choix (l'exercice \ref{exo:equation2} est plus difficile).
\item Fonctions paires et impaires : Faire un des deux exercices \ref{exo:parite1} ou \ref{exo:parite2} au choix (l'exercice \ref{exo:parite1} est plus difficile).
\item Les exercices \ref{exo:parite} et \ref{exo:automatismes} sont obligatoires.
\end{itemize}
\end{em}

\begin{exercice}\label{exo:equation1}
On considère les fonctions $f$ et $g$ définies sur l'ensemble $\intervalleio{0}\cup\intervalleoi{0}$ par :
\[
f(x)=\frac{2}{x}
\text{ et }
g(x)=8x
\]

Les trois questions sont indépendantes.
\begin{enumerate}
\item Quels sont les antécédents de $2,4$ par $g$ ?
\item Résoudre $f(x)=10$.
\item Résoudre $f(x)=g(x)$.
\end{enumerate}
\end{exercice}

\begin{exercice}\label{exo:equation2}
On considère les fonctions $f$ et $g$ définies sur $\mathbb{R}$ par :
\[
f(x)=3x^2-3
\text{ et }
g(x)=x^2-5x
\]
\begin{enumerate}
\item Montrer que l'équation $f(x)=g(x)$ est équivalente à :
\[(2x-1)(x+3)=0\]
\item En déduire les solutions de $f(x)=g(x)$.
\end{enumerate}
\end{exercice}

\begin{exercice}[Obligatoire]\label{exo:parite}
Les courbes des fonctions $f$ et $g$ sont partiellement représentées ci-dessous.

\begin{tikzpicture}[very thick,xscale=.8, yscale=1]
  \draw[dotted, lightgray, xstep=1, ystep=.5] (-4, -1) grid (4, 3);
  \draw[-latex] (-4,0) -- (4,0);
  \draw[-latex] (0,-1) -- (0,3);
  \draw (0,0) node[below left]{$O$};
  \draw (4.5, -.75) node[anchor=south east]{Courbe de $f$.};
  \clip (-4, -1) rectangle (0, 3);
  \draw [cyan] plot [smooth, tension=0.5] coordinates {
    (-4,-1)
    (-3, 2.5)
    (0, 1.1)
    (3, 2.5)
    (4,-1)
  };
\end{tikzpicture}%
\hfill
\begin{tikzpicture}[very thick,xscale=.8, yscale=1]
  \draw[dotted, lightgray, xstep=1, ystep=.5] (-4, -2) grid (4, 2);
  \draw[-latex] (-4,0) -- (4,0);
  \draw[-latex] (0,-2) -- (0,2);
  \draw [cyan] plot [smooth, tension=0.5] coordinates {
    (0, 0)
    (1, 1)
    (3, 2)
    (4,1)
  };
  \draw (0,0) node[below left]{$O$};
  \draw (4.5, -1.5) node[anchor=south east]{Courbe de $g$.};
\end{tikzpicture}%

\begin{enumerate}
\item Complétez la courbe de $f$, sachant que cette fonction est \emph{paire}.
\item Complétez la courbe de $g$, sachant que cette fonction est \emph{impaire}.
\end{enumerate}
\end{exercice}

\pagebreak

\begin{exercice}
\label{exo:parite1}
% Inspiré de l'exercice 44 p. 58 du manuel de 2nde, LeLivreScolaire.fr, éditions 2019.

Dans cet exercice, on va prouver la propriété : 
\begin{quote}
Si une fonction $f$ a sa courbe représentative symétrique par rapport à l'origine, alors elle est impaire.
\end{quote}

Soit $f$ une fonction définie sur $\mathbb{R}$, dont la courbe est symétrique par rapport à l'origine du repère. Soit $M$ un point de la courbe, d'abscisse $x$, et $N$ son symétrique par rapport à l'origine.

	\begin{em}
Remarquez bien qu'à ce stade, on ne sait pas encore que la fonction est impaire, donc on ne peut pas utiliser la propriété $f(-x)=-f(x)$ : il faudra la prouver.
	\end{em}

\begin{enumerate}
\item Rappelez les coordonnées de l'origine $O$ du repère.
\item Rappelez pourquoi les coordonnées de $M$ sont $\left(x; f(x)\right)$.
\item \emph{Les deux questions sont indépendantes.}
\begin{enumerate}
\item Montrez que les coordonnées de $N$ (symétrique de $M$ par rapport à l'origine $O$ du repère) sont $N\left(-x; -f(x)\right)$
\item Rappelez pourquoi $N$ est sur la courbe de la fonction $f$, et montrer que les coordonnées de $N$ sont $N\left(-x; f(-x)\right)$.
\end{enumerate}
\item \emph{Bilan :} En déduire que $f$ est impaire.
\end{enumerate}
\end{exercice}

\begin{exercice}
\label{exo:parite2}
Dans cet exercice, on va montrer que la fonction $f$ définie sur $\mathbb{R}$ par $f:x\mapsto\frac{x^2+1}{x}$  est impaire, mais pas paire.
\begin{enumerate}
\item Calculer $f(4)$ et $f(-4)$. La fonction $f$ est-elle paire ?
\item Soit $x$ un nombre réel quelconque.
\begin{enumerate}
\item Justifiez que $\left(-x\right)^2=x^2$.
\item Montrez que $f(-x)=-\frac{x^2+1}{x}$.
\item En déduire que la fonction $f$ est impaire.
\end{enumerate}
\item La courbe de $f$ est-elle symétrique par rapport à l'axe des ordonnées ? par rapport à l'origine ?
\end{enumerate}
\end{exercice}

\begin{exercice}[Exercice libre --- Obligatoire]\label{exo:automatismes}
  Choissez un exercice sur le site web \url{http://pyromaths.org}, imprimez l'énoncé (ou envoyez-le moi par Pronote), résolvez cet exercice, et vérifiez vos résultats avec le corrigé. Rendez l'énoncé avec la copie.

Faites l'exercice sur votre copie, mais je ne le corrigerai pas (sauf si vous le demandez).

Exemples d'exercices pour travailler le chapitre en cours, ou des notions vues au collèges qui ont peut-être été un peu oubliées.
  \begin{itemize}
    \item \emph{Classe de troisième $\rightarrow$ Équation} (pour travailler les équations du premier degré) ;
    \item \emph{Classe de troisième $\rightarrow$ Bilan sur la notion de fonction} (pour travailler une partie du chapitre précédent) ;
    \item \emph{Classe de troisième $\rightarrow$ Fractions} (pour travailler le calcul de fractions) ;
    \item \emph{Classe de troisième $\rightarrow$ Factorisations} (pour travailler les factorisations (que nous reverrons ensemble)) ;
    \item \emph{Classe de troisième $\rightarrow$ Identités remarquables} (pour travailler les identités remarquables (que nous reverrons ensemble)).
  \end{itemize}
\end{exercice}

\end{document}
