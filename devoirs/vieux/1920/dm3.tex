%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2019 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{1920-pablo}
\usepackage{1920-pablo-paternault}
\usepackage{1920-pablo-devoir}
\usepackage{1920-pablo-listings}
\usepackage[a5paper, margin=2cm]{geometry}

\title{Fonctions\\ Algorithmique}
\date{03/12/19}
\classe{2\up{nde}}
\dsnum{DM \no3}

\pagestyle{empty}
\setlength{\parindent}{0pt}

\begin{document}
\maketitle

\begin{em}
Faire un des deux exercices \ref{exo:parite1} ou \ref{exo:parite2} au choix (l'exercice \ref{exo:parite1} est plus long et difficile). Les autres exercices sont obligatoires.
\end{em}

\begin{exercice}
\label{exo:parite1}
% Inspiré de l'exercice 44 p. 58 du manuel de 2nde, LeLivreScolaire.fr, éditions 2019.

Dans cet exercice, on va prouver la propriété : 
\begin{quote}
Si une fonction $f$ a sa courbe représentative symétrique par rapport à l'origine, alors elle est impaire.
\end{quote}

Soit $f$ une fonction définie sur $\mathbb{R}$, dont la courbe est symétrique par rapport à l'origine du repère. Soit $M$ un point de la courbe, d'abscisse $x$, et $N$ son symétrique par rapport à l'origine.

\begin{enumerate}
\item Rappeler les coordonnées de l'origine $O$ du repère.
\item Expliquer pourquoi les coordonnées de $M$ sont $\left(x; f(x)\right)$.
\item Déterminer, en fonction de $x$ et $f(x)$, les coordonnées de $N$, symétrique de $M$ par rapport à l'origine $O$ du repère.
\item Justifier que $N$ est sur la courbe de la fonction $f$.
\item En déduire que $f$ est impaire.
\end{enumerate}
\end{exercice}

\begin{exercice}
\label{exo:parite2}
Les courbes des fonctions $f$ et $g$ sont partiellement représentées ci-dessous.

\begin{tikzpicture}[very thick,xscale=.6, yscale=1]
  \draw[dotted, lightgray, xstep=1, ystep=.5] (-4, -1) grid (4, 3);
  \draw[-latex] (-4,0) -- (4,0);
  \draw[-latex] (0,-1) -- (0,3);
  \draw (0,0) node[below left]{$O$};
  \draw (4.5, -.75) node[anchor=south east]{Courbe de $f$.};
  \clip (-4, -1) rectangle (0, 3);
  \draw [cyan] plot [smooth, tension=0.5] coordinates {
    (-4,-1)
    (-3, 2.5)
    (0, 1.1)
    (3, 2.5)
    (4,-1)
  };
\end{tikzpicture}%
\hfill
\begin{tikzpicture}[very thick,xscale=.6, yscale=1]
  \draw[dotted, lightgray, xstep=1, ystep=.5] (-4, -2) grid (4, 2);
  \draw[-latex] (-4,0) -- (4,0);
  \draw[-latex] (0,-2) -- (0,2);
  \draw [cyan] plot [smooth, tension=0.5] coordinates {
    (0, 0)
    (1, 1)
    (3, 2)
    (4,1)
  };
  \draw (0,0) node[below left]{$O$};
  \draw (4.5, -1.5) node[anchor=south east]{Courbe de $g$.};
\end{tikzpicture}%

\begin{enumerate}
\item Compléter la courbe de $f$, sachant que cette fonction est \emph{paire}.
\item Compléter la courbe de $g$, sachant que cette fonction est \emph{impaire}.
\end{enumerate}
\end{exercice}

\begin{exercice}
\label{exo:algo}
Qu'affiche l'algorithme suivant ?

\begin{em}
\begin{enumerate}[{Remarque} 1]
\item Le mot-clef \texttt{Renvoie} correspond au \texttt{return} en Python.
\item Expliquer votre démarche pour la dernière ligne (pas besoin de justifier pour les autres).
\end{enumerate}
\end{em}

\begin{lstlisting}[language=naturel,frame=single, mathescape=true]
Fonction d(a, b):
  Renvoie b - a 

Fonction abs(a):
  Si a $\geq$ 0
  Alors
    Renvoie a
  Sinon
    Renvoie -a
  FinSi

Afficher(d(12, 8))
Afficher(abs(-8))
Afficher(abs(2))
Afficher(abs(d(12, 8)))
\end{lstlisting}
\end{exercice}

\vfill\hfill\emph{Tournez la page…}
\pagebreak

\begin{exercice}
\label{exo:modelisation}
Sabrina a fabriqué un lanceur de balles de tennis. Elle aimerait filmer sa création avec son drone. On modélise la situation par la figure suivante (qui n'est pas à l'échelle), où l'axe des abscisses est à l'horizontale, et l'axe des ordonnées à la verticale.

\begin{center}
\begin{tikzpicture}[xscale=2, yscale=1, very thick]
  \draw[-latex] (-2,0) -- (2,0);
  \draw[-latex] (0,0) -- (0,4);
  \draw (0,0) node[below] {$\scriptstyle O$};
  \draw (1, 1) node{$\bullet$} node[below left]{$D$};
  \clip (-2, 0) rectangle (2, 4);
  \draw[color=blue, domain=-2:1, dashed, ->] plot (\x,{-\x*\x +3});
\end{tikzpicture}
\end{center}

La trajectoire de la balle est modélisée par la courbe bleue, d'équation $f:x\mapsto -x^2+3$ (l'unité est le décamètre, soit \SI{10}{m}). Le drone est positionnée en $D(1;1)$.

La question que se pose Sabrina est : À quelle distance du drone va s'approcher la balle de tennis ?

Soit $M$ un point de la courbe de $f$, d'abscisse $x$. On appelle $DM$ la distance entre le point $M$ et le drone $D$.

\begin{enumerate}
\item Justifier que les coordonnées de $M$ sont $\left(x; -x^2+3\right)$.
\item \emph{Optionnel (cette question est difficile) :} Montrer que $DM=\sqrt{\left(x-1\right)^2+\left(-x^2+2\right)^2}$ (il est possible de simplifier davantage cette expression, mais on ne le demande pas).
\end{enumerate}
On admet que : $DM=\sqrt{\left(x-1\right)^2+\left(-x^2+2\right)^2}$.
\begin{enumerate}[resume]
\item À l'aide d'un ordinateur ou de votre calculatrice, tracer la courbe représentative de la distance $DM$ en fonction de $x$. Tracer l'allure de la courbe sur votre copie (c'est-à-dire la « forme » de la courbe, sans nécessairement respecter l'échelle).
\item Par lecture graphique, déterminer une valeur approchée de la plus petite valeur prise par $DM$.
\item Réponse au problème posé : À quelle distance du drone va s'approcher la balle de tennis ?
\end{enumerate}
\end{exercice}

\begin{exercice}[Exercice libre]
  \label{exo:culture}
  Choisir un exercice sur le site web \url{http://pyromaths.org}, imprimer l'énoncé (ou me l'envoyer par courriel), résoudre cet exercice, et vérifiez vos résultats avec le corrigé. Rendre l'énoncé avec la copie.

Faites l'exercice sur votre copie, mais je ne le corrigerai pas (sauf si vous le demandez).

Exemples d'exercices pour travailler le chapitre en cours, ou des notions vues au collèges qui ont peut-être été un peu oubliées.
  \begin{itemize}
    \item \emph{Classe de quatrième $\rightarrow$ Somme de positifs en écriture fractionnaire} (pour travailler les fractions) ;
    \item \emph{Classe de quatrième $\rightarrow$ Distributivité \emph{et} Bases du calcul littéral} (pour travailler le calcul littéral (avec des $x$), et la distributivité ;
    \item \emph{Classe de troisième $\rightarrow$ Équation} (pour travailler les équations du premier degré) ;
    \item \emph{Classe de troisième $\rightarrow$ Bilan sur la notion de fonction} (pour travailler le chapitre en cours.
  \end{itemize}
\end{exercice}
\end{document}
