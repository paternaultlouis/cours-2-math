# Un peu de calcul…

## Puissances (1h30)

- Cours (cours-puissances.tex)

## Racine carrée (2h)

- Cours (cours-racine-carree.tex)
- Exercices : 39, 40 p. 20

## Identités remarquables (2h)

- Cours (cours-identites-remarquables.tex)
- Exercices : 65, 69 p. 23
