%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2020-2021 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{2021-pablo}
\usepackage{2021-pablo-paternault}
\usepackage{2021-pablo-math}

\usepackage{2021-pablo-tikz}
\usepackage[europeanresistors]{circuitikz}

\usepackage[a5paper, margin=10mm, includehead]{geometry}
\usepackage{2021-pablo-devoir}
\fancyhead[L]{\textsc{Ch. 12 --- Fonctions de référence}}
\fancyhead[R]{\textsc{Distanciel 7}}
\fancyfoot[C]{}

\usepackage{multicol}
\usepackage{qrcode}

\begin{document}

% Avec le manuel LeLivreScolaire, math, secondes, 2019.

\section*{Exercices sur tout le chapitre}

\emph{Tous les exercices mentionnés sont ceux du chapitre 4 du manuel (à partir de la page 117).}

Faites autant d'exercices que possible, et arrêtez-vous lorsque vous avez fait 55 minutes de mathématiques aujourd'hui.

\begin{exercice*}
  Ces exercices sont (à peu près) classés par ordre de difficulté croissante. Passez vite, voire ne faites pas, les exercices qui vous paraissent faciles, en particulier si vous souhaitez choisir la spécialité mathématiques en première générale l'an prochain.
  \begin{itemize}
    \item QCM rapides : 6, 8 à 13.
    \item Équations et inéquations : 75, 76.
    \item Petits problèmes, exercices plus difficiles : 79, 88.
    \item Exercice difficile : voir l'exercice suivant.
  \end{itemize}
\end{exercice*}

\begin{exercice}\label{exo:resistance}
  \emph{Cet exercice est difficile. Ne paniquez pas si vous n'y arrivez pas.}

  On dispose, en parallèle, d'une résistance de \SI{2}{\ohm}, et d'un potentiomètre (résistance variable) de résistance $x$.

  \begin{center}
    \begin{circuitikz}[scale=1, very thick]
      \draw
      (0,0) to (1,0)
      to (1, 1em)
      to [R, l^=\SI{2}{\ohm}] (3,1em)
      to (3, 0)
      to (4, 0);

      \draw
      (1, 0) to (1, -1em)
      to [R, l_=$x$] (3, -1em)
      to (3, 0);
    \end{circuitikz}
  \end{center}

  L'ensemble de ce circuit est équivalent à une résistance d'une certaine valeur $R(x)$, dont on admet qu'elle est donnée par la formule :
  \[
    R(x) = \frac{1}{\frac{1}{2}+\frac{1}{x}}
  \]

  Le potentiomètre permet de donner à $x$ des valeurs allant de $0,5$ à 4, et on cherche à connaître la plus grande et la plus petite valeur pouvant alors être prise par $R(x)$.

  \begin{enumerate}
    \item Montrer que $R(x)=\frac{2x}{2+x}$, puis que $R(x)=2-\frac{4}{x+2}$.
    \item \emph{Encadrement de $R(x)$.}
      \begin{description}
        \item[Première partie : $0,5\leq x$] Lisez et comprenez le calcul suivant.

      \begin{align*}
        0,5 & \leq x&\\
        2+0,5 & \leq 2+x&\\
        2,5 & \leq 2+x&\\
        \frac{1}{2,5} & \geq\frac{1}{2+x} \text{ car la fonction inverse est décroissante} \\
        0,4 & \geq\frac{1}{2+x} \\
        4\times0,4 & \geq\frac{4}{2+x} \\
        1,6 & \geq\frac{4}{2+x} \\
        -1,6 & \leq-\frac{4}{2+x} \text{ car on a multiplié}\\
        &\text{\phantom{XXXXXXX} par un nombre négatif ($-1$)} \\
        2-1,6 & \leq2-\frac{4}{2+x} \\
        0,4 & \leq2-\frac{4}{2+x} \\
        0,4 & \leq R(x)\\
      \end{align*}
    \item[Deuxième partie : $x\leq4$] En utilisant le même genre de démonstration que dans la première partie, montrez que $R(x) \leq \frac{4}{3}$ (utilisez de préférence les valeurs exactes, mais vous pouvez utiliser des valeurs approchées pour simplifier les calculs, auquel cas vous devez obtenir $R(X)\leq1,3$).
      \end{description}
    \item Conclure : Quelles sont la plus petite et la plus grande valeur qui peut être prise par la résistance $R(x)$ de l'ensemble du circuit ?
  \end{enumerate}
\end{exercice}

\begin{exercice*}[Corrigé de l'exercice \ref{exo:resistance}]
    \begin{enumerate}
      \item \emph{Montrer que $R(x)=\frac{2x}{2+x}$, puis que $R(x)=2-\frac{4}{x+2}$.}

        Soit $x\in\intervallef{0,5;4}$ :
        \begin{multicols}{2}
          D'une part :
        \begin{align*}
          R(x)
          &=\frac{1}{\frac{1}{2}+\frac{1}{x}}\\
          &=\frac{1}{\frac{1\times x}{2\times x}+\frac{1\times2}{2\times x}}\\
          &=\frac{1}{\frac{x}{2x}+\frac{2}{2x}}\\
          &=\frac{1}{\frac{x+2}{2x}}\\
          &=\frac{2x}{x+2}
        \end{align*}

        D'autre part :
        \begin{align*}
          2-\frac{4}{x+2}
          &=\frac{2\times(x+2)}{x+2}-\frac{4}{x+2}\\
          &=\frac{2x+4}{x+2}-        \frac{4}{x+2}\\
          &=\frac{2x+4-4}{x+2}\\
          &=\frac{2x}{x+2}\\
          &=R(x)
        \end{align*}
        \end{multicols}
      \item \emph{Encadrement de $R(x)$.}
        \begin{description}
          \item[Deuxième partie : $x\leq4$]
               \begin{align*}
                x&\leq4\\
                x+2&\leq6\\
                \frac{4}{x+2}&\geq\frac{4}{6}\text{ car la fonction inverse est décroissante.}\\
                \frac{4}{x+2}&\geq\frac{2}{3}\\
                -\frac{4}{2+x}&\leq-\frac{2}{3} \text{ car on a multiplié}\\
                &\text{\phantom{XXXXXXX} par un nombre négatif ($-1$)} \\
                2-\frac{4}{2+x}&\leq2-\frac{2}{3}\\
                R(x)&\leq\frac{6}{3}-\frac{2}{3}\\
                R(x)&\leq\frac{4}{3}
              \end{align*}
        \end{description}
      \item Donc la plus petite valeur que peut prendre $R(x)$ est \SI{0,4}{\ohm}, la plus grande est $\frac{4}{3}\Omega$.
    \end{enumerate}
  \end{exercice*}

\end{document}
