from math import sqrt

donnees = [1, 5, 6, 8]
classe1 = [6.5, 7.5, 13, 9.5, 7.5, 17, 15.5, 4, 16, 12.5, 13.5, 11, 9.5, 14, 19.5, 16, 9.5, 6.5, 8.5, 4, 12, 7.5, 10.5, 15.5, 11.5, 3.5, 13.5, 10, 5.5, 9.5, 8, 18]
classe2 = [12, 4, 15.5, 11, 16.5, 15, 11, 8, 14.5, 17, 16, 6, 13.5, 17, 17, 9, 17.5, 9.5, 16.5, 17.5, 6, 8.5, 12, 8, 13, 9, 8.5]


def somme(liste):
    return sum(liste)

def moyenne(liste):
    somme = 0
    for nombre in liste:
        somme = somme + nombre
    return somme/len(liste)

def ecarttype(liste):
    m = moyenne(liste)
    somme = 0
    for nombre in liste:
        somme = somme + (nombre - m)**2
    return sqrt(somme/len(liste))

def intervalle(liste):
    m = moyenne(liste)
    sigma = ecarttype(liste)
    dedans = 0
    for nombre in liste:
        if m - 2*sigma <= nombre <= m + 2*sigma:
            dedans = dedans + 1
            
    return dedans/len(liste)

print("Sommes")
print(somme(donnees))
print(somme(classe1))
print(somme(classe2))
print("Moyennes")
print(moyenne(donnees))
print(moyenne(classe1))
print(moyenne(classe2))
print("Écart type")
print(ecarttype(donnees))
print(ecarttype(classe1))
print(ecarttype(classe2))
print("Intervalles")
print(intervalle(donnees))
print(intervalle(classe1))
print(intervalle(classe2))
