%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2022 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[10pt]{article}

\usepackage{2122-pablo}
\usepackage{2122-pablo-tikz}
\usepackage{2122-pablo-math}
\usepackage{2122-pablo-paternault}

\usepackage[
    a5paper,
    margin=7mm,
    includehead,
    headsep=2mm,
   ]{geometry}
\usepackage{2122-pablo-header}

\fancyhead[L]{\textsc{Ch. 13 --- Information chiffrée}}

\usepackage{multicol}

\begin{document}

\section{Proportion et Pourcentages}

\begin{definition*}
	Soient $E$ un ensemble non vide, et $n_E$ le nombre d'élément de $E$. Soient $A$ une partie de $E$, ayant $n_A$ éléments.

	La \emph{proportion} de $A$ dans $E$ est le nombre défini par : $p=\dfrac{n_A}{n_E}$.
\end{definition*}

\begin{propriete*}
	On a donc : $n_E = \dfrac{n_A}{p}$ et $n_A=p\times n_E$.
\end{propriete*}

\begin{remarque*}
	Pour obtenir la proportion sous forme de pourcentage, on peut multiplier le résultat par 100 : $\dfrac{n_A}{n_E}\times100$.
\end{remarque*}

\begin{remarque*}
	Pour manipuler les proportions, on peut utiliser un tableau de proportionnalité.
\end{remarque*}

\begin{exemple}
	Une entreprise emploie 134 salariés.
	\begin{enumerate}
		\item Parmi ceux-ci, 77 sont des femmes. Quelle proportion de femmes l'entreprise emploie-t-elle ?
		\item Environ 63\% des salariés sont des ouvriers. Combien d'ouvriers y a-t-il dans l'entreprise ?
	\end{enumerate}
\end{exemple}

\begin{propriete*}
	Soit $E$ un ensemble, $A$ une partie de $E$, et $B$ une partie de $A$.

	On appelle $p_A$ la proportion de $A$ dans $E$, et $p_B$ la proportion de $B$ dans $A$.

	Alors la proportion de $B$ dans $E$ est \blanc{$p_A\times p_B$}.
\end{propriete*}

\begin{exemple}
  Certains élèves d'un lycée suivent une première générale. Parmi ceux-ci, 78\% ont choisi la spécialité mathématique. Parmi ceux-ci 52\% conservent cette spécialité en terminale. On se demande quelle est parmi l'ensemble des élèves de première générale, quelle est la proportion qui prendre la spécialité mathématiques en terminale.
\end{exemple}

\section{Évolution}

\begin{definition*}
	Soient une valeur initiale $V_I$ et une valeur finale $V_F$.

	\begin{itemize}
		\item La \emph{variation absolue} est le nombre $V_F-V_I$.
		\item La \emph{variation relative} est le nombre $\dfrac{V_F-V_I}{V_I}$, appelé \emph{taux d'évolution}.
	\end{itemize}
\end{definition*}

\begin{exemple}
	Entre 2020 et 2021, le nombre d'adhérents d'une association est passé de 342 à 411 membres.
	\begin{enumerate}
		\item Calculer l'évolution absolue du nombre d'adhérents.
		\item Calculer l'évolution relative du nombre d'adhérents.
		\item Les responsables espèrent une évolution de 5\% de l'année 2021 à l'année 2022. Quelle serait alors le nombre d'adhérents ?
	\end{enumerate}
\end{exemple}

\pagebreak

\begin{defprop*}~
\begin{itemize}
\item Appliquer un taux d'évolution $t$ à une valeur revient à la multiplier par le nombre $1+t$, appelé \emph{coefficient multiplicateur}.
\item Appliquer un taux d'évolution de $t\%$ à une valeur revient à la multiplier par $1+\frac{t}{100}$.
\end{itemize}
\end{defprop*}

\begin{exemple}
En 2018, il y avait dans une réserve naturelle 68 loups et 296 cerfs.
\begin{enumerate}
\item De 2018 à 2019, le taux d'évolution du nombre de loups dans la réserve était de \numprint{-0,12}. Combien y avait-il de loups en 2019 ?
\item De 2018 à 2019, le nombre de cerfs a augmenté de 6\%. Combien y avait-il de cerfs en 2019 ?
\end{enumerate}
\end{exemple}

\begin{propriete*}
Soit $t$ un taux d'évolution, et $CM$ le coefficient multiplicateur correspondant. Alors :
\[
CM = 1 + t 
\text{ et }
t = CM - 1
\]
\end{propriete*}

\begin{exemple}~
\begin{enumerate}
\item Convertir les taux d'évolutions suivants en coefficients multiplicateurs :
\begin{enumerate*}
\item +30\% ;
\item -0,05 ;
\item -50\%.
\end{enumerate*}
\item Convertir les coefficients multiplicateurs en taux d'évolution :
\begin{enumerate*}
\item 0,97 ;
\item 1,36 ;
\item 1.
\end{enumerate*}
\end{enumerate}
\end{exemple}

\begin{propriete*}
Le coefficient multiplicateur associé à plusieurs évolutions successives est égal au produit des coefficients multiplicateurs de chacune des évolutions.
\end{propriete*}

\begin{exemple}
Une association répare et vend des vélos usagés. Voici les taux d'évolution des ventes de vélo durant quatre ans.
\begin{center}\begin{tabular}{r*{4}{c}}
\toprule
Année & 2017 & 2018 & 2019 & 2020 \\
\midrule
Évolution par rapport à l'année précédente & +3\% & +8\% & -5\% & +7\% \\
\bottomrule
\end{tabular}\end{center}
Quelle est le taux d'évolution du nombre de vélos vendus entre 2016 et 2020 ?
\end{exemple}

\begin{propriete*}
Le coefficient multiplicateur associé à une évolution réciproque est égal à l'inverse du coefficient multiplicateur de l'évolution.
\end{propriete*}

\begin{exemple}
De 2020 à 2021, le chiffre d'affaires d'une entreprise a baissé de 23\%. Quel devrait être le taux d'évolution de ce chiffre d'affaire de 2021 à 2022 pour qu'il retrouve sa valeur initiale ?
\end{exemple}

\begin{exemple}[Presque hors-programme]
Voici l'évolution du nombre d'employés d'une entreprise pendant quatre ans.
\begin{center}\begin{tabular}{r*{3}{c}}
\toprule
Année & 2018 & 2019 & 2020 \\
\midrule
Évolution par rapport à l'année précédente & +21\% & +4\% & +9\% \\
\bottomrule
\end{tabular}\end{center}
Calculer le taux d'évolution annuel moyen du nombre d'employés de l'entreprise durant ces trois années.
\end{exemple}

\end{document}
