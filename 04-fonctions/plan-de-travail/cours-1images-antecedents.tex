%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2018 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename
\documentclass[12pt, twoside]{article}


\usepackage{1920-pablo}
\usepackage{1920-pablo-cours}
\usepackage{1920-pablo-paternault}
\usepackage[a5paper, margin=.8cm, outer=.3cm, inner=2cm]{geometry}
\usetikzlibrary{intersections}

\begin{document}

\section{Images et Antécédents}

\subsection{Définitions}

\begin{ndefinition}
  Étant donné un sous-ensemble $\mathcal{D}$ de $\mathbb{R}$, définir une \emph{fonction} de $\mathcal{D}$ dans $\mathbb{R}$, c'est associer à tout nombre $x$ de $\mathcal{D}$ un unique nombre $f(x)$ de $\mathbb{R}$.

  \begin{itemize}[$\bullet$]
    \item $\mathcal{D}$ est appelé \emph{ensemble de définition de $f$} ;
    \item $f(x)$ est \emph{l'image} de $x$ par $f$ ;
    \item $x$ est un \emph{antécédent} de $f(x)$ par $x$.
  \end{itemize}
\end{ndefinition}

\begin{nexemple}
    Soit la fonction $V$ qui à une longueur $c$ (exprimée en mètre) associe le volume du cube de côté $c$ (exprimé en mètres cube).

      On peut exprimer cette fonction sous la forme :
      \[\begin{array}{rcl}
          V:\mathbb{R}^+&\to&\mathbb{R}\\
          c&\mapsto& c^3
      \end{array}\]
      Cela signifie :
      \begin{itemize}
        \item Le domaine de définition de la fonction est $\mathbb{R}^+$ (l'ensemble des nombres réels positifs). En d'autres termes, on peut calculer $V(x)$ pour n'importe quel nombre positif $x$ (mais calculer $V(-7)$, par exemple, n'a pas de sens).
        \item L'ensemble image est $\mathbb{R}$ (les images de la fonction sont des nombres réels).
        \item Pour calculer l'image d'un nombre $c$, on applique la formule $c^3$.
      \end{itemize}
      On a alors $V(2)=2^3=8$, ce qui signifie : « Le volume d'un cube de côté \SI{2}{m} est \SI{8}{m^3}.
\end{nexemple}

\begin{remarque}
  Assez souvent en seconde, on notera simplement : $V:c\mapsto c^3$ (sans les ensembles de définition et ensembles image).
\end{remarque}

\begin{npropriete}~
  \begin{itemize}
    \item Tout nombre $x$ de l'ensemble de définition de $f$ a une \emph{unique} image par $f$.
    \item Un nombre réel $a$ a zéro, un ou plusieurs antécédents par $f$.
  \end{itemize}
\end{npropriete}

\begin{nexemple}~
  \begin{enumerate}
    \item Pour tout nombre $x$, on lance une pièce équilibrée. Si elle tombe sur pile, on multiplie $x$ par deux ; sinon, on le divise par deux.

      Ceci n'est pas une fonction car en partant du nombre $8$ (par exemple), on peut tomber sur 16 (si la pièce retombe sur pile), ou 4 (si la pièce retombe sur face). Or, pour un même nombre de départ $x$, une fonction produit \emph{toujours} la même image $f(x)$.
    \item On considère la fonction $f:x\mapsto x^2$.

      \begin{itemize}
        \item  Puisque $f(4)=4^2=16$, et $f(-4)=\left(-4\right)^2=16$, alors $f(4)=f(-4)=16$ et le nombre 16 a deux antécédents par $f$ : $4$ et $-4$.
        \item $0$ est le seul nombre dont le carré vaut $0$, donc $0$ n'a qu'un seul antécédent (il existe un seul nombre $x$ tel que $f(x)=0$, et c'est $x=0$).
        \item Puisqu'un carré est toujours positif, alors $-5$ n'a pas d'antécédent : il n'existe pas de $x$ tel que $f(x)=-5$.
      \end{itemize}
  \end{enumerate}

\end{nexemple}

\begin{ndefinition}
  L'\emph{ensemble image} d'une fonction $f$ est l'ensemble des valeurs que peut prendre $f(x)$ lorsque $x$ parcourt son ensemble de définition.
\end{ndefinition}

\begin{nmethode}~
  \begin{itemize}
    \item Pour déterminer l'image de $x$ par $f$, on calcule $f(x)$ en remplaçant $x$ par sa valeur dans la formule de $f$.
    \item Pour trouver les antécédentes de $a$ par $f$, on résout l'équation $f(x)=a$.
  \end{itemize}
\end{nmethode}

\begin{nexemple}
  On considère la fonction définie sur $\mathbb{R}$ par $f:x\mapsto 3x-2$.

  \begin{enumerate}
    \item Calculer $f(-3)$.
    \item Calculer l'image de 7 par $f$.
    \item Résoudre $f(x)=4$.
    \item Quels sont les antécédents de $-5$ par $f$ ?
  \end{enumerate}

  \begin{enumerate}[1 {(corrigé)}.]
    \item Pour calculer $f(-3)$, on remplace $x$ par $-3$ dans l'expression de $f$ : $f(-3)=3\times\left(-3\right)-2=-6-2=-8$.
    \item L'image de 7 par $f$ est $f(7)=3\times7-2=21-2=19$.
    \item Résolvons l'équation.
      \begin{multicols}{2}
      \begin{align*}
f(x) &= 4 \\
3x-2&=4\\
3x&=4+2\\
3x&=6\\
x&=\frac{6}{3}\\
x&=2
      \end{align*}

      \columnbreak

      Donc 4 a un unique antécédent par $f$ : 2 (on peut vérifier en calculant $f(2)=3\times 2-2=6-2=4$).
    \end{multicols}
    \item Pour chercher les antécédents de $-5$ par $f$, on résout l'équation $f(x)=-5$, c'est-à-dire $3x-2=-5$, et on obtient $x=-1$. Donc l'unique antécédent de $-5$ par $f$ est $-1$ (en d'autres termes : $f(-1)=-5$).
  \end{enumerate}
\end{nexemple}

\subsection{Représentation et lecture graphiques}

\begin{ndefinition}
  Dans le plan muni d'un repère, la \emph{courbe représentative} (ou \emph{représentation graphique}) de la fonction $f$ est l'ensemble $\mathcal{C}_f$ des points $(x, f(x))$, où $x$ décrit le domaine de définition de $f$.
\end{ndefinition}

\begin{nmethode}
  Pour lire l'image de $x$ par $f$ :
  \begin{itemize}
    \item on repère $x$ sur l'axe des abscisses ;
    \item on trace la droite parallèle à l'axe des ordonnées d'abscisse $x$ ;
    \item on repère le point $M$, intersection de la courbe de $f$ et de la droite précédente ;
    \item on lit l'ordonnée de ce point $M$ : c'est l'image de $x$ par $f$.
  \end{itemize}
\end{nmethode}

\begin{nexemple}Lecture graphique de $f(3)$ (c'est-à-dire l'image de 3 par $f$).

\begin{center}
    \begin{tikzpicture}[very thick,xscale=.8, yscale=1.2]
      \draw[dotted, lightgray, xstep=1, ystep=.5] (-4, -1) grid (6, 3);
      \draw[-latex] (-4,0) -- (6,0);
      \draw[-latex] (0,-1) -- (0,3);
      \foreach \x in {-4, -3, -2, -1, 1, 2, 3, 4, 5, 6} {
        \draw (\x,0) node[below]{\x};
        \draw (\x,{0.1}) -- (\x,{-0.1});
      }
      \foreach \y in {-1, 1, 2} {
        \draw (0,\y) node[left]{\y};
        \draw (-.1, \y) -- (.1, \y);
      }
      \draw[cyan, name path=F] plot [smooth, tension=0.5] coordinates {
        (-4,-1)
        (-3, 2.5)
        (0, 2.1)
        (2, .5)
        (4, 1.5)
        (5, .5)
        (6, 0)
      };
      \draw (0,0) node[below left]{$O$};
      \draw (1.5, 1.5) node[above right]{$\mathcal{C}_f$};

    \coordinate (X) at (3, 0);
    \coordinate (O) at (0, 0);
    \path[name path=DX] (3, 0) -- ++(0, 3);
    \path[name intersections={of=F and DX, by=FX}];
    \draw[dashed] (3, 0) -- (FX) -- (O |- FX);
    \end{tikzpicture}
\end{center}

On lit graphiquement : $f(3)\approx 0,9$.

\end{nexemple}

\begin{nmethode}
  Pour lire les antécédents de $a$ par $f$ :
  \begin{itemize}
    \item on repère $a$ sur l'axe des ordonnées ;
    \item on trace la droite parallèle à l'axe des abscisses, d'ordonnée $a$ ;
    \item on repère les points d'intersection de la courbe de $f$ et cette droite ;
    \item on lit les abscisses de ces points d'intersection : ce sont les antécédents de $a$ par $f$.
  \end{itemize}
\end{nmethode}

\begin{nexemple}Résolution graphique de $f(x)=1$ (c'est-à-dire recherche des antécédents de 1 par $f$).
\begin{center}
    \begin{tikzpicture}[very thick,xscale=.8, yscale=1.2]
      \draw[dotted, lightgray, xstep=1, ystep=.5] (-4, -1) grid (6, 3);
      \draw[-latex] (-4,0) -- (6,0);
      \draw[-latex] (0,-1) -- (0,3);
      \foreach \x in {-4, -3, -2, -1, 1, 2, 3, 4, 5, 6} {
        \draw (\x,0) node[below]{\x};
        \draw (\x,{0.1}) -- (\x,{-0.1});
      }
      \foreach \y in {-1, 1, 2} {
        \draw (0,\y) node[left]{\y};
        \draw (-.1, \y) -- (.1, \y);
      }
      \draw[cyan, name path=F] plot [smooth, tension=0.5] coordinates {
        (-4,-1)
        (-3, 2.5)
        (0, 2.1)
        (2, .5)
        (4, 1.5)
        (5, .5)
        (6, 0)
      };
      \draw (0,0) node[below left]{$O$};
      \draw (1.5, 1.5) node[above right]{$\mathcal{C}_f$};

    \coordinate (Y) at (0, 1);
    \coordinate (O) at (0, 0);
    \path[name path=DY] (-4, 1) -- ++(10, 0);
    \path[name intersections={of=F and DY}];
    \coordinate (I1)  at (intersection-1);
    \coordinate (I2)  at (intersection-2);
    \coordinate (I3)  at (intersection-3);
    \coordinate (I4)  at (intersection-4);
    \draw[dashed] (I1) -- (I4);
    \draw[dashed] (I1) -- (I1 |- O);
    \draw[dashed] (I2) -- (I2 |- O);
    \draw[dashed] (I3) -- (I3 |- O);
    \draw[dashed] (I4) -- (I4 |- O);
    \end{tikzpicture}
\end{center}

Les antécédents de 1 par $f$ sont donc environ : -3,6 ; 1,2 ; 3,1 et 4,6.
\end{nexemple}

\begin{remarque}
  Ces méthodes ne permettent que d'obtenir des valeurs approchées. Pour obtenir des valeurs exactes, le calcul de $f(x)$ ou la résolution algébrique de $f(x)=a$ sont nécessaires.
\end{remarque}

\pagebreak

\end{document}
