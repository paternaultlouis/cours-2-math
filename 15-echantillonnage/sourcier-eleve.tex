%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2018-2021 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[10pt]{article}

\usepackage{2021-pablo}
\usepackage{2021-pablo-paternault}
\usepackage{2021-pablo-math}

\usepackage{2021-pablo-listings}
\lstset{
  mathescape=true,
  language=python,
  frame=single,
  basicstyle=\ttfamily
}

\usepackage[a5paper, includehead, margin=5mm]{geometry}
\usepackage{2021-pablo-devoir}
\fancyhead[L]{\textsc{Ch. 15 --- Échantillonnage}}
\fancyhead[R]{\textsc{Esprit critique}}

\renewcommand{\reponse}{\emph{Réponse : .......................}}

\renewcommand{\blanc}[1]{\textcolor{white}{#1}}

\usepackage{multicol}
\usepackage{numworks}


\begin{document}

\begin{exercice}
  Un «~sourcier~» accepte de se préter à une expérience pour prouver son pouvoir : il essaye de déterminer si de l'eau traverse un tuyau.

  Sur 50 essais, il a trouvé la réponse correcte 30 fois.
  Le sourcier a-t-il apporté la preuve de son pouvoir ?

  Pour répondre à cette question, nous allons voir si une personne soumise au même test que le sourcier, mais répondant au hasard, pourrait obtenir un aussi bon taux de réussite que le prétendu sourcier.

  \begin{enumerate}
    \item En annonçant au hasard si le seau contient ou non de l'eau, quelle est la probabilité d'obtenir la bonne réponse ? \reponse
    \item Simuler une expérience : sur la calculatrice, tirer un nombre entre 0 et 1 au hasard.
	    Dans le menu \enquote{Calculs}, bouton \numworks{\textbf{T}}, puis \enquote{Aléatoire et approximation}, \enquote{random()}.

      Si ce nombre est supérieur à 0,5, compter une bonne réponse (succès) ; sinon, compter une mauvaise réponse (échec). \reponse
    \item Le sourcier a eu droit à 50 essais. Simuler 50 expériences (en répétant les mêmes instructions qu'à la question précédente), et noter les résultats ci-dessous. Dans chaque case, mettre un cercle $\circ$ en cas de succès, et une croix $\times$ en cas d'échec.
      \begin{center}
        \begin{tabular}{*{25}{|c}|}
          \hline
          &&&&&&&&&&&&&&&&&&&&&&&&\\
          \hline
          &&&&&&&&&&&&&&&&&&&&&&&&\\
          \hline
        \end{tabular}
      \end{center}
      \emph{Nombre total de succès obtenus : \ldots}
    \item Partager les résultats avec l'ensemble de la classe (pour plus de lisibilité, laisser vide les cases contenant 0).
      \begin{center}
        \setlength\tabcolsep{1.5pt} % default value: 6pt
        \begin{tabular}{|l|*{17}{|c}|}
          \hline
          Succès & 0&1&2&3&4&5&6&7&8&9&10&11&12&13&14&15&16\\
          \hline
          Effectif &&&&&&&&&&&&&&&&& \\
          \hline
          \hline
          Succès & 17&18&19&20&21&22&23&24&25&26&27&28&29&30&31&32&33\\
          \hline
          Effectif &&&&&&&&&&&&&&&&& \\
          \hline
          \hline
          Succès & 34&35&36&37&38&39&40&41&42&43&44&45&46&47&48&49&50\\
          \hline
          Effectif &&&&&&&&&&&&&&&&& \\
          \hline
        \end{tabular}
      \end{center}
    \item Combien de simulations ont produit autant ou plus de succès que le sourcier ? \reponse
    \item Le sourcier a-t-il apporté la preuve de son pouvoir ?
  \end{enumerate}
\end{exercice}

\newpage
\begin{exercice}
  L'Assemblée nationale est composée (en juin 2021) de 576 députés, dont 237 femmes\footnote{Source : \emph{Liste des députés répartis par sexe}, Assemblée Nationale, \url{http://www2.assemblee-nationale.fr/deputes/liste/homme-femme} (consulté le 08/06/2021).},
  alors qu'il y avait (toujours en juin 2021) sur les listes électorales 48\% de femmes\footnote{Source : \emph{Insee Focus \no{241}}, \textsc{Insee}, juin 2021, \url{https://relais.insee.info/wp-content/uploads/2021/06/479-millions-delecteurs-inscrits-sur-les-listes-electorales-francaises-en-mai-2021-Insee-Focus-241.pdf}.}.
  La question que l'on se pose est : La proportion de femmes à l'Assemblée est-elle normale, ou sont-elles sous-représentées ?

  \begin{enumerate}
    \item Quelle est la proportion de femmes à l'Assemblée nationale ?
    \item \emph{Un seul échantillon.} On choisit au hasard un échantillon de 576 personnes dans la population française.
	    \begin{multicols}{2}
      \begin{enumerate}
        \item Quelle est la taille de l'échantillon ? Quelle est la probabilité qu'un individu choisi au hasard parmi les français de la population française soit une femme ?
	\item Compléter le programme en Python ci-contre pour qu'il simule cet échantillon, et renvoie la proportion de femmes.
\item Exécutez cette fonction plusieurs fois. Dans ces échantillons, y a-t-il plus ou moins de femmes qu'à l'Assemblée nationale ?
      \end{enumerate}

\columnbreak

        \begin{lstlisting}[]
            from random import random
            
            def assemblée_hasard():
                femmes = 0
		for i in range($\blanc{576}$):
		    if random() < $\blanc{.48}$:
			$\blanc{femmes = femmes + 1}$
		return $\blanc{femmes/576}$
	\end{lstlisting}
	    \end{multicols}
      \item\emph{Plusieurs échantillons.} On souhaite répéter un grand nombre de fois la simulation de la question précédente.
	      \begin{multicols}{2}
		  \begin{enumerate}
			  \item Compléter la fonction ci-contre pour qu'elle simule \texttt{N} échantillons de taille 576, et renvoie la proportion de ces échantillons dont la proportion de femmes est inférieure à celle de l'Assemblée nationale.
\item Exécuter cette fonction avec un \texttt{N} \enquote{assez grand}. Quelle proportion d'échantillons ont une proportion de femmes inférieure à celle de l'Assemblée nationale ?
		  \end{enumerate}
		      \columnbreak
        \begin{lstlisting}[]
            def plusieurs_echantillons(N):
                plus_petits = 0
                for i in range(N):
		    if $\blanc{assemblée_hasard() < 237/576}$:
			$\blanc{plus_petits = plus_petits + 1}$
		return $\blanc{plus_petits / N}$
	\end{lstlisting}
	      \end{multicols}
    \item Conclure : Les femmes sont-elles sous-représentées à l'Assemblée nationale ?
  \end{enumerate}
\end{exercice}


\end{document}
