from math import *
from collections import *

coordonnées = namedtuple("coordonnées", ["x", "y"])

def vecteur(A, B):
    x = B.x - A.x
    y = B.y - A.y
    return coordonnées(x, y)

def colinéaires(u, v):
    déterminant = u.x * v.y - u.y * v.x
    if déterminant == 0:
        return True
    else:
        return False

def alignés(A, B, C):
    AB = vecteur(A, B)
    BC = vecteur(B, C)
    return colinéaires(AB, BC)

def parallèles(A, B, C, D):
    AB = vecteur(A, B)
    CD = vecteur(C, D)
    return colinéaires(AB, CD)

A = coordonnées(0, 0)
B = coordonnées(4, 3)
E = coordonnées(2, 1.5)
C = coordonnées(-1, 1)
D = coordonnées(-5, -2)

u = coordonnées(2, 3)
v = coordonnées(-4, -6)
w = coordonnées(1, 5)

print(vecteur(A, B))
print(colinéaires(u, v), True)
print(colinéaires(u, w), False)
print(alignés(A, B, C), False)
print(alignés(A, B, E), True)
print(parallèles(A, B, C, D), True)
print(parallèles(A, B, C, E), False)

