from math import *

def lire_point(nom):
    print(nom)
    return (
        float(input("x=? ")),
        float(input("y=? ")),
        )

def distance(A, B):
    return sqrt((A[0]-B[0])**2 + (A[1]-B[1])**2)

A = lire_point("A")
B = lire_point("B")
C = lire_point("C")

AB = distance(A, B)
BC = distance(B, C)
AC = distance(A, C)

print(AB, AC, BC)
if AC == AB or AB == BC or AC == BC:
    print("Le triangle est isocèle.")
else:
    print("Le triangle n'est pas isocèle.")
