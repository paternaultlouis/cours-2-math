# Généralités sur les fonctions

## Définition (2h30)

- Cours :
  - Fonction
  - Vocabulaire : Fiche
  - Courbe représentative

- Exercices
  - 30, 32
  - etc.

## Équations (2h30)

- Résolution algébrique
  - Cours : Fiche
  - Exemple : Fiche

- Résolution graphique (équations seulement)
  - Équations : Activité d'intro
  - Cours : Fiche
  - Exemples : Fiche

- Exercices
  - 22
  - 23, 25

## Fonctions paires et impaires (2h)

- Intro : Fiche
- Cours : Fiche

## Calculatrice (1h)

Fiche
