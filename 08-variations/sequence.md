# Variation de fonctions

- Introduction : activite-intro-court.pdf
- Cours : Variations cours-variations.pdf
- Exercices : 28 p. 81 (tracé) ; 30 p. 91 (lecture)
- Preuve : Variations de -2x+3
- Exemple : Comparer  f(4) et f(-5)
- Cours : Extremums
- Exemple : Lecture sur un tableau de var
- Exemple : Preuve avec une fonction trinôme sous forme canonique.
- Exercices : Pas grand'chose : les automatismes en couvrent pas mal

# Inéquations f(x)<k, f(x)<g(x)

- Cours : cours-inequations.pdf
- Exercices application directe : 26 p. 80, 38 p. 82, 39 p. 82.
