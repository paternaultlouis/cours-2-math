import itertools
import random
import sys

def f(x):
    return x**3-10*x**2+27*x-15

print("""Résolution de x^3-10*x^2+27^x-15=0 par dichotomie""")
print("""=================================================""")
print()
print("""f(0) = {}""".format(f(0)))
print("""f(8) = {}""".format(f(8)))
print()

def lit_nombre():
    while True:
        reponse = input("Proposer une valeur de x (ou Q pour quitter) ? ")
        if reponse.strip().lower() == "q":
            print("Fin du programme.")
            sys.exit(0)
        try:
            return float(reponse)
        except ValueError:
            print("Nombre invalide...")

try:
    for compteur in itertools.count(1):
        essai = lit_nombre()
        print("""Essai {}: f({}) = {}""".format(compteur, essai, f(essai)))
except EOFError:
    print("Abandon...")
