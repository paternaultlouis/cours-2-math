%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2018-2022 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{2223-pablo}
\usepackage{2223-pablo-paternault}
\usepackage{2223-pablo-tikz}
\usepackage{2223-pablo-math}

\usepackage[a4paper, margin=5mm]{geometry}

\usepackage{multicol}
\setlength{\columnseprule}{1pt}

\renewcommand\thesection{\alph{section}.}

\newcommand{\unecourbe}{%
    \begin{tikzpicture}[ultra thick,xscale=.6, yscale=1.4]
      \draw[dotted, lightgray, xstep=1, ystep=.5] (-4, -1) grid (6, 3);
      \draw[-latex] (-4,0) -- (6,0);
      \draw[-latex] (0,-1) -- (0,3);
      \foreach \x in {-4, -3, -2, -1, 1, 2, 3, 4, 5, 6} {
        \draw (\x,0) node[below]{\x};
        \draw (\x,{0.1}) -- (\x,{-0.1});
      }
      \foreach \y in {-1, 1, 2} {
        \draw (0,\y) node[left]{\y};
        \draw (-.1, \y) -- (.1, \y);
      }
      \draw [cyan] plot [smooth, tension=0.5] coordinates {
        (-4,-1)
        (-3, 2.5)
        (0, 2.1)
        (2, .5)
        (4, 1.5)
        (5, .5)
        (6, 0)
      };
      \draw (0,0) node[below left]{$O$};
      \draw (1.5, 1.5) node[above right]{$\mathcal{C}_f$};
    \end{tikzpicture}%
}
\newcommand{\deuxcourbes}{%
    \begin{tikzpicture}[very thick,xscale=.6, yscale=1.4]
      \draw[dotted, lightgray, xstep=1, ystep=.5] (-4, -1) grid (6, 3);
      \draw[-latex] (-4,0) -- (6,0);
      \draw[-latex] (0,-1) -- (0,3);
      \foreach \x in {-4, -3, -2, -1, 1, 2, 3, 4, 5, 6} {
        \draw (\x,0) node[below]{\x};
        \draw (\x,{0.1}) -- (\x,{-0.1});
      }
      \foreach \y in {-1, 1, 2} {
        \draw (0,\y) node[left]{\y};
        \draw (-.1, \y) -- (.1, \y);
      }
      \draw [cyan] plot [smooth, tension=0.5] coordinates {
        (-4,-1)
        (-3, 2.5)
        (0, 2.1)
        (2, 1)
        (3, 1.5)
        (4, .5)
        (6, 0)
      };
      \draw [brown] plot [smooth, tension=0.5] coordinates {
        (-4,1)
        (-3, 2)
        (0, 1.5)
        (1, 1.5)
        (3, 2)
        (4, .5)
        (6, -1)
      };
      \draw (0,0) node[below left]{$O$};
      \draw (-3, 2.5) node[above left]{$\mathcal{C}_g$};
      \draw (3, 2) node[above right]{$\mathcal{C}_h$};
    \end{tikzpicture}%
}

\begin{document}

\pagebreak

\section{Par le calcul}

\begin{methode*}[Résolution algébrique (par le calcul)]~
  \begin{itemize}
    \item Pour déterminer l'image de $x$ par $f$, on calcule $f(x)$ en remplaçant $x$ par sa valeur dans la formule de $f$.
    \item Pour trouver les antécédentes de $a$ par $f$, on résout l'équation $f(x)=a$ : on remplace $f(x)$ par son expression, et on résout l'équation ainsi obtenue.
  \end{itemize}
\end{methode*}

\begin{exemple}
  Soient les fonctions définies sur $\mathbb{R}$ par :
  \[f:x\mapsto 2x-1 \text{ et } g:x\mapsto -x+5\]

\begin{multicols}{2}
  \begin{enumerate}
    \item
      \begin{enumerate}
        \item Calculer l'image de 3 par $f$.
        \item Calculer $g(-1)$.
      \end{enumerate}
    \item
      \begin{enumerate}
        \item Quels sont les antécédents de 2 par $g$ ?
        \item Résoudre $f(x)=2$.
      \end{enumerate}
      \columnbreak
    \item Résoudre $f(x)\geq1$.
    \item Résoudre $f(x)=g(x)$.
    \item Résoudre $f(x)< g(x)$.
  \end{enumerate}
  \end{multicols}
\end{exemple}

\section{Lecture graphique}

\begin{methode*}[Résolution graphique]
  Pour lire une valeur approchée de l'image de $x$ par $f$ :
  \begin{itemize}
    \item on repère $x$ sur l'axe des abscisses ;
    \item on trace la droite parallèle à l'axe des ordonnées d'abscisse $x$ ;
    \item on repère le point $M$, intersection de la courbe de $f$ et de la droite précédente ;
    \item on lit l'ordonnée de ce point $M$ : c'est l'image de $x$ par $f$.
  \end{itemize}
\end{methode*}

\begin{methode*}[Résolution graphique]
  Pour lire une valeur approchée des antécédents de $a$ par $f$ :
  \begin{itemize}
    \item on repère $a$ sur l'axe des ordonnées ;
    \item on trace la droite parallèle à l'axe des abscisses, d'ordonnée $a$ ;
    \item on repère les points d'intersection de la courbe de $f$ et cette droite ;
    \item on lit les abscisses de ces points d'intersection : ce sont les antécédents de $a$ par $f$.
  \end{itemize}
\end{methode*}

\begin{multicols}{3}
\begin{exemple}[Lire une image]~
\begin{enumerate}
\item Quelle est l'image de 1 par la fonction $f$ ?
\item Lire graphiquement $f(-2)$.
\end{enumerate}
\unecourbe
\end{exemple}

\columnbreak

\begin{exemple}[Lire un antécédent]~
\begin{enumerate}
\item Résoudre graphiquement $f(x)=2$.
\item Quels sont les antécédents de $-0,5$ par $f$ ?
\item Quels sont les antécédents de 3 par $f$ ?
\end{enumerate}
\unecourbe
\end{exemple}

\columnbreak

\begin{exemple}[Résoudre une équation de type $f(x)=g(x)$]~

Résoudre $f(x)=g(x)$.

\deuxcourbes

\end{exemple}
\end{multicols}

\end{document}
