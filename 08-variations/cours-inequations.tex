%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2020-2022 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[12pt]{report}

\usepackage{2122-pablo}
\usepackage{2122-pablo-paternault}
\usepackage{2122-pablo-math}
\usepackage{2122-pablo-tikz}

\usepackage[
	a5paper,
	margin=7mm,
	]{geometry}

\pagestyle{empty}

\begin{document}
\begin{propriete*}~
  \begin{itemize}
	  \item Les solutions de l'inéquation $f(x)\geq k$ sont \blanc{les abscisses des points de la courbe de $f$ situés \emph{au dessus} de la courbe}.
	  \item Les solutions de l'inéquation $f(x)\leq k$ sont \blanc{les abscisses des points de la courbe de $f$ situés \emph{au dessous} de la courbe}.
  \end{itemize}
  \end{propriete*}
  \begin{methode*}[Résolution graphique de l'inéquation $f(x)\geq k$]~
  \begin{enumerate}
  \item Tracer la droite d'équation $y=k$ (elle est parallèle à l'axe des abscisses).
  \item Repérer les points de la courbe de $f$ situés au dessus de cette droite.
  \item Lire leurs abscisses.
  \end{enumerate}
  \end{methode*}

  \begin{exemple}
  On a tracé la courbe de la fonction $f$ définie sur $\intervallef{-2;5}$. Résoudre graphiquement $f(x)\geq2$.
  \begin{tikzpicture}[very thick, xscale=.9, yscale=.5]
    \draw[dotted, lightgray, xstep=1, ystep=1] (-2, -3) grid (5, 3);
    \draw[-latex] (-2,0) -- (5,0);
    \draw[-latex] (0,-3) -- (0,3);
    \foreach \x in {-2, -1, ..., 5} {
      \ifthenelse{\equal{\x}{0}}{
      }{
        \draw (\x,0) node[below]{\x};
        \draw (\x,{0.1}) -- (\x,{-0.1});
      }
    }
    \foreach \y in {-3, -2, ..., 3} {
      \ifthenelse{\equal{\y}{0}}{
      }{
        \draw (0,\y) node[left]{\y};
        \draw (-.1, \y) -- (.1, \y);
      }
    }
    \draw [cyan] plot [smooth, tension=0.6] coordinates {
      (-2, 1)
      (0, 3)
      (1, -1)
      (2, 0)
      (3, -3)
      (4, -1)
      (5, 3)
    };
    \draw (0,0) node[below left]{$O$};
    %\draw[red] (-2, 2) -- (5,2);
    %\begin{scope}[dashed, red]
    %  \draw (-1.3, 2) -- +(0, -2) node[shift={(-.2, -.3)}, color=red]{$-1,3$};
    %  \draw (0.4, 2) -- +(0, -2) node[below, color=red]{$0,4$};
    %  \draw (4.75, 2) -- +(0, -2) node[shift={(-.2, -.3)}, color=red]{$4,7$};
    %\end{scope}
  \end{tikzpicture}

  %\textcolor{red}{On trace la droite d'équation $y=2$. Il y a deux « morceaux » de courbe situés au dessus de la droite : de $-1,3$ à $0,4$ d'une part, et après $4,7$ d'autre part. Les solutions de l'inéquation sont donc $x\in\intervallef{-1,3;0,4}\cup\intervallef{4,7;5}$.}
  \end{exemple}

\begin{propriete*}~
  \begin{itemize}
	  \item Les solutions de l'inéquation $f(x)\geq g(x)$ sont \blanc{les abscisses $x$ pour laquelle l'image de $f$ est supérieure à l'image de $g$}.
	  \item Les solutions de l'inéquation $f(x)\leq g(x)$ sont \blanc{les abscisses $x$ pour laquelle l'image de $f$ est inférieure à l'image de $g$}.
  \end{itemize}
  \end{propriete*}
  \begin{methode*}[Résolution graphique de l'inéquation $f(x)\geq g(x)$]~
  \begin{enumerate}
  \item Repérer les points de la courbe de $f$ situés au dessus de la droite $g$.
  \item Lire leurs abscisses.
  \end{enumerate}
  \end{methode*}

  \begin{exemple}
  On a tracé les courbes des fonctions $f$ et $g$, définies sur $\intervallef{-2;5}$. Résoudre graphiquement $f(x)\geq g(x)$.
  \begin{center}
  \begin{tikzpicture}[very thick, xscale=1, yscale=.5]
    \draw[dotted, lightgray, xstep=1, ystep=1] (-2, -3) grid (5, 3);
    \draw[-latex] (-2,0) -- (5,0);
    \draw[-latex] (0,-3) -- (0,3);
    \foreach \x in {-2, -1, ..., 5} {
      \ifthenelse{\equal{\x}{0}}{
      }{
        \draw (\x,0) node[below]{\x};
        \draw (\x,{0.1}) -- (\x,{-0.1});
      }
    }
    \foreach \y in {-3, -2, ..., 3} {
      \ifthenelse{\equal{\y}{0}}{
      }{
        \draw (0,\y) node[left]{\y};
        \draw (-.1, \y) -- (.1, \y);
      }
    }
    \draw [cyan] plot [smooth, tension=0.6] coordinates {
      (-2, 1)
      (0, 3)
      (1, -1)
      (2, 0)
      (3, -3)
      (4, -1)
      (5, 3)
    };
    \draw [green] plot [smooth, tension=0.6] coordinates {
      (-2, -2)
      (1, 2)
      (3, 3)
      (5, -1)
    };
    \draw (-2, 3) node[anchor=north west]{$\mathcal{C}_f$};
    \draw (-2, -3) node[anchor=south west]{$\mathcal{C}_g$};
    \draw (0,0) node[below left]{$O$};
    \begin{scope}[dashed, red]
      %\draw (0.5, 0) node[below, color=red]{$0,5$} -- +(0, 1.4) node{$\bullet$} ;
      %\draw (4.4, 0) node[below, color=red]{$4,5$} -- +(0, .4) node{$\bullet$};
    \end{scope}
  \end{tikzpicture}
  \end{center}
  %{
  %        \color{red}
  %        On repère les « morceaux » de courbes pour lesquels la courbe de $f$ est \emph{au dessus} de celle de $g$. Il y en a deux : avant $0,5$, et après $4,5$.

  %        Les solutions sont donc $x\in\intervallef{-2;0,5}\cup\intervallef{4,5;5}$.
  %}
  \end{exemple}

\end{document}
