%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2018 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename
\documentclass[11pt]{article}


\usepackage{1920-pablo}
\usepackage{1920-pablo-cours}
\usepackage{1920-pablo-paternault}
\usetikzlibrary{intersections}

\usepackage[a5paper, top=.5cm, bottom=.5cm, inner=2cm, outer=.5cm]{geometry}

\begin{document}

\setcounter{section}{2}
\section{Représentations graphiques}

\subsection{Nuage de points}

\begin{definition}[Nuage de points]
  Un nuage de points est l'ensemble des points $\left( x_i; n_i \right)$, où $x_i$ sont les valeurs des caractères, et $n_i$ les effectifs.
\end{definition}
\begin{nexemple}
Tracer le nuage de points de la série suivante.

    \begin{center}
      \begin{tabular}{c||c|c|c|c|c|c}
        Nombre d'habitants &
        1 & 2 & 3 & 4 & 5 & 6 et plus
        \\
        \hline
        Fréquence (en \%) &
        22,1 &
        27,6 &
        19,1 &
        15,3 &
        8,2 &
        7,6
        \\
      \end{tabular}
    \end{center}
  Répartition des résidences principales selon le nombre d'habitants (source : \textsc{Insee}). Lecture : \emph{« 19,1\% des résidences principales en France sont occupées par 3 habitants »}.

\begin{center}
\begin{tikzpicture}[yscale=.06, xscale=1.4]
  \foreach \x/\y in {1/22.1, 2/27.6, 3/19.1, 4/15.3, 5/8.2, 6/7.6 } {
    \draw (\x, \y) node{$\times$};
  }
    \draw[->] (0,0) -- (6.5, 0);
    \draw[->] (0,0) -- (0, 32);
    \foreach \x in {1, ..., 6} {
      \draw (\x,0) node{|} node[below]{\x};
    }
    \foreach \y in {10, 20, 30} {
      \draw (0, \y) node{-} node[left]{\y};
    }
    \draw (0, 0) node[below left]{0};
    \draw (3.5, 0) node[below=.5cm]{Nombre d'habitants};
    \draw (0, 20) node[left=.6cm, rotate=90, anchor=south]{Fréquence (\%)};
  \end{tikzpicture}
\end{center}
\end{nexemple}

\subsection{Histogramme}

\begin{definition}[Histogramme]
  Un \emph{histogramme} permet de représenter graphiquement des données regroupées par classes.

  Le plus souvent, les classes ont la même étendue, et les hauteurs des barres sont proportionnelles aux caractères.
\end{definition}

\begin{nexemple}On considère les précipitations à Vienne, en 2013, en millimètres.
  \begin{center}
  \begin{tabular}{l*{6}{|c}}
    Mois & Jan. & Fév. & Mar. & Avr. & Mai & Juin \\
  Précipitations (mm) & 51,8 & 39,5 & 74,4 & 87,1 & 199,3 & 23,9 \\
  \hline
    Mois & Juil. & Août & Sep. & Oct. & Nov. & Déc. \\
  Précipitations (mm) & 74,8 & 46 & 84,6 & 84,5 & 110,8 & 96,8\\
  \end{tabular}
\end{center}
  Source : Wikipédia (\url{https://fr.wikipedia.org/wiki/Vienne_(Isère)}, d'après Météo France).

  \begin{center}
    \begin{tikzpicture}
      \begin{axis}[
          yscale=0.4,
          ymin=0,
          %ymax=40,
          xmin=1,
          xmax=13,
          xlabel={Mois de l'année 2013},
          ylabel={Précip. (mm)},
          ybar interval,
          %xticklabels={Jan, Fév, Mar, Avr, Mai, Jui, Jui, Aoû, Sep, Oct, Nov, Déc},
        ]
        \addplot coordinates {
          (1, 51.8)
          (2, 39.5)
          (3, 74.4)
          (4, 87.1)
          (5, 199.3)
          (6, 23.9)
          (7, 74.8)
          (8, 46)
          (9, 84.6)
          (10, 84.5)
          (11, 110.8)
          (12, 96.8)
          (13, 0)
        };
        \draw (axis cs:2, 0) node[below]{2};
        \draw[red, fill=green](0, 0) rectangle (axis cs: 2, 0);
      \end{axis}
      %\foreach \x in {1, 8, ..., 50} {
        %\draw[thick] ({.12286*\x-0.1429}, 0) node[below]{\x};
    %}
    \end{tikzpicture}
  \end{center}

\end{nexemple}

\subsection{Courbe des effectifs cumulés}

\begin{methode}[Courbe des effectifs (ou fréquences) cumulés croissants]
Relier l'ensemble des points $(x_i; n_i)$, où $x_i$ sont les valeurs, et $n_i$ les effectifs cumulés croissants.
\end{methode}

\begin{nexemple}
On considère la série statistique suivante.

  \resizebox{.9\linewidth}{!}{
  \begin{tabular}{l|*{7}{|c}}
    Âges & $\left[ 0;20 \right[$
      & $\left[ 20;30 \right[$
      & $\left[ 30;40 \right[$
      & $\left[ 40;50 \right[$
      & $\left[ 50;60 \right[$
      & $\left[ 60;70 \right[$
      & $\left[ 70;120\right[$
        \\
    \hline
    Effectifs
      & \numprint{8017}
      & \numprint{3830}
      & \numprint{4244}
      & \numprint{4436}
      & \numprint{4512}
      & \numprint{4215}
      & \numprint{5399}
      \\
(milliers) &&&&&&& \\
\hline
Fréquences (\%) &&&&&&& \\
\hline
Fréq. cumulées    &&&&&&& \\
croissantes &&&&&&& \\
                  \end{tabular}
                }
\begin{enumerate}
\item Compléter la ligne \emph{Effectifs cumulés croissants}.
  \emph{À faire vous-même.}
\item Tracer la courbe des effectifs cumulés croissants.
  \begin{center}\begin{tikzpicture}[xscale=.08, yscale=.03]
      \draw[name path=FCC]
  (0, 0) node{$\times$}
  -- (20, 23.1) node{$\times$}
  -- (30, 34.2) node{$\times$}
  -- (40, 46.4) node{$\times$}
  -- (50, 59.2) node{$\times$}
  -- (60, 72.3) node{$\times$}
  -- (70, 84.4) node{$\times$}
  -- (120, 100) node{$\times$}
    ;
    \draw[->] (0,0) -- (120, 0);
    \draw[->] (0,0) -- (0, 105);
    \foreach \x in {10, 20, ..., 110} {
      \draw (\x,0) node{|} node[below]{\x};
    }
    \foreach \y in {20, 40, ..., 100} {
      \draw (0, \y) node{-} node[left]{\numprint{\y}};
    }
    \coordinate (O) at (0, 0);
    \draw (O) node[below left] {0};
    \draw (120, 0) node[above=.5cm, anchor=south east]{Âge};
    \draw (0, 20) node[left=.6cm, rotate=90, anchor=south]{Fréquence (\%)};

    \path[name path=yQ1] (0, 25) -- ++(120, 0);
    \path[name intersections={of=FCC and yQ1,by=Q1}];
    \draw[dashed] (0, 25) -- (Q1) -- (Q1 |- O);

    \path[name path=yQ2] (0, 50) -- ++(120, 0);
    \path[name intersections={of=FCC and yQ2,by=Q2}];
    \draw[dashed] (0, 50) -- (Q2) -- (Q2 |- O);

    \path[name path=yQ3] (0, 75) -- ++(120, 0);
    \path[name intersections={of=FCC and yQ3,by=Q3}];
    \draw[dashed] (0, 75) -- (Q3) -- (Q3 |- O);

  \end{tikzpicture}\end{center}
\item Lire graphiquement la valeur de la médiane et des premier et troisième quartiles.

  \begin{description}
    \item[Premier quartile] On trace donc la droite d'équation $y=25$ (droite horizontale d'ordonnée \numprint{25}), et on lit l'abscisse du point d'intersection de cette droite avec la courbe. On obtient environ 22, donc le premier quartile est environ 25\%.
    \item[Médiane] On fait la même chose avec 50, et on obtient une médiane d'environ 43\%.
    \item[Troisième quartile] Enfin, avec 75, on obtient un troisième quartile d'environ 63\%.
  \end{description}
\end{enumerate}
\end{nexemple}


\end{document}
