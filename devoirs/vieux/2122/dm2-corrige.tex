%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2020-2021 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{2122-pablo}
\usepackage{2122-pablo-paternault}
\usepackage{2122-pablo-math}
\usepackage{2122-pablo-tikz}

\usepackage[includehead, a5paper, margin=11mm]{geometry}
\usepackage{2122-pablo-header}
\fancyhead[L]{\textsc{Mathématiques 2\up{de}{}}}
\fancyhead[C]{\textsc{DM \no2 --- Corrigé}}
\fancyhead[R]{13/11/2021}
\fancyfoot[C]{}

\begin{document}

\begin{exercice}[Inéquations]
  \begin{em}
  Une usine de conditionnement de soupe reçoit du bouillon de la part d'un fournisseur, et des légumes en poudre de la part d'un autre fournisseur. Elle assemble les deux pour fabriquer sa soupe.

  Une cuve a été remplie avec \SI{100}{L} de bouillon. L'objet de l'exercice est de déterminer la quantité de légumes à ajouter pour fabriquer la soupe, sachant que :

  \begin{itemize}
    \item La cuve de bouillon contient déjà \SI{50}{g} de sel, et \SI{0}{g} de sucre.
    \item Chaque kilogramme de légumes en poudre contient \SI{3,2}{g} de sel, et \SI{1,4}{g} de sucre.
    \item La soupe (le bouillon mélangé aux légumes) doit contenir \emph{au moins} \SI{150}{g} de sel (pour la conservation), et \emph{au maximum} \SI{50}{g} de sucre (pour relever le goût).
  \end{itemize}

  Dans la suite de l'exercice, tous les poids sont exprimés en grammes.
\end{em}

  \begin{enumerate}
    \item \emph{On ajoute $x$ kilogrammes de légumes dans le bouillon. Montrer que la quantité de sucre dans la soupe est $1,4x$, et que la quantité de sel est $50+3,2x$}

      Puisque chaque kilogramme $x$ de légumes amène \SI{1,4}{g} de sucre au bouillon qui n'en contient pas encore, la quantité de sucre est $1,4x$.

      Puisque le bouillon contient déjà \SI{50}{g} de sel, et que l'on y ajoute \SI{3,2}{g} par kilogramme $x$ de légumes, la quantité de sel est alors $50+3,2x$.
    \item \emph{En déduire que pour respecter les contraintes, les relations suivantes doivent être respectées :}
      \[
        1,4x\leq50\text{ et }50+3,2x\geq150
      \]

      La quantité de sucre doit être inférieure à \SI{50}{g}, donc $1,4x\leq50$.

      La quantité de sel doit être supérieure à \SI{150}{g}, donc $50+3,2x\geq150$.
    \item \emph{Résoudre les deux inéquations $1,4x\leq50$ et $50+3,2x\geq150$, et représenter les solutions sous la forme d'un seul intervalle.}

      \begin{align*}
        1,4x&\leq50 & 50+3,2x&\geq150\\
        x&\leq\frac{50}{1,4} & 3,2x&\geq100\\
         &                   &    x&\geq\frac{100}{3,2}\\
      \end{align*}
      Cela donne, en arrondissant au dixième de gramme : $x\leq35,7$ et $x\geq31,3$.

      \begin{center}
        \begin{tikzpicture}[scale=.5,ultra thick]
          \draw[-Stealth] (25, 0) -- (40, 0);
          \draw[blue, -Fermé, yshift=-12pt] (25, 0) -- ({50/1.4}, 0) node[below]{$35,7$} node[midway, below]{$x\leq35,7$};
          \draw[Green, -Fermé, yshift=12pt] (40, 0) -- ({100/3.2}, 0) node[above]{$31,3$} node[midway, above]{$x\geq31,3$};
        \end{tikzpicture}
      \end{center}
      Donc l'ensemble des solutions est $x\in\intervallef{31,3;35,7}$.
    \item \emph{Conclure par une phrase en français : Quel poids de légume peut-on verser dans la cuve de bouillon pour respecter les contraintes ?}

      Pour respecter les contraintes, on peut ajouter entre \SI{31,3}{g} et \SI{35,7}{g} de légumes.
  \end{enumerate}
\end{exercice}

\begin{exercice}[Repérage]
  \begin{em}
	Dans un repère orthonormé, on considère les points $A(-1; 2)$, $B(2;6)$, $C(10; 0)$.
\end{em}
  \begin{enumerate}
    \item \emph{Tracer un repère, et placer les points $A$, $B$, $C$, sur ce repère.}
      Voir en fin d'exercice.
    \item
      \begin{enumerate}
        \item \emph{Calculer les coordonnées du milieu $I$ de $[AC]$.}

          Puisque $I$ est le milieu de $\left[ AC \right]$, alors :

          \begin{align*}
            x_I&=\frac{x_A+x_C}{2} & y_I&=\frac{y_A+y_C}{2}\\
               &=\frac{-1+10}{2}   &    &=\frac{2+0}{2}\\
               &=\frac{9}{2}       &    &=1\\
               &=4,5
          \end{align*}

          Donc les coordonnées du milieu $I$ sont $D\left( \numprint{4,5};1 \right)$.
        \item \emph{Déterminer les coordonnées de $D$, symétrique de $B$ par rapport à $I$.}

          Puisque $D$ est le symétrique de $B$ par rapport à $I$, alors $I$ est le milieu de $[BD]$, donc :

          \begin{align*}
            x_I&=\frac{x_B+x_D}{2} & y_I&=\frac{y_B+y_D}{2}\\
            4,5&=\frac{2  +x_D}{2} & 1  &=\frac{6  +y_D}{2}\\
            2\times4,5&=2  +x_D & 2\times1  &=6  +y_D\\
            9&=2  +x_D & 2  &=6  +y_D\\
            9-2&=x_D & 2-6  &=y_D\\
            7&=x_D & -4  &=y_D\\
          \end{align*}

          Donc les coordonnées de $D$ sont $D\left( 7;-4 \right)$.

        \item \emph{En déduire que $ABCD$ est un parallélogramme.}

          Puisque $I$ est à la fois le milieu de $[AC]$ et $[BD]$, alors les diagonales du quadrilatère $ABCD$ se coupent en leur milieu : $ABCD$ est un parallélogramme.
      \end{enumerate}
\item
  \begin{enumerate}
    \item \emph{On admet que $AB=5$ et $AC=5\sqrt{5}$. Calculer la longueur $BC$.}

      \begin{align*}
BC
&= \sqrt{\left( x_C-x_B \right)^2+\left( y_C-y_B \right)^2}\\
&= \sqrt{\left( 10 -2   \right)^2+\left( 0  -6   \right)^2}\\
&= \sqrt{8^2+\left( -6 \right)^2}\\
&= \sqrt{64 +36}\\
&= \sqrt{100}\\
&=10
      \end{align*}
    \item \emph{Montrer que le triangle $ABC$ est rectangle en $B$.}

      Vérifions si le triangle est rectangle en utilisant la réciproque du théorème de Pythagore.

      \begin{itemize}
        \item D'une part : $AC^2=\left( 5\sqrt{5} \right)^2=5^2\times\sqrt{5}^2=25\times5=125$.
        \item D'autre part : $AB^2+BC^2=5^2+10^2=25+100=125$.
      \end{itemize}

      Donc $AC^2+AB^2+BC^2$, et d'après la réciproque du théorème de Pythagore, le triangle $ABC$ est rectangle en $B$.
    \item \emph{Préciser la nature du parallélogramme $ABCD$ (est-ce un rectangle ? un losange ? un carré ? un parallélogramme quelconque ?).}

      \begin{itemize}
        \item Nous avons déjà dit que $ABCD$ est un parallélogramme.
        \item Puisque le triangle $ABC$ est rectangle en $B$, alors l'angle $\widehat{ABC}$ est droit, et $ABCD$ est un rectangle.
        \item Puisque $AB\neq BC$, alors les quatre côtés du parallélogramme ne sont pas égaux : ce n'est pas un losange.
      \end{itemize}
  \end{enumerate}
\item \emph{Sans aucun calcul (mais toujours sans lecture graphique), dire si les droites $[AC]$ et $[BD]$ sont perpendiculaires.}

  D'après la question précédente, $ABCD$ n'est pas un losange, donc ses diagonales $[AC]$ et $[BD]$ ne sont pas perpendiculaires.
  \end{enumerate}

  \begin{center}
    \begin{tikzpicture}[scale=1,very thick]
      \draw[gray, dotted] (-1.5, -4.5) grid (10.5, 6.5);
      \draw[-latex] (-1.5, 0) -- (10.5, 0);
      \draw[-latex] (0, -4.5) -- (0, 6.5);
      \draw (0, 0) node[below left]{$\mathcal{O}$};
      \draw (1, 0) node[below]{1};
      \draw (0, 1) node[left]{1};

      \coordinate (A) at (-1, 2);
      \coordinate (B) at (2,6);
      \coordinate (C) at (10,0);
      \coordinate (I) at ($.5*(A)+.5*(C)$);
      \coordinate (D) at ($2*(I)-(B)$);

      \draw (A) node{$\times$} node[left]{$A$};
      \draw (B) node{$\times$} node[above]{$B$};
      \draw (C) node{$\times$} node[below right]{$C$};
      \draw (D) node{$\times$} node[below]{$D$};
      \draw (I) node{$\times$} node[below left]{$I$};

      \draw (A) -- (B) -- (C) -- (D) -- cycle;
      \draw[dashed] (A) -- (C);
      \draw[dashed] (B) -- (D);
    \end{tikzpicture}
  \end{center}
\end{exercice}

\end{document}
