from matplotlib import pyplot
import pandas
import seaborn

SPÉCIALITÉS = {
    #"Art": "art",
    #"Biologie-écologie": "bio",
    "Histoire-géographie, géopolitique et sciences politiques": "HG",
    "Humanités, littérature et philosophie": "HLP",
    "Langues, littératures et cultures étrangères": "LLCE",
    "Littérature, langues et cultures de l'Antiquité": "LLCA",
    "Mathématiques": "math",
    #"Numérique et sciences informatiques": "NSI",
    "Physique-Chimie": "PC",
    #"Science de l'Ingénieur": "SI",
    "Sciences de la vie et de la Terre": "SVT",
    "Sciences économiques et sociales": "SES",
    #"Sport": "sport",
}

QUESTIONS = {
    "Vous êtes...": "genre",
    "J'habite à": "adresse",
    "Mes moyennes générales se situent": "moyennes",
    "Mes parents...": "parents",
    "Option #1: l'option qui me parait la plus importante": "spé1",
    "Option #2: la seconde option qui me parait la plus importante": "spé2",
    "Option #3: la troisième option que je choisis": "spé3",
    "Option #4: l'option que j'hésite encore à prendre": "spé4",
    "Si je souhaite aller dans le technologique": "techno",
    "J'ai choisi ces spécialités parce qu'elles me permettront l'orientation que je souhaite.": "orientation",
    "J'ai choisi ces spécialités parce qu'elles me plaisaient le plus.": "plaisent",
    "J'ai choisi ces spécialités parce qu'elles sont celles où je pense avoir les meilleures notes.": "notes",
    "J'ai choisi ces spécialités après de longues discussions avec ma famille.": "famille",
    "J'ai choisi ces spécialités parce qu'elles sont celles où je pense avoir les meilleures notes..1": "note2",
    "J'ai choisi ces spécialités parce qu'elles sont celles où je pense retrouver mes amis.": "amis",
    "J'ai choisi ces spécialités parce qu'elles sont les moins pires.": "pires",
}


def lit_fichier(fichier):
    données = pandas.read_csv("sondage-2-378.csv", sep=",")
    données.rename(columns=QUESTIONS, inplace=True)

    # Ajout d'une colonne sur le choix de la série : techno ou générale
    données["série"] = [""] * len(données)
    données["techno"] = données["techno"].fillna("")
    for indice in range(len(données["genre"])):
        if données["techno"][indice]:
            données.loc[indice, "série"] = "techno"
        else:
            données.loc[indice, "série"] = "générale"

    # Ajout de colonnes sur le choix des spécialités
    for spé in SPÉCIALITÉS.values():
        données[spé] = [""] * len(données)
    for indice in range(len(données["genre"])):
        if données["techno"][indice]:
            # L'élève choisit une série technologique. On l'ignore.
            continue

        for spé in SPÉCIALITÉS:
            données.loc[indice, SPÉCIALITÉS[spé]] = spé in (
                données[question][indice]
                for question in ("spé1", "spé2", "spé3", "spé4")
            )

    return données


def reverse_dict(arg):
    for key, value in (SPÉCIALITÉS | QUESTIONS).items():
        if value == arg:
            return key
    return arg


def trace(données, x, y, *, titre=None):
    print("\n" + "#" * 80)

    print("### Données brutes")
    groupées = données.groupby([x])[y]
    print(groupées.value_counts())

    print("\n### Pourcentages")
    pourcentages = (
        groupées.value_counts(normalize=True)
        .rename("Pourcentage")
        .mul(100)
        .reset_index()
        .sort_values(x)
    )
    print(
        groupées.value_counts(normalize=True)
        .rename("Pourcentage")
        .mul(100)
        .reset_index()
        .sort_values(x)
    )

    graphique = seaborn.barplot(x=x, y="Pourcentage", hue=y, data=pourcentages)
    graphique.axes.set_xlabel(reverse_dict(x))
    graphique.axes.set_ylabel(reverse_dict(y))
    if titre:
        graphique.axes.set_title(titre)
    pyplot.show()


données = lit_fichier("sondage-2-378.csv")

# Raison des choix
for notation in (
    "orientation",
    "plaisent",
    "notes",
    "famille",
    #"note2",
    "amis",
    "pires",
):
    trace(données, x="série", y=notation)

# Choix de série en fonction d'un déterminisme
for déterminisme in ("genre", "adresse", "moyennes", "parents"):
    trace(données, x=déterminisme, y="série")

# Choix d'une spécialité en fonction d'un déterminisme
for spé in SPÉCIALITÉS.values():
    for déterminisme in ("genre", "adresse", "moyennes", "parents"):
        trace(
            données.loc[(données["série"] == "générale"), :],
            x=déterminisme,
            y=spé,
        )
