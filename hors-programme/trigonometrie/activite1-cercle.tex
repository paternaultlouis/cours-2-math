%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2018 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename
\documentclass[12pt]{article}


\usepackage{1920-pablo}
\usepackage{1920-pablo-paternault}
\usetikzlibrary{decorations.pathreplacing}

\usepackage[color=blackandwhite]{graph35}

\usepackage[margin=1cm, a5paper]{geometry}

\newcommand{\reponse}[1]{
      \raisebox{\depth}{\rotatebox{180}{
        \parbox{\linewidth}{
          #1
        }
      }}
    }

\title{Angles et Arcs}
\date{}

\begin{document}

\maketitle

\section{Longueur d'un arc de cercle}

    Une petite fille joue avec un train électrique, roulant sur des rails disposés en cercle de rayon \SI{1}{m}. On modélise le problème dans un repère orthonormé $\left( O, I, J \right)$, où $O$ est le centre du cercle. Les points $I'$ et $J'$ sont les symétriques des points $I$ et $J$ par le point $O$.

    Les bissectrices des angles $\widehat{IOJ}$ et $\widehat{JOI'}$ coupent le cercle de centre $O$ et de rayon 1~cm en $A$, $B$, $C$ et $D$.

    Le train, que l'on considère ponctuel (aussi petit qu'un point), part du point $I$ et parcourt le cercle dans le sens inverse des aiguilles d'une montre.

    \begin{center}
  \begin{tikzpicture}[scale=1.3]
    \draw(0,0) circle (1);
    \foreach \i in {0, 4, ..., 360} {
      \draw (\i: .94) -- (\i: 1.06);
    }
    \draw (0,-1.12) -- (0,1.12);
    \draw (-1.12,-1.12) -- (1.12,1.12);
    \draw (-1.12,0) -- (1.12,0);
    \draw (1.12,-1.12) -- (-1.12,1.12);
    \draw (0:1) node[above right]{$I$};
    \draw (90:1) node[above right]{$J$};
    \draw (180:1) node[above left]{$I'$};
    \draw (270:1) node[below left]{$J'$};
    \draw (45:1) node[right]{$A$};
    \draw (135:1) node[above]{$B$};
    \draw (225:1) node[left]{$C$};
    \draw (315:1) node[below]{$D$};
    \draw (0,0) node[below left]{$O$};

    \draw[-latex] (1.5, 0) arc (0:35:1.5);
  \end{tikzpicture}
\end{center}

\begin{em}
  Dans toute cette partie, on ne manipulera que des mètres. On s'autorise donc à ne pas indiquer l'unité de longueur.
\end{em}

\begin{exercice}~
  \begin{enumerate}
    \item Quelle distance le train a-t-il parcouru quand il revient en $I$ après un tour complet ?

      \reponse{Le train a parcouru la circonférence du cercle, de rayon 1m : il a donc parcouru $2\pi\times1=2\pi$.}

    \item Quelle distance aura-t-il parcouru quand il arrive pour la première fois en $I'$ ? en $J$ ?

      \reponse{En $I'$, le train à parcouru la moitié de la circonférence, soit $\frac{2\pi}{2}=\pi$.}

      \reponse{En $J$, le train a parcouru le quart de la circonférence, soit $\frac{2\pi}{4}=\frac{\pi}{2}$.}
  \end{enumerate}
    \end{exercice}

      On remarque ici que la distance parcoure est proportionnelle à l'angle formé avec le centre $O$. Si le train parcourt un tour complet, soit 360°, il aura parcour une distance de $2\pi$.

      \begin{nexemple}
        \emph{Quelle distance le train aura-t-il parcouru quand il arrive pour la première fois en $A$ ?}

        Remarquons d'abord que l'angle $\widehat{IOA}$ mesure 45°.

        Le tableau suivant est un tableau de proportionnalité.
        \begin{center}\begin{tabular}{l||c|c}
            Angle & 360 & 45 \\
            \hline
            Distance & $2\pi$ & \\
          \end{tabular}\end{center}

        Donc la distance parcourue sera : $\frac{45\times2\pi}{360}=\frac{90\times\pi}{90\times4}=\frac{\pi}{4}$.
      \end{nexemple}

      \begin{nexemple}
        \emph{Le train parcourt une distance de $1,5\pi$ pour arriver au point $M$. Combien mesure l'angle $\widehat{IOM}$ ?}

        Nous utilisons le même tableau de proportionnalité.

        \begin{center}\begin{tabular}{l||c|c}
            Angle & 360 &  \\
            \hline
            Distance & $2\pi$ & $1,5\pi$ \\
          \end{tabular}\end{center}

        Donc l'angle $\widehat{IOM}$ mesure $\frac{1,5\pi\times360}{2\pi}=\frac{1,5\pi\times180\times2}{2\pi}=1,5\times180=270$, soit 270°.
      \end{nexemple}

      \begin{exercice}
        \emph{Utiliser le tableau de proportionnalité des deux exemples précédents.}
        \begin{enumerate}
          \item Quelle distance aura parcouru le train lorsqu'il arrive en C ? en J' ? en B ?
          \item En quel point arrive le train après avoir parcouru $\frac{\pi}{2}$ ? de $\frac{3\pi}{2}$ ? de $\frac{7\pi}{4}$ ?
        \end{enumerate}
      \end{exercice}

      \section{Sens de rotation}
      On prend la convention suivante : une longueur positive correspond à un déplacement dans le sens inverse des aiguilles d'une montre ; une longueur négative correspond à un déplacement dans le sens des aiguilles d'une montre.

      \begin{nexemple}~
        \begin{itemize}
          \item Le train parcourt une distance de $2\pi$ : il part de $I$, passe par $A$, $J$, etc. jusqu'à revenir à $I$.
          \item Le train parcourt une distance de $-2\pi$ : il part de $I$, passe par $D$, $J'$, etc. jusqu'à revenir à $I$.
        \end{itemize}
        Dans les deux cas, il a parcouru $2\pi$, mais le sens de rotation change.
      \end{nexemple}

      \begin{nexemple}~
        \begin{em}
        \begin{itemize}
          \item Le train arrive en $C$ dans le sens des aiguilles d'une montre. Quelle distance a-t-il parcouru ?
          \item Le train arrive en $D$. Donner deux distances possibles qu'il ait parcouru.
        \end{itemize}
      \end{em}
      \begin{itemize}
        \item Reprenons le tableau de proportionnalité, en remarquant que l'angle $\widehat{IOC}$ mesurent 135°.
        \begin{center}\begin{tabular}{l||c|c}
            Angle & 360 & 135 \\
            \hline
            Distance & $2\pi$ & \\
          \end{tabular}\end{center}
        La distance parcourue est donc $\frac{135\times2\pi}{360}=\frac{135\times2}{360}\pi=\frac{3}{4}\pi$. Mais cette distance est parcourue dans le sens des aiguille d'une montre, donc elle est négative : elle vaut $-\frac{3}{4}\pi$.
      \item Si le train se déplace dans le sens des aiguilles d'une montre, l'angle $\widehat{IOD}$ mesure 315°, donc il aura parcouru $\frac{7}{8}\pi$ (il faut utiliser le tableau de proportionnalité pour faire le calcul).

        Si au contraire le train se déplace dans le sens inverse des aiguilles d'une montre, l'angle $\widehat{IOD}$ ne mesure alors que 45°, donc le train aura parcouru $-\frac{\pi}{8}$.

        Donc suivant le sens de rotation du train, il y a deux manières d'arriver jusqu'au point $D$ (et il y en a encore d'autres).
      \end{itemize}
      \end{nexemple}

\end{document}
