%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2018-2023 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{2223-pablo}
\usepackage{2223-pablo-paternault}
\usepackage{2223-pablo-math}
\usepackage{2223-pablo-tikz}

\usepackage[
	a5paper,
	margin=9mm,
	includehead,
	headsep=3mm,
	]{geometry}
\usepackage{2223-pablo-header}
\usepackage{lastpage}
\fancyhead[L]{\textsc{Équations de droites}}
\fancyhead[R]{\textsc{\thepage/\pageref{LastPage}}}

\usepackage{multirow}
\newcommand{\repere}{
  \begin{tikzpicture}[scale=0.55,baseline=1cm,very thick]
    \draw[dotted,gray] (-1.9,-1.9) grid (3.9,3.9);
    \draw (-2,0) -- (4,0);
    \draw (0,-2) -- (0,4);
    \draw (0,0) node[below left]{$\mathcal{O}$};
    \draw (1,0) node[]{$|$} node[below]{$I$};
    \draw (0,1) node[]{$-$} node[left]{$J$};
  \end{tikzpicture}
}

\usepackage{emoji}

\begin{document}

\section{Équation réduite de droite}

\begin{propriete*}[Caractérisation analytique d'une droite]
  Soit un repère du plan $(O, \vecteur{\imath}, \vecteur{\jmath})$, et $d$
  une droite de ce repère.

  \begin{itemize}
    \item Si $d$ est parallèle à l'axe des ordonnées, alors elle admet une
	    unique équation de la forme \blanc{$x=c$}, où $c$ est un réel.
    \item Sinon, elle admet une unique équation de la forme \blanc{$y=mx+p$}, où $m$ et
      $p$ sont des réels.
  \end{itemize}
\end{propriete*}

\begin{propriete*}[Réciproque]
  Soit un repère $(O, \vecteur{\imath}, \vecteur{\jmath})$.
  \begin{itemize}
    \item Étant donné un réel $c$, l'ensemble des points du plan
	    vérifiant l'équation $x=c$ est une droite \blanc{parallèle à l'axe des ordonnées}.
    \item Étant donnés deux réels $m$ et $p$, l'ensemble des points du plan
	    vérifiant l'équation $y=mx+p$ est une droite \blanc{non parallèle à l'axe des ordonnées}.
  \end{itemize}
\end{propriete*}

\begin{propriete*}[Déterminer une équation de droite]
  Soit $d$ la droite passant par les points $A$ et $B$ (dans un repère orthonormé).
  \begin{description}
	  \item[Premier cas : $x_A=x_B$] Si $A$ et $B$ ont la même abscisse, alors l'équation de la droite est \blanc{$x=x_A$}.
	  \item[Secound cas : $x_A\neq x_B$] Si $A$ et $B$ n'ont pas la même abscisse, alors l'équation de la droite est de la forme \blanc{$y=mx+p$}, où $m=\blanc{\frac{y_B-y_A}{x_B-x_A}}$ est le \blanc{coefficient directeur}, et $p$ est \blanc{l'ordonnée à l'origine}.
  \end{description}
\end{propriete*}

\begin{exemple}
  Déterminer l'équation réduite des droites $(AB)$ et $(AC)$, avec $A(2; -1)$, $B(8, 2)$ et $C(2; 3)$.
\end{exemple}

\subsection{Tracé de droites}

\begin{methode*}[Équation de la forme $y=mx+p$]~
\begin{itemize}
  \item Choisir une première abscisse $x$ (par exemple $x=0$), et calculer l'ordonnée $mx+p$ pour cette valeur. Placer le point correspondant.
  \item Refaire la même chose avec une autre abscisse $x$.
  \item Tracer la droite passant par ces deux points.
\end{itemize}
\end{methode*}

\begin{methode*}[Équation de la forme $x=c$]~
\begin{itemize}
  \item Repérer, sur l'axe des abscisses, l'abscisse $c$.
  \item Tracer la droite parallèle à l'axe des ordonnées passant par ce point.
\end{itemize}
\end{methode*}

\begin{exemple}
Dans un repère orthonormé, tracer les droites d'équation :
	\begin{enumerate*}[label=$D_\arabic*$ : ]
		\item $y=\frac{x}{2}-1$ ;
		\item $x=5$ ;
		\item $x=-2$ ;
		\item $y=-1,5x+2$.
	\end{enumerate*}

%\begin{center}
%  \begin{tikzpicture}[ultra thick]
%      \draw[dotted,color=gray] (-3,-2) grid (10,4);
%      \draw[] (-3,0) -- (10,0);
%      \draw[] (0,-2) -- (0,4);
%
%      \draw (0,0) node[below left]{$\mathcal{O}$};
%      \draw (1,0) node[below]{$I$};
%      \draw (0,1) node[left]{$J$};
%  \end{tikzpicture}
%\end{center}
\end{exemple}

\subsection{Lecture graphique d'équations}

\begin{methode*}~
	\begin{itemize}
		\item Si la droite est parallèle à l'axe des ordonnées, alors l'équation est de la forme $x=c$.

\begin{itemize}
  \item Lire les coordonnées du point d'interesction de la droite avec l'axe des abscisses.
  \item L'équation de la droite est $x=c$, où $c$ est l'ordonnée du point d'intersection.
\end{itemize}
\item Si la droite n'est pas parallèle à l'axe des ordonnées, alors l'équation est de la forme $y=mx+p$.

\begin{itemize}
  \item Considérer un point quelconque sur la droite. Considérer le point de la droite dont l'abscisse est une unité de plus que le point précédent. Le coefficient directeur $m$ est la différence des ordonnées de ces deux points.
  \item L'ordonnée à l'origine est l'ordonnée du point d'intersection de la droite avec l'axe des ordonnées.
\end{itemize}
	\end{itemize}
\end{methode*}

\pagebreak

\begin{exemple}\label{ex:lecture}
Déterminer par lecture graphique les équations des droites suivantes.

\begin{center}
  \begin{tikzpicture}[ultra thick]
      \draw[dotted,color=gray] (-3,-3) grid (10,3);
      \draw[] (-3,0) -- (10,0);
      \draw[] (0,-3) -- (0,3);

      \draw (0,0) node[below left]{$\mathcal{O}$};
      \draw (1,0) node[below]{$1$};
      \draw (0,1) node[left]{$1$};

      \draw[blue] (-1, 3) -- (5, -3);
      \draw (1, 1) node[above right]{$d_1$};
      \draw[blue] (7, -3) -- (7, 3);
      \draw (7, 1) node[above right]{$d_2$};
	  \draw[blue] ({3+1/3}, -3) -- ({5+1/3}, 3);
      \draw (4, -1) node[above right]{$d_3$};
      \draw[blue] (-2, -3) -- (-2, 3);
      \draw (-2, 1) node[above left]{$d_4$};
      \draw[blue] (-3, -2) -- (8, 3);
      \draw (-1, -1) node[below right]{$d_5$};
  \end{tikzpicture}
\end{center}
\end{exemple}

\section{Équation cartésienne de droite}

\begin{defprop*}~
	\begin{itemize}
		\item Pour tous nombres réels $a$, $b$, $c$ (tels qu'au moins $a$ ou $b$ est non nul), l'ensemble des points $M(x;y)$ vérifiant l'équation $ax+by+c=0$ (appelée \emph{équation cartésienne}) forme une droite.
		\item Réciproquement, toute droite est définie par une \emph{équation cartésienne} de la forme  $ax+by+c=0$, où $a$, $b$ et $c$ sont des nombres réels.
	\end{itemize}
\end{defprop*}

\begin{exemple}
	Dans un repère orthonormé, tracer les droites définies par les équations suivantes :

	\begin{enumerate*}[label=$D_\arabic*$ : ]
		\item $2x-y+1=0$ ;
		\item $3x+9=0$.
	\end{enumerate*}
\end{exemple}

\begin{exemple}
	On considère la droite $d$ d'équation cartésienne $5x+7y-10=0$.
	\begin{enumerate}
		\item Les points suivants appartiennent-ils à $d$ ?

			\begin{enumerate*}[$A$]
				\item $(2;-3)$ ;
				\item $(1;1)$ ;
				\item $(-1;4)$.
			\end{enumerate*}
		\item Tracer la droite $d$ dans un repère.
	\end{enumerate}
\end{exemple}

\pagebreak

\section{Vecteur directeur d'une droite}

\begin{definition*}
	On appelle \emph{vecteur directeur} d'une droite tout vecteur $\vecteur{AB}$, où $A$ et $B$ sont deux points distincts de la droite.
\end{definition*}

\begin{exemple}
	Déterminer les coordonnées de \emph{plusieurs} vecteurs directeurs pour chacune des droites $d_1$ à $d_4$ de l'exemple \ref{ex:lecture}.
\end{exemple}

\begin{exemple}
	Dans un repère, tracer les droites suivantes :
	\begin{enumerate}[label=$D_\arabic*$ : ]
		\item de coefficient directeur $\vecteur{u}\left(0 ; 2\right)$, passant par $A\left(2 ; -1\right)$ ;
		\item de coefficient directeur $\vecteur{v}\left(-3 ; 1\right)$, passant par $B\left(-3 ; 2\right)$ ;
		\item de coefficient directeur $\vecteur{w}\left(5 ; -2\right)$, passant par $C\left(0 ; 1\right)$.
	\end{enumerate}
\end{exemple}

\begin{propriete*}
	Pour droite définie par une équation cartésienne $ax+by+c=0$, le vecteur de coordonnées $\coord{-b}{a}$ est un vecteur directeur de cette droite.
\end{propriete*}

\begin{exemple}
	Déterminer les coordonnées d'un vecteur directeur de chacune des droites suivantes.
	\begin{enumerate*}[label=$D_\arabic*$ : ]
		\item $3x+4y-2=0$ ;
		\item $2x+12y+2=0$ ;
		\item $\frac{x}{2}-7y-1=0$ ;
		\item $x+5=0$ ;
	\end{enumerate*}
\end{exemple}

\begin{propriete*}
	Deux vecteurs non nuls sont colinéaires si et seulement si ce sont les vecteurs directeurs d'une même droite.
\end{propriete*}

\begin{exemple}[\emoji{heart}]
	Déterminer une équation cartésienne de la droite de coefficient directeur $\vecteur{u}\left(3 ; -2\right)$ passant par $A\left(6 ; -1\right)$.
\end{exemple}


\section{Position relative de droites}

\begin{propriete*}
  Soient un repère $(O, \vecteur\imath, \vecteur\jmath)$, et deux droites $d$ et $d'$. Les propositions suivantes sont équivalentes.
  \begin{enumerate}[(i)]
    \item Les droites $d$ et $d'$ sont parallèles.
    \item Si les droites ne sont pas parallèles à l'axe des ordonnées : les droites $d$ et $d'$ ont même coefficient directeur.
    \item Les droites $d$ et $d'$ ont deux vecteurs directeurs colinéaires.
  \end{enumerate}
\end{propriete*}

\pagebreak

\begin{propriete*}[Position relative]~

	\begin{center}
\begin{tabular}{p{3cm}||c|c}
  Équation de $\left(d_1\right)$ & $x=c_1$ & $y=mx+p$ \\
  \hline
  Équation de $\left(d_2\right)$ & $x=c_2$ & $x=c$    \\
  \hline
  \hline
  \multirow{2}{3cm}{Position relative de $\left(d_1\right)$ et $\left(d_2\right)$} & \multirow{2}{*}{} & \multirow{2}{*}{} \\[4em]
  && \\
  Représentation
  &\repere
  &\repere
   \\
   Exemple &
   $d_1$ : $x=1$ &
   $d_1$ : $y=\frac{1}{2}x+1$
   \\
   &
   $d_2$ : $x=3$ &
   $d_2$ : $x=1$
   \\
\end{tabular}

\vfill

\begin{tabular}{p{3cm}||c|c}
  Équation de $\left(d_1\right)$ & \multicolumn{2}{c}{$y=m_1x+p_1$} \\
  \hline
  Équation de $\left(d_2\right)$ & \multicolumn{2}{c}{$y=m_2x+p_2$} \\
  \hline
  \hline
  \multirow{2}{3cm}{Position relative de $\left(d_1\right)$ et $\left(d_2\right)$} & $m_1=m_2$ & $m_1\neq m_2$ \\[4em]
  && \\
  Représentation
  &\repere
  &\repere
   \\
   Exemple &
   $d_1$ : $y=\frac{1}{2}x+1$ &
   $d_1$ : $y=\frac{1}{2}x+1$
   \\
   &
   $d_2$ : $y=\frac{1}{2}x-1$ &
   $d_2$ : $y=2x-1$
   \\
\end{tabular}
	\end{center}
\end{propriete*}

\pagebreak

\begin{exemple}
	Dans le plan muni d'un repère, on donne les point suivants :
	$A(1;2)$,
	$B(-1;3)$,
	$C(25;-2)$,
	$D(14;-0,5)$.

	\begin{enumerate}
		\item Déterminer les coordonnées d'un vecteur directeur pour chacune des droites $(AB)$, $(BC)$, et $(AD)$.
		\item En utilisant les vecteurs directeurs, déterminer lesquelles des droites $(AB)$, $(BC)$ et $(AD)$ sont parallèles.
	\end{enumerate}
\end{exemple}

\section{Système d'équation}

\begin{propriete*}[Interprétation géométrique]
	Soit $(S)$ le système d'équations $\begin{cases}ax+by=c\\a'x+b'y=c'\end{cases}$, et $d$ et $d'$ les droites définies par chacune des deux équations de $(S)$. Les solutions de $(S)$ sont les coordonnées des éventuels points d'intersection de $d$ et $d'$.
\end{propriete*}

\begin{corollaire*}Avec le même système $(S)$, trois cas seulement sont possibles :
  \begin{itemize}
    \item Le systeme $(S)$ a une infinité de solutions si et seulement si les droites $d$ et $d'$ sont confondues.
    \item Le système $(S)$ a une unique solution si et seulement si les droites $d$ et $d'$ sont sécantes. Cette solution correspond aux coordonnées de l'unique point d'intersection entre $d$ et $d'$.
    \item Le système $(S)$ n'a pas de solutions si et seulement si les droites $d$ et $d'$ sont strictement parallèles.
  \end{itemize}
\end{corollaire*}

\begin{exemple}[\emoji{heart} Résolution par substitution]
	Déterminer l'éventuel point d'intersection des droites définies par les équations $-x+2y+8=0$ et $4x+3y+1=0$.
\end{exemple}

\begin{exemple}[\emoji{heart} Résolution par combinaison linéaire]
	On dispose de deux types d'haltères, rouges et bleues. Les haltères de même couleur pèsent le même poids. On sait que :
	\begin{itemize}
		\item deux haltères rouges et cinq haltères bleues pèsent \SI{31}{kg} ;
		\item trois haltères rouges et deux haltères bleues pèsent \SI{19}{kg}.
	\end{itemize}

	Déterminer le poids de chacun des deux types d'haltères.
\end{exemple}

\end{document}
