from math import *

def lire_point(nom):
    print(nom)
    return (
        float(input("x=? ")),
        float(input("y=? ")),
        )

def distance(A, B):
    return sqrt((A[0]-B[0])**2 + (A[1]-B[1])**2)

A = lire_point("A")
B = lire_point("B")
C = lire_point("C")

AB = distance(A, B)
BC = distance(B, C)
AC = distance(A, C)

if BC**2 == AC**2 + AB**2 or AB**2 == AC**2 + BC**2 or AC**2 == AB**2 + BC**2:
    print("Le triangle est rectangle.")
else:
    print("Le triangle n'est pas rectangle.")
