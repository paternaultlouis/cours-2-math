%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2019-2022 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{2223-pablo}
\usepackage{2223-pablo-paternault}
\usepackage{2223-pablo-math}
\usepackage{2223-pablo-tikz}

\usepackage[
	a5paper,
	includehead,
	margin=10mm,
	headsep=3mm,
]{geometry}
\usepackage{2223-pablo-header}
\fancyhead[L]{\textsc{Ch. 3 --- Repérage}}
\fancyhead[R]{\textsc{1 --- Repère}}

\usepackage{multicol}
\usepackage{emoji}

\newcommand{\repere}[3]{
	\begin{tikzpicture}[scale=1, very thick]
		\coordinate (O) at (0, 0);
		\coordinate (I) at #2;
		\coordinate (J) at #3;

		\clip (-.5, -.5) rectangle (2, 2);
		\begin{scope}[gray]
		\foreach \i in {-4, -3, -2, -1, 1, 2, 3, 4, 5} {
			\draw ($(O)!-4cm!(J) + \i*(I)$) -- ($(O)!4cm!(J) + \i*(I)$);
			\draw ($(O)!-4cm!(I) + \i*(J)$) -- ($(O)!4cm!(I) + \i*(J)$);
		}
		\end{scope}
		\draw ($(O)!-4cm!(I)$) -- ($(O)!4cm!(I)$);
		\draw ($(O)!-4cm!(J)$) -- ($(O)!4cm!(J)$);
		\draw (0, 0) node[below left]{$O$};
		\draw (I) node[below]{$I$};
		\draw (J) node[left]{$J$};
	\end{tikzpicture}
	}

\usepackage{tabularx}

\begin{document}

\begin{defprop*}[Repère et Coordonnées]~
	\begin{enumerate}[$\bullet$]
		\item On appelle \blanc{repère} la donnée de trois points du plan non alignés $(O, I, J)$.

			Le point $O$ est appelé \blanc{origine}, l'axe $(OI)$ est appelé \blanc{axe des abscisses}, l'axe $(OJ)$ est appelé \blanc{axe des ordonnées}.
		\item Si le triangle $OIJ$ est rectangle en $O$, le repère est dit \blanc{orthogonal} ; si le triangle $OIJ$ est isocèle en $O$, le repère est dit \blanc{normé} ; si le triangle $OIJ$ est rectangle isocèle en $O$, le repère est dit \blanc{orthonormé}.
		\item Tout point $M$ du plan est repéré par un unique couple de coordonnées $(x;y)$, où $x$ est \blanc{l'abscisse} de $M$, et $y$ est \blanc{l'ordonnée}.
	\end{enumerate}
\end{defprop*}

\begin{center}
\begin{tabularx}{\linewidth}{|XXXX|}
	\hline
	\repere{scale=.4}{(.6,0)}{(.3,.7)}&
	\repere{scale=.4}{(.4,0)}{(0,.7)}&
	\repere{scale=.4}{(1,0)}{(60:1)}&
	\repere{scale=.4}{(.7,0)}{(0,.7)}\\
	\blanc{quelconque}&
	\blanc{orthogonal}&
	\blanc{normé}&
	\blanc{orthonormé}\\
	\hline
\end{tabularx}
\end{center}

\begin{exemple}[Coordonnées]~
	\begin{enumerate}
		\item Pour chacun des repères $(O;I;J)$, $(A;B;C)$, $(J;A;C)$, donner :
	\begin{enumerate}
		\item la nature du repère ;
		\item les coordonnées de $A$ et $D$ ;
	\end{enumerate}
  \end{enumerate}

  \begin{multicols}{2}
  \begin{center}
    \begin{tikzpicture}[very thick, scale=.8]
      \draw[dotted, gray] (-1, -1) grid (6, 5);

      \draw (0,0) node{$\bullet$} node[below left]{$O$};
      \draw (1,0) node{$\bullet$} node[below]{$I$};
      \draw (0,1) node{$\bullet$} node[left]{$J$};

      \draw (2, 1) node{$\bullet$} node[below left]{$A$};
      \draw (3, 1) node{$\bullet$} node[below right]{$B$};
      \draw (2, 3) node{$\bullet$} node[above left]{$C$};

      \draw (4, 3) node{$\bullet$} node[above right]{$D$};
    \end{tikzpicture}
  \end{center}


  \begin{enumerate}[resume]
\item Placer les points $E$, $F$, $G$ :
	\begin{enumerate}
		\item $E(1;3)$ dans le repère $(O;I;J)$ ;
		\item $F(3;1)$ dans le repère $(A;B;C)$ ;
		\item $G(1;2)$ dans le repère $(J;A;C)$.
	\end{enumerate}
	\end{enumerate}
	\end{multicols}
\end{exemple}

\begin{propriete*}[Milieu d'un segment]
	Soient $A(x_A;y_A)$ et $B(x_B;y_B)$ deux points du plan muni d'un repère. Alors $I$ est le milieu du segment $[AB]$ si et seulement si :
	\vspace{1cm}
	%\blanc{$x_I=\frac{x_A+x_B}{2}$} et \blanc{$y_I=\frac{y_A+y_B}{2}$}.
\end{propriete*}

\begin{exemple}[\emoji{heart}]
	On donne  $A(1;1)$, $B(4;2)$, $C(5;4)$ et $D(2;3)$. Sans faire de figures, déterminer si $ABCD$ est un parallélogramme.
\end{exemple}

\begin{exemple}[\emoji{heart}]
  Déterminer les coordonnées de $M$, symétrique de $N(5; -1)$ par rapport à $K(2 ; 3)$.
\end{exemple}

\begin{exemple}[\emoji{heart}]
  On donne  $A(-1;1)$, $B(3;2)$, $C(0;10)$. Déterminer les coordonnées d'un point $D$ pour que $ABCD$ soit un parallélogramme.
\end{exemple}

\begin{propriete*}[Longueur d'un segment]
	Soient $A(x_A;y_A)$ et $B(x_B;y_B)$ deux points du plan muni d'un repère orthonormé. Alors la longueur du segment $[AB]$ (ou distance entre $A$ et $B$) est donnée par la formule :
	\vspace{1cm}
	%\[\sqrt{\left(x_A-x_B\right)^2+\left(y_A-y_B\right)^2}\]
\end{propriete*}

\begin{exemple}
	On donne trois points $A(1;4)$, $B(4;6)$, $C(7;1)$ dans le plan muni d'un repère ortonormé.
	\begin{enumerate}
		\item Placer le trois points dans un repère, puis conjecturez si le triangle $ABC$ est rectangle en $B$ ou non.
		\item Confirmez ou infirmez votre conjecture par le calcul.
	\end{enumerate}
\end{exemple}

\end{document}
