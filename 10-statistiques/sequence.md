# Statistiques descriptives

## Définition (1/2h)

- Cours : Fiche

## Médiane et Quartiles

- Cours : Fiche (1h)
- Exercices : 36, 37 p. 283
- Exercices : 61, 62 p. 289

## Calculatrice (15')

À la main…

## Moyenne et Écart-type

- Cours : Fiche (1h)
- Exercices : 52 p. 287

## Tableur (1h)

Linéarité de la moyenne
