#!/usr/bin/env python3

# Copyright 2019 Louis Paternault
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import functools
import math
import operator
import random

PREMIERS = [
    2,
    3,
    5,
    7,
    11,
    13,
    17,
    19,
    23,
    29,
    31,
    37,
    41,
    43,
    47,
    53,
    59,
    61,
    67,
    71,
    73,
    79,
    83,
    89,
    97,
]
POIDS = [1.5 ** power for power in range(len(PREMIERS), 0, -1)]
FACTEURS = 5


def nombres():
    return [
        functools.reduce(
            operator.mul, random.choices(PREMIERS, weights=POIDS, k=FACTEURS), 1
        )
        for i in range(2)
    ]


if __name__ == "__main__":
    print("### PGCD --- Énoncés")
    random.seed(42)
    for i in range(50):
        a, b = nombres()
        print(r"\item ${}$ et ${}$ ;".format(a, b))

    print("### PGCD --- Corrigés")
    random.seed(42)
    for i in range(50):
        a, b = nombres()
        print(r"\item le PGCD de ${}$ et ${}$ est ${}$;".format(a, b, math.gcd(a, b)))

    print("### Fractions --- Énoncés")
    random.seed(1729)
    for i in range(50):
        a, b = nombres()
        print(r"\item $\frac{{{}}}{{{}}}$ ;".format(a, b))

    print("### Fractions --- Corrigés")
    random.seed(1729)
    for i in range(50):
        a, b = nombres()
        pgcd = math.gcd(a, b)
        print(
            r"\item $\frac{{{a}}}{{{b}}} = \frac{{{a}\div {pgcd}}}{{{b}\div {pgcd}}} = \frac{{{aa}}}{{{bb}}}".format(
                a=a, b=b, pgcd=pgcd, aa=a // pgcd, bb=b // pgcd
            )
        )
