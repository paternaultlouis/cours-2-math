# Vecteurs

# Définition

- Introduction (fiche)
- Cours : Définition ; Égalité
- Exercice : 31 p. 184.
- Cours : Propriétés : Parallélogramme et Milieu
- Exercices : 36 et 38 p. 185.

# Somme et Produit

- Cours : Somme
- Exercice : 43 p. 185
- Exercice bilan sans coordonnées
- Cours : Produit
  - produit
  - opposé
  - soustraction
- Exercices 45 p. 209

# Coordonnées

- Cours : fiche
- Exercice : 49 p. 188, 19 p. 183, 56 p. 189, 58 p. 189
- Exercice bilan : exo-bilan-avec-coordonnees.pdf

# Algorithmique

TODO
