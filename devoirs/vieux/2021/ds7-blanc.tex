%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2017-2021 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{2021-pablo}
\usepackage{2021-pablo-math}
\usepackage{2021-pablo-paternault}
\usepackage{2021-pablo-tikz}

\usepackage[
	a5paper,
	includehead,
	margin=10mm,
	]{geometry}
\usepackage{2021-pablo-devoir}
\fancyhead[L]{\textsc{Math 2\up{de}{}}}
\fancyhead[C]{\textsc{DS \no7}}
\fancyhead[R]{\textsc{Fiche de révision}}
\fancyfoot[C]{}

\usepackage{multicol}
\usepackage{tabularx}

\begin{document}
\begin{center}\begin{tabularx}{\linewidth}{X<{\dotfill}@{}c}
    \toprule
    \multicolumn{2}{c}{\textbf{Méthodes à connaître}} \\
    \midrule
    \multicolumn{2}{c}{\emph{Fonctions affines (chapitre 3 du manuel)}} \\
      Tracer la courbe d'une fonction affine & 24 \\
      Déterminer l'expression d'une fonction affine par le calcul & 22 \\
      Même chose, mais par lecture graphique & 23 \\
      Dresser le tableau de variations d'une fonction affine & 25 \\
      Dresser le tableau de signes d'une fonction affine & 26 \\
      Faire la tableau de signes d'un produit ou d'un quotient & 73, 75 \\
    \midrule
    \multicolumn{2}{c}{\emph{Vecteurs (chapitre 7 du manuel)}} \\
      Déterminer si deux vecteurs sont colinéaires & 27 \\
      Déterminer si deux droites sont parallèles & 29 \\
      Déterminer si trois points sont alignés & 31 \\
      Calculs avec les vecteurs & 28 \\
      \bottomrule
  \end{tabularx}\end{center}

\begin{exercice}
On considère la fonction $f:x\mapsto -5x+15$, et la fonction $g$ dont on connait le tableau de signes suivant.

  \begin{center}
    \begin{tikzpicture}
      \tkzTabInit[lgt=2,espcl=2]
      {$x$ /1,
        $g(x)$ /1
      }
      {$-\infty$, -1, $\infty$}%
      \tkzTabLine{, +, z ,-}
    \end{tikzpicture}
  \end{center}

\begin{enumerate}
\item Dresser les tableaux de signe et de variations de $f$.
\item Compléter en utilisant l'un des quatre signes $<$, $>$, $=$ ou $?$ (s'il manque des informations pour répondre à la question).
\begin{multicols}{2}
\begin{enumerate}
\item $g(2)\ldots5$
\item $f(-3)\ldots0$
\item $f(6)\ldots g(1)$
\item $f(-1)\ldots g(-1)$
\end{enumerate}
\end{multicols}
\end{enumerate}
\end{exercice}

\begin{exercice}
	On considère une fonction affine $f$, dont on sait que $f(13)=12$ et $f(9)=17$.
  \begin{enumerate}
    \item Montrer que l'équation de la fonction $f$ est : $f:x\mapsto -1,25x+28,25$.
    \item Quelles sont les variations de $f$ ? Justifier.
    \item Le point $C(123; -125,5)$ est-il sur la courbe de $f$ ? Justifier.
  \end{enumerate}
\end{exercice}

\begin{exercice}
	La gestionnaire d'un lycée étudie plusieurs propositions pour l'achat de ramettes de papier dans un lycée.

	\begin{enumerate}[A]
		\item Chaque ramette est vendue à 2,54 €.
		\item Le lycée souscrit un contrat à 230 €, puis chaque ramette est ensuite vendue à 2,02 €.
	\end{enumerate}

	L'objet de l'exercice est de déterminer le contrat le plus avantageux en fonction du nombre de ramette consommées dans l'année.

	On admet que pour $x$ ramettes de papier consommées, le coût est donné par la fonction $A:x\mapsto 2,54x$ pour l'entreprise A, et $B:x\mapsto 2,02x+230$ pour l'entreprise B.

\begin{enumerate}
\item \emph{Résolution graphique}
\begin{enumerate}
\item Tracer les courbes des fonctions $A$ et $B$ sur le graphique ci-dessous.
\item Répondre par lecture graphique : À partir de combien de ramettes vendues le contrat $B$ sera-t-il plus avantageux que le contrat $A$ ?
\end{enumerate}
\begin{center}
\begin{tikzpicture}[xscale=2, yscale=1, ultra thick]
\draw[xstep=0.5, dotted, gray] (0, 0) grid (4, 5);
\draw[->] (0, 0) -- (4, 0);
\draw[->] (0, 0) -- (0, 5);
\draw (0, 0) node[below left]{$\mathcal{O}$};
\foreach \i in {1, ..., 8} {
	\draw ({.5*\i}, 0) node[below]{\i00};
}
\foreach \i in {4, 8, ..., 20} {
	\draw (0, {.25*\i}) node[left]{\i00};
}
\end{tikzpicture}
\end{center}
\item \emph{Résolution algébrique} On souhaite répondre à la même question, mais par le calcul.
  \begin{enumerate}
    \item Montrer que le contrat $A$ est plus avantageux que le contrat $B$ si et seulement si :
	    $0,52x-230\geq 0$.
    \item Dresser le tableau de signes de la fonction $x\mapsto 0,52x-230$ (arrondir les valeurs à l'unité si nécessaire).
    \item Conclure en lisant le tableau de signes : À partir de combien de ramettes de papier vendues le contrat $B$ est-il plus avantageux que le contrat $A$ ?
  \end{enumerate}
\end{enumerate}
\end{exercice}

\begin{exercice}L'objet de l'exercice est de résoudre l'inéquation : \[4x^2+x-1\geq-2x^2+1\]
	\begin{enumerate}
		\item \emph{Par lecture graphique.} On a tracé les courbes des deux fonctions définies par $f(x)=4x^2+x-1$ et $g(x)=-2x^2+1$.

\begin{center}
\begin{tikzpicture}[ultra thick,xscale=5, yscale=2.5]
\tikzset{coordonnees/.style={text opacity=1,color=black,fill=white,opacity=.7}};
\draw[dotted, gray, xstep=.25, ystep=.25] (-1, -1) grid (1, 1);
\draw[->] (-1, 0) -- (1, 0);
\draw[->] (0,-1) -- (0,1);
\draw(0, 0) node[below left]{0};
\foreach \x in {-1, -.5,.5, 1} {
	\draw (\x, 0) node[coordonnees,below]{\numprint{\x}};
}
\foreach \y in {-1, -.5,.5, 1} {
	\draw (0, \y) node[coordonnees,left]{\numprint{\y}};
}
	\clip (-1, -1.2) rectangle (1, 1.1);
\draw[domain=-1:1,smooth,blue] plot ({\x},{4*\x*\x+\x-1});
	\draw[blue] (.25, -.5) node[below right]{$\mathcal{C}_f$};
\draw[domain=-1:1,smooth,red] plot ({\x},{-2*\x*\x+1});
	\draw[red] (-.25, .5) node[above left]{$\mathcal{C}_g$};
\end{tikzpicture}
\end{center}

			Résoudre graphiquement l'inéquation.
		\item\emph{Par le calcul}
	\begin{enumerate}
		\item Montrer que l'inéquation $4x^2+x-1\geq-2x^2+1$ est équivalente à $6x^2+x-2\geq0$.
		\item Montrer que $6x^2+x-2=(2x-1)(3x+2)$.
		\item Dresser le tableau de signes de l'expression $(2x-1)(3x+2)$.
			\item En déduire les solutions de l'inéquation de départ.
	\end{enumerate}
	\end{enumerate}
\end{exercice}

\begin{exercice}
  Dans le plan muni d'un repère, on considère trois points $A(0;2)$, $B(-1;3)$ et $C\left( 2;2 \right)$, ainsi qu'un quatrième point $D$ tel que $\vecteur{BD}=2\vecteur{AB}+\vecteur{AC}$.
  \begin{enumerate}
    \item Montrer que les coordonnées de $D$ sont $\coord{-1}{5}$.
    \item Les droites $\left( AB \right)$ et $\left( CD \right)$ sont-elles parallèles ?
  \end{enumerate}
\end{exercice}

\begin{exercice}
  Dans le plan muni d'un repère, on considère trois points $A(1;3)$, $B(0;4)$ et $C\left( 3;3 \right)$, ainsi qu'un quatrième point $D$ tel que $\vecteur{AD}=2\vecteur{AB}-\vecteur{CB}$.
  \begin{enumerate}
    \item Montrer que les coordonnées de $\vecteur{AD}$ sont $\coord{1}{1}$.
    \item Montrer que les coordonnées de $D$ sont $\coord{2}{4}$.
    \item  Les points $A$, $D$, $C$ sont-ils alignés ? Justifier.
  \end{enumerate}
\end{exercice}

\begin{exercice}
	On considère un parallélogramme $ABCD$, et deux points $E$ et $F$ tels que :
	\begin{itemize}[$\bullet$]
		\item $E$ est le milieu de $[BC]$ ;
		\item $\vecteur{AF}=2\vecteur{DC}$.
	\end{itemize}
	L'objet de l'exercice est de montrer que $E$ est le milieu de $[DF]$.
	\begin{enumerate}
		\item Faire une figure.
		\item Justifier que $\vecteur{DA}=\vecteur{CB}$, et $\vecteur{CB}=2\vecteur{CE}$.
		\item On admet que d'après la relation de Chasles, on a : $\vecteur{DF}=\vecteur{DA}+\vecteur{AF}$. En déduire que $\vecteur{DF}=2\vecteur{CE}+2\vecteur{DC}$.
		\item On admet que d'après la relation de Chasles (encore), on a : $\vecteur{DC}+\vecteur{CE}=\vecteur{DE}$. En déduire que $\vecteur{DF}=2\vecteur{DE}$.
		\item Conclure.
	\end{enumerate}
\end{exercice}

\end{document}
