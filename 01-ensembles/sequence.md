# Ensembles de nombres et Intervalles

## Ensembles de nombres

- Introduction : intro-ensembles.tex
- Cours :
  - Ensembles de nombres
  - Symbole appartient à
- Exercices (rapide : 2, 5 p. 15 , premier exo du DM)

## Équations, Inéquations

- Programme de calcul ; Somme et produit
- Résoudre une équation / inéquation
- Exercices : 80, 81, 85, 86 p. 25

- Cours : Équation produit
- Exemple :
	a. Mq $(2x-3)(x+1) = 2x^2-x-3$
	b. Résoudre $2x^2-x-3=0$
- Exercice : 82 p. 25 => Automatismes
- Problème : exo-equation-produit

## Intervalles

- Introduction : activite-inequations.tex
- Cours : intervalle
- Exercices : 14, 15 p. 17
- Union et intersection : Activité d'intro
- Cours
- Problème : Modélisation (mise en inéquation, résolution, réunion d'intervalles)

## Valeur absolue

- Cours
  - Définition (avec la distance à 0)
  - Distance entre deux nombres réels
  - $|x-a|<=r <=> x\in[a-r;a+r]$

- Exercices : 24 p. 18
