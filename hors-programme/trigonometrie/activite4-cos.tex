%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2018 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{1920-pablo}
\usepackage{1920-pablo-paternault}
\usetikzlibrary{decorations.pathreplacing}

\usepackage[color=blackandwhite]{graph35}

\usepackage[margin=1cm, a5paper]{geometry}

\title{Sinus et Cosinus d'un nombre réel}
\date{}

\begin{document}

\maketitle

\section{Trigonométrie dans un triangle}

\begin{propriete}[Rappel de collège]~
  Dans un triangle $ABC$ rectangle en $A$, on a :
  \begin{multicols}{2}
    \begin{align*}
      \sin\widehat{ABC} &= \frac{AC}{BC} \\
      \cos\widehat{ABC} &= \frac{AB}{BC} \\
      \tan\widehat{ABC} &= \frac{AC}{AB} \\
    \end{align*}

    \begin{center}
      \begin{tikzpicture}[scale=1,very thick]
        \coordinate (A) at (0, 0);
        \coordinate (B) at (4, 0);
        \coordinate (C) at (0, 2);

        \draw (A) -- (B) -- (C) -- cycle;
        \draw (A) node[below left]{$A$};
        \draw (B) node[below right]{$B$};
        \draw (C) node[above left]{$C$};

        \draw ($(A)+(.2, 0)$) -- ++(0, .2) -- ++(-.2, 0);
        \draw ($(B)-(1.5, 0)$) arc (180:{180-atan(.5)}:1.5);
        \draw (2.5, .4) node[left]{$\widehat{ABC}$};
      \end{tikzpicture}
    \end{center}
  \end{multicols}
\end{propriete}

\begin{nexemple}
  \begin{em}
    On considère le triangle ci-contre, rectangle en $A$.

    \begin{multicols}{2}
      \begin{enumerate}
        \item Calculer la mesure de l'angle $\widehat{ABC}$, arrondi au dixième de degré.
        \item Calculer la longueur $AC$, arrondi au dixième (sans utiliser le théorème de Pythagore).
      \end{enumerate}

      \columnbreak

      \begin{center}
        \begin{tikzpicture}[scale=0.7,very thick]
          \coordinate (A) at (0, 0);
          \coordinate (B) at (4, 0);
          \coordinate (C) at (0, 2);

          \draw (A) -- (B) -- (C) -- cycle;
          \draw (A) node[below left]{$A$};
          \draw (B) node[below right]{$B$};
          \draw (C) node[above left]{$C$};

          \draw ($(A)+(.4, 0)$) -- ++(0, .4) -- ++(-.4, 0);
          \draw ($.5*(A)+.5*(B)$) node[below]{4};
          \draw ($.5*(C)+.5*(B)$) node[above]{5};
        \end{tikzpicture}
      \end{center}
    \end{multicols}
  \end{em}
  \begin{enumerate}
    \item On a : $\cos\widehat{ABC}=\frac{AB}{BC}=\frac{4}{5}$. À la calculatrice, on calcule \texttt{acos(4/5)} (touches \key{SHIFT}\key[shift]{cos}) et on obtient $\widehat{ABC}\approx36,9$°.
    \item On a : $\sin\widehat{ABC}=\frac{AC}{BC}$, donc $AC=BC\times\sin\widehat{ABC}=5\times\sin36,9=3,0$.
  \end{enumerate}
\end{nexemple}

\begin{exercice}~
  \begin{enumerate}
    \item \begin{multicols}{2}
        Le triangle $ABC$ est rectangle en $A$, dont la mesure de $\widehat{ABC}$ est 34°. Calculer les longueurs des côtés $[AC]$ et $[BC]$ (arrondies au dixième).
        \begin{center}
          \begin{tikzpicture}[scale=0.3, very thick]
            \coordinate (C) at (-5, 0);
            \coordinate (B) at (5, 0);
            \coordinate (A) at (-3, -4);

            \draw (A) -- (B) -- (C) -- cycle;
            \draw (A) node[below]{$A$};
            \draw (B) node[right]{$B$};
            \draw (C) node[left]{$C$};

            \draw ($(A)+(.8, .4)$) -- ++(-.4, .8) -- ++(-.8, -.4);
            \draw ($.5*(A)+.5*(B)$) node[below right]{2};
          \end{tikzpicture}
        \end{center}
      \end{multicols}
    \item \begin{multicols}{2}
        Le triangle $RST$ est rectangle en $S$. Calculer une mesure des angles $\widehat{STR}$ et $\widehat{SRT}$ (arrondies au dixième de degré).

        \begin{center}
          \begin{tikzpicture}[scale=0.3, very thick]
            \begin{scope}[rotate=145]
              \coordinate (A) at (-5, 0);
              \coordinate (B) at (5, 0);
              \coordinate (C) at (-3, -4);

              \draw (A) -- (B) -- (C) -- cycle;
              \draw (A) node[below]{$R$};
              \draw (B) node[above left]{$T$};
              \draw (C) node[right]{$S$};

              \draw ($(C)+(.8, .4)$) -- ++(-.4, .8) -- ++(-.8, -.4);
              \draw ($.5*(A)+.5*(B)$) node[below left]{8};
              \draw ($.5*(C)+.5*(B)$) node[above right]{5};
            \end{scope}
          \end{tikzpicture}
        \end{center}
      \end{multicols}
  \end{enumerate}
\end{exercice}

\newpage

\section{Cercle trigonométrique}

\begin{definition}
  Dans un repère orthonormé $(O, I, J)$, on appelle \emph{cercle trigonométrique} le cercle de centre $O$ et de rayon 1.
\end{definition}

\begin{multicols}{2}
  Soit $M(x; y)$ un point du cercle trigonométrique, et cherchons à déterminer ses coordonnées (en fonction de la mesure de l'angle $\widehat{IOM}$.

  On place $A$ et $B$ comme sur la figure ci-contre (tels que les angles $\widehat{MAO}$ et $\widehat{MBO}$ soient droits). On remarque que $OA=x$ et $OB=y$.

  \begin{tikzpicture}[very thick,scale=2]
    \draw (0,0) circle (1);
    \draw (-1.1,0) -- (1.1,0);
    \draw (0,-1.1) -- (0,1.1);
    \draw (1,0) node[below right]{$I$};
    \draw (0,1) node[above right]{$J$};
    \draw (0,0) node[below left]{$O$};

    \draw (0,0) -- ({cos(30)}, {sin(30)}) node[above right]{$M$};
        %\draw (0.6,0) arc (0:30:0.6) node at ({0.6*cos(15)},{0.6*sin(15)})[right]{$\widehat{IOM}$};
    \draw[dashed] ({cos(30)}, {sin(30)}) -- ({cos(30)}, 0) node[below]{$A$};
    \draw[dashed] ({cos(30)}, {sin(30)}) -- (0, {sin(30)}) node[left]{$B$};

    \draw [decorate, decoration={brace,amplitude=10pt,mirror,raise=4pt}] (0, 0) -- ({cos(30}, 0) node [midway,below, yshift=-0.4cm] {$x$};
    \draw [decorate, decoration={brace,amplitude=10pt,raise=4pt}] (0, 0) -- (0, {sin(30}) node [midway,left, xshift=-0.4cm] {$y$};
  \end{tikzpicture}
\end{multicols}

\paragraph{Abscisse} On se place dans le triangle $OAM$, rectangle en $A$. On sait que $OM=1$ (Pourquoi ?). On a donc : $\cos\widehat{AOM}=\frac{OA}{OM}=\frac{x}{1}=x$. Donc l'abscisse $x$ de $M$ est égale à $\cos\widehat{AOM}$.

\paragraph{Ordonnées} On se place dans le triangle $OBM$, rectangle en $B$. On sait que les angles $\widehat{OMB}$ et $\widehat{AOM}$ sont égaux (Pourquoi ?). On a donc $\sin\widehat{AOM}=\sin\widehat{OMB}=\frac{OB}{OM}=\frac{y}{1}=y$. Donc l'ordonnée $y$ de $M$ est égale à $\sin\widehat{AOM}$.

\paragraph{Bilan} Nous avons montré que les coordonnées du point $M$ sont $\coord{\cos{\widehat{AOM}}}{\sin{\widehat{AOM}}}$.

\begin{nexemple}\label{exemple:a2c}
  \emph{Le point $A$ est placé sur le cercle trigonométrique tel que la mesure de $\widehat{IOA}$ soit égale à 126°. Déterminer les coordonnées de $A$ (arrondies aux centième).}

  D'après le bilan précédent, ses coordonnées sont $\coord{\cos126}{\sin126}$, soit $\coord{-0,59}{0,81}$.
\end{nexemple}

\begin{nexemple}\label{exemple:c2a}
  \emph{Le point $A$ est sur le cercle trigonométrique, sous l'axe des abscisses, et son abscisse est $-0,8$. Calculer son ordonnée (arrondie au dixième).}

  Nous savons que l'abscisse de $A$ est $\cos\widehat{IOA}$. D'après l'énoncé, elle est égale à $-0,8$. Donc $\cos\widehat{IOA}=-0,8$, et la calculatrice nous affirme que $\widehat{IOA}\approx143$, et donc son ordonnée est $\sin\widehat{IOA}=\sin143\approx0,6$.

  Mais regardons le cercle ci-dessous : il existe deux points possibles $A'$ et $A''$ sur le cercle ayant pour abscisse $-0,8$. Celui que nous cherchons est $A''$ (car il est dit dans l'énoncé que $A$ est en dessous de l'axe des abscisses), alors que la calculatrice nous a donné l'ordonnée de celui de dessus. Puisque ces deux points sont symétriques par rapport à l'axe des abscisse, leurs ordonnées sont opposées, et l'ordonnée de $A''$ n'est donc pas $0,6$ mais $-0,6$.

  Les coordonnées de $A$ sont donc $\coord{-0,8}{-0,6}$.

  \begin{center}
    \begin{tikzpicture}[scale=2,very thick]
    \draw (0,0) circle (1);
    \draw (-1.1,0) -- (1.1,0);
    \draw (0,-1.1) -- (0,1.1);
    \draw (1,0) node[below right]{$I$};
    \draw (0,1) node[above right]{$J$};
    \draw (0,0) node[below left]{$O$};

    \draw (0,0) -- ({cos(143)}, {sin(143)}) node[above left]{$A'$};
    \draw (0,0) -- ({cos(143)}, {-sin(143)}) node[below left]{$A''$};
    \draw[dashed] ({cos(143)}, {sin(143)}) -- ({cos(143)}, {-sin(143)});
    \draw ({cos(143)}, 0) node[above right]{-0,8};
    \draw[dashed] ({cos(143)},  {sin(143)}) -- (0,  {sin(143)}) node[right]{0,6};
    \draw[dashed] ({cos(143)}, {-sin(143)}) -- (0, {-sin(143)}) node[right]{-0,6};

    \end{tikzpicture}
  \end{center}
\end{nexemple}

\begin{exercice}
  \emph{Méthode de l'exemple \ref{exemple:a2c}.}
  On considère les points $A$ et $B$, sur le cercle trigonométrique tels que la mesure de l'angle $\widehat{IOA}$ soit 56° et celle de l'angle $\widehat{IOB}$ soit 302°. Déterminer les coordonnées des points $A$ et $B$ (arrondir au centième).
\end{exercice}

\begin{exercice}
  \emph{Méthode de l'exemple \ref{exemple:c2a}.}
  \begin{enumerate}
    \item On considère un point $A$ sur le cercle trigonométrique, au dessus de l'axe des abscisses, tel que son abscisse soit 0,2. Calculer son ordonnée (arrondir au dixième).
    \item On considère un point $B$ sur le cercle trigonométrique, à gauche de l'axe des ordonnées, tel que son ordonnée soit 0,4. Calculer son abscisse.
  \end{enumerate}
\end{exercice}

\end{document}
