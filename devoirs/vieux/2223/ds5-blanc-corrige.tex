%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2017-2023 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage[
  a5paper,
  includehead,
  margin=10mm,
  headsep=3mm,
]{geometry}

\usepackage{2223-pablo}
\usepackage{2223-pablo-paternault}
\usepackage{2223-pablo-math}
\usepackage{2223-pablo-tikz}
\usetikzlibrary{angles}
\usetikzlibrary{quotes}

\usepackage{2223-pablo-header}
\fancyhead[L]{\textsc{Math 2\up{de}{}}}
\fancyhead[C]{\textsc{DS \no5 --- Sujet blanc}}
\fancyhead[R]{\textsc{Corrigé}}
\fancyfoot[C]{}

\usepackage{multicol}

\begin{document}

\begin{exercice}
  \begin{em}
    On considère la fonction $f:x\mapsto -5x+15$, et la fonction $g$ dont on connait le tableau de signes suivant.
  \end{em}

  \begin{center}
    \begin{tikzpicture}
      \tkzTabInit[lgt=2,espcl=2]
      {$x$ /.5,
        $g(x)$ /.5
      }
      {$-\infty$, -1, $\infty$}%
      \tkzTabLine{, +, z ,-}
    \end{tikzpicture}
  \end{center}

  \begin{enumerate}
    \item \emph{Dresser les tableaux de signe et de variations de $f$.}

      \begin{enumerate}
        \item\emph{Variations}
          $f$ est une fonction affine de coefficient directeur $-5$, négatif, donc elle est décroissante.

          \begin{center}
            \begin{tikzpicture}
              \tkzTabInit[lgt=1,espcl=3]
              {$x$ /1,
              $f$ /1}
              {$-\infty$,$+\infty$}%
              \tkzTabVar{+/, -/}
            \end{tikzpicture}
          \end{center}

        \item\emph{Signe}
          $f$ est une fonction affine de coefficient directeur $-5$, négatif, donc elle est décroissante, donc d'abord positive, puis négative. Elle change de signe en $-\frac{15}{-5}=3$.

  \begin{center}
  \begin{tikzpicture}
    \tkzTabInit[lgt=3,espcl=2]{$x$ /1, $-5x+15$ /1 }%
    {$-\infty$ ,$3$, $+\infty$}%
    \tkzTabLine{,+,z,-}
  \end{tikzpicture}
\end{center}
      \end{enumerate}
    \item \emph{Compléter, sans justifier, en utilisant l'un des quatre signes \fbox{$<$}, \fbox{$>$}, \fbox{$=$} ou \fbox{$?$} (s'il manque des informations pour répondre à la question).}
      \begin{enumerate}
          \item $g(2)\ldots5$

            D'après le tableau de signes de $g$, $g(2)<0$. Donc si $g(2)$ est négatif, alors $g(2)<5$.

          \item $f(-3)\ldots0$

            D'après le tableau de signes de $f$, $f(-3)$ est strictement positif, donc $f(-3)>0$.

          \item $f(6)\ldots g(1)$

            D'après les tableaux de signes de $f$ et $g$, on a : $f(6)<0$, et $g(6)<0$. Les deux sont négatifs, mais on ne sait pas lequel des deux est le plus petit : on manque d'informations pour répondre.

          \item $f(-1)\ldots g(-1)$

            D'après les tableaux de signes de $f$ et $g$, on a $f(-1)>0$ et $g(-1)=0$, donc $f(-1)>g(-1)$.
      \end{enumerate}
  \end{enumerate}
\end{exercice}

  \begin{exercice}~
\begin{multicols}{2}
    \begin{em}
      Sur le repère suivant, dont l'échelle est inconnue, quatre courbes ont été tracées. Laquelle correspond à la fonction définie par : $f(x)=-2x+6$ ? Justifier.
    \end{em}

    \begin{center}
      \begin{tikzpicture}[ultra thick,xscale=3, yscale=1.5]
        \tikzset{coordonnees/.style={text opacity=1,color=black,fill=white,opacity=.7}};
        \draw[->] (-1, 0) -- (1, 0);
        \draw[->] (0,-1) -- (0,1);
        \draw(0, 0) node[below left]{0};
        \draw[blue] (.5, 1) node[right]{$\mathcal{A}$};
        \draw[blue] (1, .8) node[right]{$\mathcal{B}$};
        \draw[blue] (1, -.3) node[right]{$\mathcal{C}$};
        \draw[blue] (.3, -1) node[right]{$\mathcal{D}$};
        \clip (-1, -1) rectangle (1, 1);
        \draw[domain=-1:1,smooth,blue] plot ({\x},{4*\x*\x+\x-.5});
        \draw[domain=-1:1,smooth,blue] plot ({\x},{.4*\x+.4});
        \draw[domain=-1:1,smooth,blue] plot ({\x},{-\x+.7});
        \draw[domain=-1:1,smooth,blue] plot ({\x},{-2*\x-.4});
      \end{tikzpicture}
    \end{center}
\end{multicols}

Puisque $f$ est une fonction affine, alors sa courbe représentative est une droite : cela exclut la courbe $\mathcal{A}$ : il ne reste que : $\mathcal{B}$, $\mathcal{C}$, $\mathcal{D}$.

Puisque le coefficient directeur de $f$, $-2$, est négatif, alors $f$ est décroissante. Cela exclut la courbe $\mathcal{B}$ : il ne reste que $\mathcal{C}$ ou $\mathcal{D}$.

Puisque l'ordonnée à l'origine de $f$, 6, est positif, alors la courbe de $f$ coupe l'axe des ordonnées en $6$, c'est-à-dire au dessus de l'axe des abscisses. Donc seule la courbe $\mathcal{B}$ correspond.
  \end{exercice}

\begin{exercice}
  \begin{em}
    L'objet de l'exercice est de résoudre l'inéquation : \[-8 x^2 + 27x - 15\geq-2x^2+15\]
  \end{em}
  \begin{enumerate}
    \item \emph{Montrer que résoudre : $-8 x^2 + 27x - 15\geq-2x^2+15$ est équivalent à résoudre : $-6 x^2 + 27x - 30\geq0$.}

      \begin{align*}
    -8 x^2 + 27x - 15&\geq-2x^2+15\\
    -8 x^2 + 27x - 15+2x^2-15&\geq0\\
    -6 x^2 + 27x -30&\geq0\\
      \end{align*}
    \item \emph{Montrer que pour tout $x\in\mathbb{R}$, on a : \[-6 x^2 + 27x - 30=(-2x+5)(3x-6)\]}

      Pour tout $x\in\mathbb{R}$, on a :

      \begin{align*}
(-2x+5)(3x-6)
&=-2x\times3x+\left( -2x \right)\times\left( -6 \right)+5\times\left( 3x \right)+5\times\left( -6 \right)\\
&=-6x^2+12x+15x-30\\
&=-6x^2+27x-30\\
      \end{align*}

    \item \emph{Recopier et compléter le tableau de signes suivant.}
      \begin{center}
        \begin{tikzpicture}
          \tkzTabInit[lgt=4,espcl=2]
          {$x$ /.6,
            $-2x+5$ /.6,
            $3x-6$ /.6,
            $(-2x+5)(3x-6)$/.6
          }
          {$-\infty$, 2, {2,5}, $+\infty$}%
          \tkzTabLine{,+,t,+,z,-}
          \tkzTabLine{,-,z,+,t,+}
          \tkzTabLine{,-,z,+,z,-}
        \end{tikzpicture}
      \end{center}
    \item \emph{En déduire les solutions de l'inéquation de départ.}

      D'après le tableau de signes, les solutions de 
      $(-2x+5)(3x-6)\geq0$ sont $x\in\intervallef{2;2,5}$.

      Or nous avons montré que $(-2x+5)(3x-6)=-6 x^2 + 27x -30$, donc les solutions de $-6 x^2 + 27x -30\geq0$ sont aussi $x\in\intervallef{2;2,5}$.

      Enfin, nous avons montré à la première question que les inéquations $-8 x^2 + 27x - 15\geq-2x^2+15$ et $-6 x^2 + 27x - 30\geq0$ sont équivalentes, donc les solutions de $-8 x^2 + 27x - 15\geq-2x^2+15$ sont aussi $x\in\intervallef{2;2,5}$.
  \end{enumerate}
\end{exercice}

\begin{exercice}
  \begin{em}
    On considère une fonction affine $f$, dont on sait que $f(13)=12$ et $f(9)=17$.
  \end{em}
  \begin{enumerate}
    \item \emph{Montrer que l'équation de la fonction $f$ est : $f:x\mapsto -1,25x+28,25$.}

      Puisque $f$ est une fonction affine, elle est de la forme $f(x)=ax+b$.

      Le coefficient directeur $a$ est :
      \begin{align*}
        a&=\frac{f(13)-f(9)}{13-9}\\
        &=\frac{12-17}{4}\\
        &=\frac{-5}{4}\\
        &=-1,25\\
      \end{align*}
      Donc $f$ est de la forme $f(x)=-1,25x+b$. Calculons l'ordonnée à l'origine $b$.

      Puisqeu $f(9)=17$, alors :
      \begin{align*}
        f(9)&=17\\
        -1,25\times9+b&=17\\
        -11,25+b&=17\\
        b&=17+11,25\\
        b&=28,25
      \end{align*}
      Donc l'expression de $f$ est : $f(x)=-1,25x+28,25$.

    \item \emph{Quelles sont les variations de $f$ ? Justifier.}

      La fonction $f$ est affine de coefficient directeur $-1,25$, négatif, donc elle est décroissante.

    \item \emph{Le point $C(123; -125,5)$ est-il sur la courbe de $f$ ? Justifier.}

      Calculons $f(123)$ :
      \[
        f(123)
        =-1,25\times123+28,25=-125,5
        \]
        Donc les coordonnées du point $C$ sont bien de la forme $\left(123 ; f(123)\right)$ : il est sur la courbe de $f$.
  \end{enumerate}
\end{exercice}

\begin{exercice}
  \begin{em}
    La gestionnaire d'un lycée étudie plusieurs propositions pour l'achat de ramettes de papier dans un lycée.

    \begin{enumerate}[A]
      \item Chaque ramette est vendue à 2,54 €.
      \item Le lycée souscrit un contrat à 230 €, puis chaque ramette est ensuite vendue à 2,02 €.
    \end{enumerate}

    L'objet de l'exercice est de déterminer le contrat le plus avantageux en fonction du nombre de ramette consommées dans l'année.

    On admet que pour $x$ ramettes de papier consommées, le coût est donné par la fonction $A:x\mapsto 2,54x$ pour l'entreprise A, et $B:x\mapsto 2,02x+230$ pour l'entreprise B.
  \end{em}

  \begin{enumerate}
    \item \emph{Résolution graphique}
      \begin{enumerate}
        \item \emph{Tracer les courbes des fonctions $A$ et $B$ sur le graphique ci-dessous.}

          Dans chacun des cas, on prend deux valeurs de $x$ distinctes, et on calcule les images par la fonction.
          \begin{enumerate}[label={Courbe \Alph*}]
            \item Pour $x=0$, on a $A(0)=2,54\times0=0$. Donc le point de coordonnées $(0; 0)$ est sur la courbe de $A$.

              Pour $x=500$, on a $A(500)=2,54\times500=1270$, donc le point de coordonnées $(500;1270)$ est sur la courbe de $A$.
            \item Pour $x=0$, on a $B(0)=2,02\times0+230=230$. Donc le point de coordonnées $(0; 230)$ est sur la courbe de $B$.

              Pour $x=400$, on a $B(400)=2,02\times400+230=1208$, donc le point de coordonnées $(400;1208)$ est sur la courbe de $B$.
          \end{enumerate}

        \item \emph{Répondre par lecture graphique : À partir de combien de ramettes vendues le contrat $B$ sera-t-il plus avantageux que le contrat $A$ ?}

          Les deux courbes se croisent approximativement en $x\approx450$, donc le contrat $B$ sera plus avantageux que le contrat $A$ à partir d'environ 450 ramettes de papier achetées.
      \end{enumerate}
      \begin{center}
        \begin{tikzpicture}[xscale=2, yscale=1, ultra thick]
          \draw[xstep=0.5, dotted, gray] (0, 0) grid (4, 5);
          \draw[->] (0, 0) -- (4, 0);
          \draw[->] (0, 0) -- (0, 5);
          \draw (0, 0) node[below left]{$\mathcal{O}$};
          \foreach \i in {1, ..., 8} {
            \draw ({.5*\i}, 0) node[below]{\i00};
          }
          \foreach \i in {4, 8, ..., 20} {
            \draw (0, {.25*\i}) node[left]{\i00};
          }

          \draw[blue] (0, 0) -- ({.5*8}, {.25*2.54*8}) node[right]{$A$};
          \draw[red] (0, {.25*2.3}) -- ({.5*8}, {.25*(2.02*8+2.3)}) node[right]{$B$};
        \end{tikzpicture}
      \end{center}
    \item \emph{Résolution algébrique On souhaite répondre à la même question, mais par le calcul.}
      \begin{enumerate}
        \item \emph{Montrer que le contrat $B$ est plus avantageux que le contrat $A$ si et seulement si : $0,52x-230\geq 0$.}

          Le contrat $B$ est plus avantageux que le contrat $A$ si et seulement si :
          \begin{align*}
            A(x)&\geq B(x)\\
            2,54x&\geq2,02x+230\\
            2,54x-2,02x-230&\geq0\\
            (2,54-2,02)x-230&\geq0\\
            0,52x-230&\geq0\\
          \end{align*}
          
        \item \emph{Dresser le tableau de signes de la fonction $x\mapsto 0,52x-230$ (arrondir les valeurs à l'unité si nécessaire).}

          La fonction $0,52x-230$ est une fonction affine de coefficient directeur $0,52$, positif : elle est donc croissante (donc d'abord négative, puis positive), et elle change de signe en $-\frac{-230}{0,52}\approx442$.

  \begin{center}
    \begin{tikzpicture}
      \tkzTabInit[lgt=3,espcl=2]
      {$x$ /1,
        {$0,52x-230$} /1
      }
      {$-\infty$, 442, $\infty$}%
      \tkzTabLine{, -, z ,+}
    \end{tikzpicture}
  \end{center}
        \item \emph{Conclure en lisant le tableau de signes : À partir de combien de ramettes de papier vendues le contrat $B$ est-il plus avantageux que le contrat $A$ ?}

          D'après le tableau de signes, les solutions de $0,52x-230\geq0$ sont $x\geq442$ (environ), donc le contrat $B$ est plus avantageux que le contrat $B$ à partir de 442 ramettes de papier vendues.
      \end{enumerate}
  \end{enumerate}
\end{exercice}

\end{document}
