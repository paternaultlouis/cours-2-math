%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2020-2023 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{2223-pablo}
\usepackage{2223-pablo-paternault}
\usepackage{2223-pablo-math}

\usepackage[
  a5paper,
  margin=6mm,
  includehead,
]{geometry}
\usepackage{2223-pablo-header}
\fancyhead[L]{\textsc{Mathématiques 2\up{de}{}}}
\fancyhead[C]{\textsc{DM \no5}}
\fancyhead[R]{\textsc{Corrigé}}

\usepackage{2223-pablo-tikz}
\usetikzlibrary{patterns}

\usepackage{multicol}

\begin{document}

\begin{exercice} Corrigé en autonomie…
\end{exercice}

\begin{exercice}\label{ex:facile}
  \begin{em}
    On cherche à résoudre l'inéquation : \[2x^2+18x-13\geq15\]
  \end{em}
  \begin{enumerate}
    \item \emph{Montrer que résoudre cette inéquation revient à résoudre l'inéquation : \[(2x-4)(-x+7)\geq0\]}

      D'une part, on a :
      \begin{align*}
    2x^2+18x-13&\geq15\\
    2x^2+18x-13-15&\geq0\\
    2x^2+18x-28&\geq0\\
      \end{align*}

      Et d'autre part, on a :
      \begin{align*}
    (2x-4)(-x+7)&\geq0\\
  2x\times\left( -x \right)+2x\times7+\left( -4 \right)\times\left( -x \right)+\left( -4 \right)\times7&\geq0\\
  -2x^2+14x+4x-28&\geq0\\
  -2x^2+18x-28&\geq0
      \end{align*}
      
      Les deux inéquations sont équivalentes à $-2x^2+18x-28\geq0$, donc elles sont équivalentes entre elles.
    \item
      \begin{enumerate}
        \item \emph{Dresser le tableau de signes des fonctions affines d'expression : $2x-4$ et $-x+7$.}
          Voir la question suivante.
        \item \emph{En déduire le tableau de signes de l'expression : $\left( 2x-4 \right)\left( -x+7 \right)$.}

          La première fonction est affine de coefficient directeur $2$ : elle est croissante, et change de signe en $-\frac{-4}{2}=2$.

          La seconde fonction est affine de coefficient directeur $-1$ : elle est décroissante, et change de signe en $-\frac{7}{-1}=7$.

        \begin{center}
        \begin{tikzpicture}
                \tkzTabInit[lgt=4,espcl=2]
                        {$x$ /1,
                          $2x-4$ /1,
                        $-x+7$ /1,
                        $(2x-4)(-x+7)$ /1
                        }
                        {$-\infty$, 2, 7, $\infty$}%
                        \tkzTabLine{, -, z ,+,t,+}
                        \tkzTabLine{, +, t ,+,z,-}
                        \tkzTabLine{, -, z ,+,z,-}
        \end{tikzpicture}
        \end{center}
      \end{enumerate}
    \item \emph{En déduire les solutions de l'inéquation de départ.}

      D'après le tableau de signes, les solutions de $\left( 2x-4 \right)\left( -x+7 \right)\geq0$ sont $x\in\intervallef{2;7}$, donc les solutions de $2x^2+18x-13\geq15$ sont aussi : \fbox{$x\in\intervallef{2;7}$}.
  \end{enumerate}
\end{exercice}

\begin{exercice}\label{ex:difficile}~
  \begin{em}
    L'architecte d'un aquarium réfléchit au plan de l'une des salles, qui aura la forme ci-contre. C'est un rectangle de \SI{30}{m} par \SI{50}{m}, composée de quatre bassins rectangulaires aux quatre coins de la salle (en bleu sur la figure); les deux bandes blanches sont les allées pour les visiteurs.
  \end{em}

  \begin{multicols}{2}
  \begin{em}
    L'architecte a les contraintes suivantes :
    \begin{itemize}
      \item il faut que la superficie de l'ensemble des bassins soit supérieure à \SI{800}{m^2} ;
      \item il faut que la superficie des allées soit supérieure à \SI{375}{m^2} .
    \end{itemize}
    On appelle $x$ (compris dans l'intervalle $\intervallef{0;30}$) la largeur des allées, en mètres. L'objet de l'exercice est de déterminer quelles sont les valeurs possibles pour $x$ pour que les deux contraintes soient respectées.
  \end{em}

    \columnbreak

    \begin{center}
      \begin{tikzpicture}[scale=.105, very thick]
        \draw (0, 0) rectangle (50, 30);
        \begin{scope}
          \draw[pattern=north west lines, pattern color=blue] (0, 0) -- (0, 15) -- ++(25, 0) -- ++(0, -15) -- cycle;
          \draw[pattern=north west lines, pattern color=blue] (35, 0) -- ++(0, 15) -- (50, 15) -- (50, 0) -- cycle;
          \draw[pattern=north west lines, pattern color=blue] (0, 25) -- ++(25, 0) -- (25, 30) -- (0, 30) -- cycle;
          \draw[pattern=north west lines, pattern color=blue] (35, 30) -- (50, 30) -- (50, 25) -- (35, 25) -- cycle;
        \end{scope}
        \draw[latex-latex] (-3, 15) -- ++(0, 10) node[midway, left]{$x$};
        \draw[latex-latex] (25, -3) -- ++(10, 0) node[midway, below]{$x$};
        \draw[latex-latex] (0, 33) -- ++(50, 0) node[midway, above]{\SI{50}{m}};
        \draw[latex-latex] (53, 0) -- ++(0, 30) node[midway, below, rotate=90]{\SI{30}{m}};
      \end{tikzpicture}
    \end{center}
  \end{multicols}

  \begin{em}
  On appelle $B$ la fonction qui à $x$ associe l'aire des aquarium, et $A$ la fonction qui à $x$ associe l'aire des allées.
\end{em}

  \begin{enumerate}
    \item 
      \begin{enumerate}
        \item \emph{Calculer l'aire totale de la salle.}

          La salle est un rectangle de côtés \SI{50}{m} et \SI{30}{m}, donc son aire est : $30\times50=\SI{1500}{m^2}$.
        \item\label{q:bornes} \emph{Expliquer pourquoi $x\in\intervallef{0;30}$.}

          $x$ est une longueur, qui ne peut donc pas être négative. De plus, si $x>30$, alors l'allée est plus large que la salle, ce qui est impossible. Donc $x\in\intervallef{0;30}$.
        \item \emph{Montrer que pour tout $x\in\intervallef{0;30}$, on a $A(x)=-x^2+80x$.}

          L'allée horizontale a pour aire $50x$. L'allée verticale a pour aire $30x$. Mais en additionnant ces deux allées, on compte deux fois le carré central, d'aire $x^2$, qu'il faut donc retrancher. Donc :
          \[
            A(x)=50x+30x-x^2=-x^2+80x
          \]
        \item \emph{En déduire que pour tout $x\in\intervallef{0;30}$, on a $B(x)=x^2-80x+1500$.}

          L'aire des aquariums est l'aire totale, à laquelle il faut soustraire l'aire des allées. Donc :
          \begin{align*}
            B(x)&=1500-A(x)\\
            &=1500-\left( -x^2+80x \right)\\
            &=1500+x^2-80x\\
            &=x^2-80x+1500
          \end{align*}
      \end{enumerate}
    \item \emph{On s'intéresse à l'aire des bassins.}
      \begin{enumerate}
        \item \emph{Expliquer pourquoi $B(x)\geq800$.}

          D'après l'énoncé, l'aire des bassins doit être supérieure à \SI{800}{m^}, donc : $B(x)\geq800$.
        \item \emph{Montrer que résoudre $B(x)\geq800$ est équivalent à résoudre : $x^2-80x+700\geq0$.}

          \begin{align*}
            B(x)&\geq800\\
            x^2-80x+1500&\geq800\\
            x^2-80x+1500-800&\geq0\\
            x^2-80x+700&\geq0\\
          \end{align*}

        \item \emph{Montrer que résoudre $B(x)\geq800$ revient à résoudre : $\left( x-10 \right)\left( x-70 \right)\geq0$.}

          D'une part, nous avons montré à la question précédente que $B(x)\geq800$ est équivalente à $x^2-80x+700\geq0$.

          D'autre part :
          \begin{align*}
            \left( x-10 \right)\left( x-70 \right)&\geq0\\
            x\times x+x\times\left( -70 \right)+\left( -10 \right)\times x+\left( -10 \right)\times\left( -70 \right) &\geq0\\
            x^2-70x-10x+700&\geq0\\
            x^2-80x+100&\geq0\\
          \end{align*}

          Donc les deux inéquations sont équivalentes à $x^2-80x+100\geq0$ : elles sont équivalentes entre elles, et $B(x)\geq800$ est équivalent à $\left( x-10 \right)\left( x-70 \right)\geq0$.

        \item \emph{Dresser le tableau de signes du produit $\left( x-10 \right)\left( x-70 \right)$, puis en déduire que $B(x)\geq800$ si $x\leq10$.}

          La affine $x-10$ est affine de coefficient directeur 1, donc croissante, et change de signe en $-\frac{-10}{1}=10$.

          La affine $x-70$ est affine de coefficient directeur 1, donc croissante, et change de signe en $-\frac{-70}{1}=70$.

        \begin{center}
        \begin{tikzpicture}
                \tkzTabInit[lgt=4,espcl=2]
                        {$x$ /1,
                          $x-10$ /1,
                        $x-70$ /1,
                        $(x-10)(x-70)$ /1
                        }
                        {$-\infty$, 10, 70, $+\infty$}%
                        \tkzTabLine{, -, z ,+,t,+}
                        \tkzTabLine{, -, t ,-,z,+}
                        \tkzTabLine{, +, z ,-,z,+}
        \end{tikzpicture}
        \end{center}
        Donc les solutions de $(x-10)(x-70)\geq0$ sont $x\leq10$ ou $x\geq70$. Mais nous avons montré à la question \ref{q:bornes} que $x\in\intervallef{0;30}$, donc le second ensemble de solutions $x\geq70$ est à exclure : $x\in\intervallef{0;10}$.
      \end{enumerate}
    \item\label{q:allees} \emph{On s'intéresse à l'aire des allées.}
      \begin{enumerate}
        \item \emph{Expliquer pourquoi $A(x)\geq375$, puis montrer que résoudre $A(x)\geq375$ revient à résoudre : $\left( x-5 \right)\left( x-75 \right)\leq0$.}

          Puisque l'aire des allées doit être au moins \SI{375}{m^2}, alors $A(x)\geq375$.

          Pour tout $x\in\mathbb{R}$, on a :
          \begin{align*}
            \left( x-5 \right)\left( x-75 \right)&\leq0\\
            x\times x+x\times\left( -75 \right)+\left( -5 \right)\times x+\left( -5 \right)\times\left( -75 \right)&\leq0\\
            x^2-75x-5x+375&\leq0\\
            x^2-80x   +375&\leq0\\
                       375&\leq-x^2+80x\\
                       375&\leq A(x)
          \end{align*}

        \item \emph{Dresser le tableau de signe de $\left( x-5 \right)\left( x-75 \right)$, puis en déduire les solutions de l'inéquation $A(x)\geq375$.}

        \begin{center}
        \begin{tikzpicture}
                \tkzTabInit[lgt=4,espcl=2]
                        {$x$ /1,
                          $x-5$ /1,
                        $x-75$ /1,
                        $(x-5)(x-75)$ /1
                        }
                        {$-\infty$, 5, 75, $\infty$}%
                        \tkzTabLine{, -, z ,+,t,+}
                        \tkzTabLine{, -, t ,-,z,+}
                        \tkzTabLine{, +, z ,-,z,+}
        \end{tikzpicture}
        \end{center}
        Donc les solutions de $(x-5)(x-75)\leq0$ sont $x\in\intervallef{5;75}$, et les solutions de $A(x)\geq375$ sont également $x\in\intervallef{5;75}$.
      \end{enumerate}

    \item \emph{Conclure : Quelles valeurs l'architecte peut-elle prendre pour la largeur des allées pour que les contraintes soient respectées ?}

      Nous avons montré, d'une part, que $x\leq10$, et d'autre part, que $x\in\intervallef{5;75}$. Donc $x$ doit être compris entre 5 et 10 : \fbox{$x\in\intervallef{5;10}$}.

      La largeur des allées doit être comprise entre \SI{5}{m} et \SI{10}{m}.
  \end{enumerate}
\end{exercice}

\end{document}
