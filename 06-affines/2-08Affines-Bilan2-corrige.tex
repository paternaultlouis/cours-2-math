%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2020 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename
\documentclass[12pt]{article}


\usepackage{1920-pablo}
\usepackage{1920-pablo-paternault}
\usepackage[a5paper, bottom=1.5cm, left=1cm, right=1cm]{geometry}

\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhead[L]{\textsc{Chapitre 8 --- Fonctions affines}}
\fancyhead[R]{\textsc{Bilan 2 --- Corrigé}}
\fancyfoot[C]{}

\setlength{\parindent}{0pt}

\begin{document}

\begin{exercice}
  \begin{em}
  Dresser le tableau de signes et le tableau de variations de la fonction $f$ définie sur $\mathbb{R}$ par $f(x)=-5x+3$.
\end{em}

La fonction est affine de coefficient directeur $a=-5$, et d'ordonnée à l'origine $b=3$.

\begin{description}
  \item[Tableau de variations] Puisque le coefficient directeur est strictement négatif, la fonction est strictement décroissante.
        \begin{center}
          \begin{tikzpicture}[scale=1]
            \tkzTabInit[lgt=1.5,espcl=2.4]
            {$x$ /1,
              {$f$} /1
            }
            {$-\infty$, $+\infty$}%
            \tkzTabVar{+/, -/}
          \end{tikzpicture}
        \end{center}
      \item[Tableau de signes] Puisque la fonction est décroissante, elle est d'abord positive puis négative. Elle change de signe en $-\frac{b}{a}=-\frac{3}{-5}=0,6$.
        \begin{center}
          \begin{tikzpicture}[scale=1]
            \tkzTabInit[lgt=1.5,espcl=2.4]
            {$x$ /1,
              {$f$} /1
            }
            {$-\infty$, {$0,6$}, $+\infty$}%
            \tkzTabLine{,+,z,-}
          \end{tikzpicture}
        \end{center}
\end{description}
\end{exercice}

\begin{exercice}
  \begin{em}
  Soit $f$ une fonction définie sur $\mathbb{R}$ dont on ne connaît que le tableau de signes suivant.
\end{em}
        \begin{center}
          \begin{tikzpicture}[scale=1]
            \tkzTabInit[lgt=1.5,espcl=2]
            {$x$ /1,
              {$f$} /1
            }
            {$-\infty$, -10, {1,3}, 4, 12, $+\infty$}%
            \tkzTabLine{,-,z,+,z,-,z,-,z,+}
          \end{tikzpicture}
        \end{center}
        \begin{enumerate}
          \item \emph{Quelles sont les solutions de $f(x)=0$ ?}
            Les solutions sont :
            $x=-10$,
            $x=1,3$,
            $x=4$, et
            $x=12$.
          \item \emph{Résoudre $f(x)\geq0$.}
            Il faut lire les abscisses des zones du tableau où la fonction est positive. Les solutions sont donc $x\in\intervallef{-10;1,3}\cup\left\{ 4 \right\}\cup\intervallefi{12}$.
          \item \emph{Montrer que $f(0)>f(10)$.}
            D'après le tableau de signes, $f(0)>0$ et $f(10)<0$. Donc $f(10)<0<f(0)$, et $f(10)<f(0)$.
          \item \emph{La fonction $f$ est-elle une fonction affine ?}
            Une fonction affine est soit positive puis négative, soit négative puis négative. Elle ne peut pas être négative, puis positive, puis à nouveau négative, etc. Donc ce n'est pas une fonction affine.
          \item \emph{Peut-on affirmer que la fonction $f$ est croissante sur $\intervalleif{1,3}$ ?}
            Non. Il est possible que la fonction soit croissante sur $\intervalleif{-20}$, puis décroissante sur $\intervallef{-20;-15}$, puis à nouveau croissante sur $\intervallef{-15;-10}$, le tout en restant dans les négatifs.
        \end{enumerate}
\end{exercice}

\begin{exercice}
  % Inspiré de l'exercice 45 p. 107, lelivrescolaire.fr, mathématiques seconde, 2019.
  \emph{Dans cet exercice, toutes les valeurs pourront être arrondies au centième.}

  \begin{em}
  En 2006, la population d'éléphants d'Afrique était de 526 milliers. En 2016, celle-ci n'est plus que de 415 milliers. On modélise l'évolution de cette population par une fonction affine $f$, où pour une année $x$, le nombre $f(x)$ représente la population d'éléphants d'Afrique, en milliers (par exemple, $f(2006)=526$ signifie qu'en 2006, il y avait 526 éléphants d'Afrique).
\end{em}

  \begin{enumerate}
    \item \emph{Montrer que l'expression de $f$ est $f(x)=-11,1x+22792,6$.}
      Nous cherchons l'expression d'une fonction affine telle que $f(2006)=526$ et $f\left( 2016 \right)=415$. Le coefficient directeur est donc :
      \[
        \frac{f(2016)-f(2006)}{2016-2006}=
        \frac{415-526}{2016-2006}=-11,1
      \]
      La fonction est donc de la forme $f(x)=-11,1x+b$. Calculons $b$.

      Nous savons que $f(2006)=526$. Donc :
      \begin{align*}
      -11,1\times2006+b&=526\\
      −22266,6+b&=256\\
      b&=22266,6+256\\
      b&=22792,6
      \end{align*}
      L'expression de $f$ est donc $f(x)=-11,1x+22792,6$.
    \item \emph{Si la modélisation est correcte, quelle sera la population d'éléphants en 2030 ?}
      En 2030, la population sera de $f(2030)=-11,1\times2030+22792,6=259,6$, c'est-à-dire environ 260 milliers d'individus.
    \item \emph{Dresser le tableau de signes de la fonction $f$.}
      Le coefficient directeur de la fonction, $-11,1$, est strictement négatif, donc la fonction est strictement décroissante. Elle est donc positive, puis négative. De plus, elle change de signe en 
      $-\frac{22792,6}{-11,1}\approx2053,39$.
        \begin{center}
          \begin{tikzpicture}[scale=1]
            \tkzTabInit[lgt=1.5,espcl=2.4]
            {$x$ /1,
              {$f$} /1
            }
            {$-\infty$, {$2053,39$}, $+\infty$}%
            \tkzTabLine{,+,z,-}
          \end{tikzpicture}
        \end{center}

    \item \emph{Sans nouveau calcul, déterminer l'année à partir de laquelle il n'y aura plus aucun éléphant, si cette modélisation est correcte.}
      Dans le tableau de signes, nous voyons que la fonction devient négative à partir de $x=2053,39$. Donc la population d'éléphants deviendrait nulle entre les années 2053 et 2054.
  \end{enumerate}
\end{exercice}

\end{document}
