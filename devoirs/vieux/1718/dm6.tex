%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2018 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{1920-pablo}
\usepackage{1920-pablo-devoir}
\usepackage{1920-pablo-paternault}
\usepackage[a5paper,margin=1.6cm]{geometry}

\pagestyle{empty}

\title{Fonctions affines}
\date{27/02/2018}
\classe{2\up{de}12}
\dsnum{DM 6}

\setlength{\parindent}{0cm}

\begin{document}

\maketitle

\begin{em}
Faire deux des trois exercices \ref{ex:tableau}, \ref{ex:modelisation}, \ref{ex:perpendicularite} au choix (ils sont classés par ordre de difficulté). L'exercice \ref{ex:culture} est obligatoire.
\end{em}

\begin{exercice}[Tableaux]\label{ex:tableau}
On considère la fonction $f$ définie sur $\mathbb{R}$ par : $f:x\mapsto -2x+1$.
\begin{enumerate}
\item Construire le tableau de variations de $f$.
\item Construire le tableau de signes de $f$.
\item Sans aucun calcul, en utilisant uniquement les tableaux construits aux questions précédentes, dire si les affirmations suivantes sont vraies ou fausses.

\begin{multicols}{2}
\begin{enumerate}
\item  $f(98)<f(123)$
\item $f(-2)\geq 0$
\item $f(0)\leq f(2)$
\item $f(0,5)=7$
\end{enumerate}
\end{multicols}
\end{enumerate}
\end{exercice}

\begin{exercice}[Modélisation]\label{ex:modelisation}
On considère la figure suivante (qui n'est pas à l'échelle), où les angles $\widehat{BAC}$ et $\widehat{DCA}$ sont droits. Le point $M$ est un point de $[AC]$, et on nomme $x$ la longueur $AM$, en centimètres. On connait les longueurs $AB=3$, $AC=5$ et $CD=8$. Les longueurs sont données en centimètres.
\begin{center}
\begin{tikzpicture}[xscale=1, yscale=.5, ultra thick]
\coordinate (A) at (0, 0);
\coordinate (B) at (0, 3);
\coordinate (C) at (5, 0);
\coordinate (D) at (5, 8);
\coordinate (M) at (2, 0);
\draw (A) node[below left]{$A$};
\draw (B) node[left]{$B$};
\draw (C) node[below right]{$C$};
\draw (D) node[above right]{$D$};
\draw (M) node[below]{$M$};
\draw (1, 0) node[below]{$x$};
\draw (M) -- (B) -- (A) -- (C) -- (D) -- (M);
\end{tikzpicture}
\end{center}

 On appelle $\mathcal{A}(x)$ la somme des aires des deux triangles $BAM$ et $CDM$.

\begin{enumerate}
\item \emph{Cas particulier.} Dans cette question (et uniquement dans cette question), on considère que $x=2$.
\begin{enumerate}
\item Calculer l'aire du triangle $BAM$.
\item Calcule la longueur $MC$, puis l'aire du triangle $DCM$.
\item En déduire $\mathcal{A}(2)$.
\end{enumerate}
\item \emph{Cas général.}
\begin{enumerate}
\item Montrer que l'aire de $DCM$ est égale à $20-4x$.
\item Exprimer l'aire de $BAM$ en fonction de $x$, puis montrer que $\mathcal{A}(x)=20-2,5x$.
\end{enumerate}
Répondre aux questions suivantes en utilisant l'expression $\mathcal{A}(x)=20-2,5x$.
\begin{enumerate}
\setcounter{enumii}{2}
\item Lorsque $M$ de déplace de $A$ vers $C$, est-ce que l'aire $\mathcal{A}(x)$ augmente ou diminue ? Justifier.
\item Quelle est la plus petite valeur prise par $\mathcal{A}$ ? La plus grande ?
\item Pour quelles valeurs de $x$ l'aire $\mathcal{A}$ est-elle supérieure à 10~cm\up{2} ?
\end{enumerate}
\end{enumerate}
\end{exercice}

\newpage

\begin{exercice}[Perpendicularité]\label{ex:perpendicularite}
\begin{em}
L'objet de l'exercice est de trouver une méthode permettant de savoir si deux droites (dont on connait les équations) sont perpendiculaires ou non.
\end{em}

Soient deux droites, dont les équations dans un repère orthonormé sont : $d_1:y=a_1x+b_1$ et $d_2:y=a_2x+b_2$. On suppose que ces deux droites ne sont pas parallèles, et on appelle $A$ leur point d'intersection.

On considère un nouveau repère orthonormé, d'origine $A$, dont les axes sont parallèles aux axes du repère d'origine. À partir de maintenant, on ne manipule plus que des coordonnées dans ce nouveau repère.

On considère le point $B$, sur la droite $d_1$, d'abscisse 1, et le point $C$, sur la droite $d_2$, d'abscisse 1.

La situation est schématisée dans la figure ci-dessous, où l'ancien repère est en pointillés, et le nouveau en traits pleins.

\begin{center}
\begin{tikzpicture}[scale=1, ultra thick]
\draw[<->, dashed] (0, 3) -- (0, 0) -- (5, 0);
\draw[blue] (2, 3) -- (6, 1) node[right]{$d_1$};
\draw[blue] (3, 1) -- (6, 4) node[right]{$d_2$};
\draw (5, 2) node{|} node[below right]{1};
\draw (4, 3) node{--} node[left]{1};
\draw[<->] (4, 4) -- (4, 2) -- (6, 2);
\coordinate (A) at (4, 2);
\coordinate (B) at (5, 1.5);
\coordinate (C) at (5, 3);
\draw (A) node[below]{$A$};
\draw (B) node[below]{$B$};
\draw (C) node[above]{$C$};
\draw[dotted] (B) -- (C);
\end{tikzpicture}
\end{center}

\begin{enumerate}
\item \emph{Premier cas :} On se place dans le cas où les deux droites ne sont pas parallèles.
\begin{enumerate}
\item Expliquer pourquoi les coordonnées de $A$, $B$ et $C$ sont $A(0, 0)$, $B(1, a_1)$ et $C\left(1, a_2\right)$.
\item Exprimer les nombres $AB^2$, $BC^2$ et $AC^2$ en fonction de $a_1$ et $a_2$.
\item Montrer que le triangle $ABC$ est rectangle en $A$ si et seulement si $a_1\times a_2=-1$.
\item En déduire que deux droites non parallèles sont perpendiculaires si et seulement si $a_1\times a_2=-1$.
\end{enumerate}
\item \emph{Deuxième cas :} Dans ce cas, les deux droites sont parallèles. On admet que deux droites sont parallèles si et seulement si leurs coefficients directeurs sont égaux. Montrer alors que $a_1\times a_2\neq-1$.
\end{enumerate}
En faisant le bilan des deux cas précédents, nous avons montré que les droites de deux fonctions affines sont perpendiculaires si et seulement si le produit de leurs coefficients directeurs est égal à -1.
\begin{enumerate}
\setcounter{enumi}{2}
\item \emph{Application :} Soint trois droites $\Delta_1$, $\Delta_2$, $\Delta_3$, d'équations respectives :
\[\Delta_1:y=2x-1 \hspace{1cm} \Delta_2:y=-0,5x+2 \hspace{1cm} \Delta_3: y=\frac{x}{3}-1\]
Parmi ces droites, lesquelles sont perpendiculaires ?
\end{enumerate}
\end{exercice}

\begin{exercice}[Culture]\label{ex:culture}
  Donner un exemple de problème ou conjecture non-résolu en mathématiques, dont vous comprenez (si possible) l'énoncé.
\end{exercice}

\end{document}
