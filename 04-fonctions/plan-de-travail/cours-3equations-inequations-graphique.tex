%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2018 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{1920-pablo}
\usepackage{1920-pablo-cours}
\usetikzlibrary{intersections}

\usepackage{1920-pablo-paternault}

\usepackage[a5paper, margin=1.1cm]{geometry}

\begin{document}

\setcounter{section}{2}
\section{Équations et inéquations (lecture graphique)}
\subsection{Une seule fonction}

\begin{figure}[!h]
  \begin{center}
  \begin{tikzpicture}[very thick,xscale=.8, yscale=1.2]
    \draw[dotted, lightgray, xstep=1, ystep=.5] (-4, -1) grid (6, 3);
    \draw[-latex] (-4,0) -- (6,0);
    \draw[-latex] (0,-1) -- (0,3);
    \foreach \x in {-4, -3, -2, -1, 1, 2, 3, 4, 5, 6} {
      \draw (\x,0) node[below]{\x};
      \draw (\x,{0.1}) -- (\x,{-0.1});
    }
    \foreach \y in {-1, 1, 2} {
      \draw (0,\y) node[left]{\y};
      \draw (-.1, \y) -- (.1, \y);
    }
    \draw[cyan, name path=F] plot [smooth, tension=0.5] coordinates {
      (-4,-1)
      (-3, 2.5)
      (0, 2.1)
      (2, .5)
      (4, 1.5)
      (5, .5)
      (6, 0)
    };
    \draw (0,0) node[below left]{$O$};
    \draw (1.5, 1.5) node[above right]{$\mathcal{C}_f$};

    \coordinate (Y) at (0, 1);
    \coordinate (O) at (0, 0);
    \draw[dashed, name path=DY] (-4, 1) -- ++(10, 0);
    \path[name intersections={of=F and DY}];
    \coordinate (I1)  at (intersection-1);
    \coordinate (I2)  at (intersection-2);
    \coordinate (I3)  at (intersection-3);
    \coordinate (I4)  at (intersection-4);
    \foreach \i in {I1, I2, I3, I4}{
      \draw (\i) node{$\bullet$};
    }
    \draw[dashed] (I1) -- (I1 |- O);
    \draw[dashed] (I2) -- (I2 |- O);
    \draw[dashed] (I3) -- (I3 |- O);
    \draw[dashed] (I4) -- (I4 |- O);
  \end{tikzpicture}
  \caption{Une fonction.}
  \label{f1}
\end{center}
\end{figure}

\begin{methode}[Résolution graphique de $f(x)=k$]
  Résoudre $f(x)=k$, c'est déterminer les antécédents de $k$ par $f$.
\end{methode}

\begin{exemple}[Résolution graphique de $f(x)=1$]
  Voir la figure \ref{f1} comme illustration.
  \begin{enumerate}
    \item On repère $1$ sur l'axe des ordonnées.
    \item On trace la droite parallèle à l'axe des abscisses, d'ordonnée $1$ (horizontale en pointillés sur la figure).
    \item On repère les points d'intersection de la courbe de $f$ et cette droite (marqués par des $\bullet$ sur la figure).
    \item On lit les abscisses de ces points d'intersection (environ -3,5 ; 1,2 ; 3 ; 4,5) : ce sont les antécédents de $1$ par $f$.
  \end{enumerate}
  Les solutions de $f(x)=1$ sont donc environ -3,5 ; 1,2 ; 3 ; 4,5.
\end{exemple}

\begin{methode}[Résolution graphique de $f(x)\leq k$]
  Résoudre $f(x)\leq k$, c'est déterminer les valeurs de $x$ dont l'image par $f$ est inférieure à $k$ (et donc la courbe de $f$ est en dessous de la droite d'équation $y=k$).

  Pour les autres inéquations, c'est la même propriété, à ceci que :
  \begin{itemize}
    \item pour $f(x)<k$, on regarde les points de la courbe de $f$ situés \emph{strictement} en dessous de la droite ;
    \item pour $f(x)\geq k$, on regarde les points de la courbe de $f$ situés \emph{au dessus} de la droite ;
    \item pour $f(x)>k$, on regarde les points de la courbe de $f$ situés \emph{strictement au dessus} de la droite ;
  \end{itemize}
\end{methode}

\begin{exemple}[Résolution graphique de $f(x)\leq1$]
  Voir la figure \ref{f1} comme illustration.
  \begin{enumerate}
    \item On repère $1$ sur l'axe des ordonnées.
    \item On trace la droite parallèle à l'axe des abscisses, d'ordonnée $1$ (horizontale en pointillés sur la figure).
    \item On repère les points d'intersection de la courbe de $f$ et cette droite (marqués par des $\bullet$ sur la figure), et on lit les abscisses de ces points d'intersection (environ -3,5 ; 1,2 ; 3 ; 4,5).
    \item On repère les parties de la courbe de $f$ qui sont en dessous de cette droite (ces parties sont alors délimitées par les points d'intersection de l'étape précédente).
    \item On lit les abscisses des points de la courbe de $f$ qui sont en dessous de la droite, sous forme d'intervalles, soit : $[-4; -3,5]\cup[1,2;3]\cup[4,5;6]$.
  \end{enumerate}
  Les solutions de $f(x)\leq1$ sont donc environ $[-4; -3,5]\cup[1,2;3]\cup[4,5;6]$.
\end{exemple}

\pagebreak
\subsection{Deux fonctions}
\begin{figure}[!h]
  \begin{center}
  \begin{tikzpicture}[very thick,xscale=.8, yscale=1.2]
    \draw[dotted, lightgray, xstep=1, ystep=.5] (-4, -1) grid (6, 3);
    \draw[-latex] (-4,0) -- (6,0);
    \draw[-latex] (0,-1) -- (0,3);
    \foreach \x in {-4, -3, -2, -1, 1, 2, 3, 4, 5, 6} {
      \draw (\x,0) node[below]{\x};
      \draw (\x,{0.1}) -- (\x,{-0.1});
    }
    \foreach \y in {-1, 1, 2} {
      \draw (0,\y) node[left]{\y};
      \draw (-.1, \y) -- (.1, \y);
    }
    \draw[cyan, name path=F] plot [smooth, tension=0.5] coordinates {
      (-4,-1)
      (-3, 2.5)
      (0, 2.1)
      (2, .5)
      (4, 1.5)
      (5, .5)
      (6, 0)
    };
    \draw[red, name path=G] plot [smooth, tension=0.5] coordinates {
      (-4, 2)
      (0, -.5)
      (3, 2)
      (6, 3)
    };
    \draw (0,0) node[below left]{$O$};
    \draw (4, 1.5) node[right]{$\mathcal{C}_f$};
    \draw (3, 2) node[above]{$\mathcal{C}_g$};

    \coordinate (O) at (0, 0);
    \path[name intersections={of=F and G}];
    \coordinate (I1)  at (intersection-1);
    \coordinate (I2)  at (intersection-2);
    \draw[dashed] (I1) -- (I1 |- O);
    \draw[dashed] (I2) -- (I2 |- O);
    \draw (I1) node{$\bullet$};
    \draw (I2) node{$\bullet$};
  \end{tikzpicture}
  \caption{Deux fonctions.}
  \label{f2}
\end{center}
\end{figure}

\begin{methode}[Résolution graphique de $f(x)=k$]
  Résoudre $f(x)=g(x)$, c'est déterminer les abscisses des points d'intersection de $f$ et $g$.
\end{methode}

\begin{exemple}[Résolution de $f(x)=g(x)$]
  Voir la figure \ref{f2} comme illustration.
  \begin{itemize}
    \item On cherche les points d'intersection des courbes de $f$ et $g$ (marqués par des $\bullet$ sur la figure).
    \item On lit les abscisses de ces points d'intersection (ici environ -3,5 et 1,5).
  \end{itemize}
  Les solutions de $f(x)=g(x)$ sont environ $-3,5$ et $1,5$.
\end{exemple}

\begin{methode}[Résolution graphique de $f(x)\leq g(x)$]
  Résoudre $f(x)\leq g(x)$, c'est déterminer les valeurs de $x$ dont l'image par $f$ est inférieure à $g(x)$.

  \begin{enumerate}
    \item On cherche les points d'intersection des deux courbes, et on lit leurs abscisses.
    \item On regarde entre quels points d'intersection la courbe de $f$ est en dessous de celle de $g$, et on l'exprime sous forme d'intervalles, ici :
      $[-4;-3,5]\cup[1,5;6]$.
  \end{enumerate}
  Les solutions de $f(x)\leq g(x)$ sont donc environ $x\in[-4;-3,5]\cup[1,5;6]$.
\end{methode}

\end{document}
