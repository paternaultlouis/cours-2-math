%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2020-2023 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename
%$ lualatex $basename

\documentclass[10pt]{article}

\usepackage{2223-pablo}
\usepackage{2223-pablo-paternault}
\usepackage{2223-pablo-math}

\usepackage[
  a5paper,
  margin=6mm,
  includehead,
]{geometry}
\usepackage{2223-pablo-header}
\fancyhead[L]{\textsc{Mathématiques 2\up{de}{}}}
\fancyhead[C]{\textsc{DM \no5}}
\fancyhead[R]{01/02/22}

\usepackage{2223-pablo-tikz}
\usetikzlibrary{patterns}

\usepackage{fontawesome}
\usepackage{mdframed}
\usepackage{multicol}

\begin{document}

\begin{mdframed}
  Faites un des deux menus suivants (le menu 1 est le plus court et facile ; le menu 3 est le plus long et difficile).
  \begin{enumerate}[labelwidth=-8mm, label={\emph{Menu \arabic* :}}]
    \item Exercices \ref{ex:libre} et \ref{ex:facile}.
    \item Exercices \ref{ex:libre} et \ref{ex:difficile}, mais sans faire les questions marquées d'une étoile \faStar{} (qui sont plus difficiles, ou plus longues) : admettez le résultat, et passez à la suite.
    \item Exercices \ref{ex:libre} et \ref{ex:difficile}, y compris les questions marquées d'une étoile \faStar{}.
  \end{enumerate}
\end{mdframed}

\begin{exercice}\label{ex:libre}
  Choissez un exercice sur le site web \url{http://pyromaths.org}, imprimez l'énoncé (ou envoyez-le moi par Pronote), résolvez cet exercice, et vérifiez vos résultats avec le corrigé. Rendez l'énoncé avec la copie.

  Faites l'exercice sur votre copie, mais je ne le corrigerai pas (sauf si vous le demandez).

  Exemples d'exercices pour travailler le chapitre en cours, ou des notions vues au collèges qui ont peut-être été un peu oubliées.
  \begin{itemize}
    \item \emph{Classe de troisième} $\rightarrow$ \emph{Identités remarquables} ou \emph{Puissances} ou \emph{Racines carrées} pour travailler les automatismes sur ces sujets.
    \item \emph{Classe de seconde $\rightarrow$ Vecteurs} pour préparer le prochain chapitre sur les vecteurs.
    \item \emph{Classe de troisième $\rightarrow$ Fonctions affines} pour travailler le chapitre en cours.
  \end{itemize}
\end{exercice}

\begin{exercice}\label{ex:facile}
	\begin{em}
	Si vous n'arrivez pas à faire la question 1, admettez le résultat, et faites tout de même les questions 2 et 3.
	\end{em}

	On cherche à résoudre l'inéquation : \[2x^2+18x-13\geq15\]
  \begin{enumerate}
	  \item Montrer que résoudre cette inéquation revient à résoudre l'inéquation : \[(2x-4)(-x+7)\geq0\]
    \item
      \begin{enumerate}
        \item Dresser le tableau de signes des fonctions affines d'expression : $2x-4$ et $-x+7$.
        \item En déduire le tableau de signes de l'expression : $\left( 2x-4 \right)\left( -x+7 \right)$.
      \end{enumerate}
    \item En déduire les solutions de l'inéquation de départ.
  \end{enumerate}
\end{exercice}

  \pagebreak

\begin{exercice}\label{ex:difficile}~
  \emph{Il n'y a aucune question barrage : si vous n'arrivez pas à répondre à une question, admettez le résultat, et passez à la question suivante.}

  L'architecte d'un aquarium réfléchit au plan de l'une des salles, qui aura la forme ci-contre. C'est un rectangle de \SI{30}{m} par \SI{50}{m}, composée de quatre bassins rectangulaires aux quatre coins de la salle (en bleu sur la figure); les deux bandes blanches sont les allées pour les visiteurs.

  \begin{multicols}{2}
    L'architecte a les contraintes suivantes :
    \begin{itemize}
      \item il faut que la superficie de l'ensemble des bassins soit supérieure à \SI{800}{m^2} ;
      \item il faut que la superficie des allées soit supérieure à \SI{375}{m^2} ;
    \end{itemize}
    On appelle $x$ (compris dans l'intervalle $\intervallef{0;30}$) la largeur des allées, en mètres. L'objet de l'exercice est de déterminer quelles sont les valeurs possibles pour $x$ pour que les deux contraintes soient respectées.

    \columnbreak

    \begin{center}
      \begin{tikzpicture}[scale=.105, very thick]
        \draw (0, 0) rectangle (50, 30);
        \begin{scope}
          \draw[pattern=north west lines, pattern color=blue] (0, 0) -- (0, 15) -- ++(25, 0) -- ++(0, -15) -- cycle;
          \draw[pattern=north west lines, pattern color=blue] (35, 0) -- ++(0, 15) -- (50, 15) -- (50, 0) -- cycle;
          \draw[pattern=north west lines, pattern color=blue] (0, 25) -- ++(25, 0) -- (25, 30) -- (0, 30) -- cycle;
          \draw[pattern=north west lines, pattern color=blue] (35, 30) -- (50, 30) -- (50, 25) -- (35, 25) -- cycle;
        \end{scope}
        \draw[latex-latex] (-3, 15) -- ++(0, 10) node[midway, left]{$x$};
        \draw[latex-latex] (25, -3) -- ++(10, 0) node[midway, below]{$x$};
        \draw[latex-latex] (0, 33) -- ++(50, 0) node[midway, above]{\SI{50}{m}};
        \draw[latex-latex] (53, 0) -- ++(0, 30) node[midway, below, rotate=90]{\SI{30}{m}};
      \end{tikzpicture}
    \end{center}
  \end{multicols}

  On appelle $B$ la fonction qui à $x$ associe l'aire des aquarium, et $A$ la fonction qui à $x$ associe l'aire des allées.

  \begin{enumerate}
    \item 
      \begin{enumerate}
        \item Calculer l'aire totale de la salle.
        \item Expliquer pourquoi $x\in\intervallef{0;30}$.
        \item\faStar{} Montrer que pour tout $x\in\intervallef{0;30}$, on a $A(x)=-x^2+80x$.
        \item En déduire que pour tout $x\in\intervallef{0;30}$, on a $B(x)=x^2-80x+1500$.
      \end{enumerate}
    \item On s'intéresse à l'aire des bassins.
      \begin{enumerate}
        \item Expliquer pourquoi $B(x)\geq800$.
        \item Montrer que résoudre $B(x)\geq800$ est équivalent à résoudre : $x^2-80x+700\geq0$.
        \item Montrer que résoudre $B(x)\geq800$ revient à résoudre : $\left( x-10 \right)\left( x-70 \right)\geq0$.
        \item Dresser le tableau de signes du produit $\left( x-10 \right)\left( x-70 \right)$, puis en déduire que $B(x)\geq800$ si $x\leq10$.
      \end{enumerate}
    \item\label{q:allees}\faStar{} On s'intéresse à l'aire des allées.
      \begin{enumerate}
        \item Expliquer pourquoi $A(x)\geq375$, puis montrer que résoudre $A(x)\geq375$ revient à résoudre : $\left( x-5 \right)\left( x-75 \right)\leq0$.
        \item Dresser le tableau de signe de $\left( x-5 \right)\left( x-75 \right)$, puis en déduire les solutions de l'inéquation $A(x)\geq375$.
      \end{enumerate}
  \end{enumerate}
  Si vous n'avez pas fait la question \ref{q:allees}, admettez que pour que l'aire des allées soit supérieure à \SI{375}{m^2}, il faut que $x\geq5$.
  \begin{enumerate}[resume]
    \item Conclure : Quelles valeurs l'architecte peut-elle prendre pour la largeur des allées pour que les contraintes soient respectées ?
  \end{enumerate}
\end{exercice}

\end{document}
