from random import random

def assemblée_hasard():
    femmes = 0
    for i in range(576):
        if random() < .48:
            femmes = femmes + 1
    return femmes/576

def plusieurs_echantillons(N):
    plus_petits = 0
    for i in range(N):
        if assemblée_hasard() < 237/576:
            plus_petits = plus_petits + 1
    return plus_petits / N


print(assemblée_hasard())
print(plusieurs_echantillons(10000))
