# Colinéarité

Introduction :  activite-produit-par-un-reel (c'est un prétexte pour re-manipuler des vecteurs)

## Produit d'un vecteur par un réel

- Cours (sens, direction, norme) ; vecteur nul
- Exercices 42, 45 p. 209
- Cours : Coordonnées
- Exemple
- Exercices : 23 p. 207, 50, 51 p. 209

## Colinéarité

- Intro : activite-colinearite.pdf
- Cours : cours-colinearite.pdf
- Exo : 19, 24 p. 207
- Cours : Déterminant, critère de colinéarité, Transitivité
- Exemple : deux vecteurs u et v colinéaires ?
- Exercice : 26 p. 207
- Exercice (difficile) : 73 p. 211

## Applications

- Cours :
  - Alignement, parallélisme
  - Exemple : A(-10;14) ; B(11;1) et C(45;-20) sont-ils alignés ? Conjecture + Preuve.
  - Milieu (rappel)
- Exercices : 29 à 32 p. 207 ; 69 p. 211

## Algorithmique

TP : algo.pdf

## Exercices

exercices-bilan.pdf
