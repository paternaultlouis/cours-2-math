%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2023 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{2223-pablo}
\usepackage{2223-pablo-paternault}
\usepackage{2223-pablo-math}
\usepackage{2223-pablo-tikz}

\usepackage[
  a5paper,
  margin=10mm,
  headsep=3mm,
  includehead,
]{geometry}
\usepackage{2223-pablo-header}
\fancyhead[L]{\textsc{Math 2\up{de}{}}}
\fancyhead[R]{\textsc{DS \no7 --- Sujet blanc --- Corrigé}}
\fancyfoot[C]{}

\begin{document}

\begin{em}
  Aucune démonstration par lecture graphique ne sera acceptée !
\end{em}

\begin{exercice}
  \begin{em}
    Dans le plan muni d'un repère, on considère les points $A\coord{2}{-2}$, $B\coord{0}{-1}$ et $C\coord{1}{1}$. On définit le point $D$ par la relation :
    \[\vecteur{AD}=\vecteur{AC}-2\vecteur{BA}\]
  \end{em}

  \begin{enumerate}
    \item \emph{Sur une figure, placer les points $A$, $B$, $C$, puis le point $D$ en respectant la relation.}
      \begin{center}
        \begin{tikzpicture}[scale=.5,very thick]
          \coordinate (A) at (2, -2);
          \coordinate (B) at (0, -1);
          \coordinate (C) at (1, 1);
          \coordinate (D) at ($(C)-2*(A)+2*(B)$);
          \begin{scope}
            \clip (-3.5, -2.5) rectangle (2.5, 3.5);
            \draw[dotted, gray] (-3.5, -2.5) grid (2.5, 3.5);
            \draw[-latex] (-3, 0) -- (2, 0);
            \draw[-latex] (0, -2) -- (0, 3);
            \draw (0, 0) node[below left]{$\mathcal{O}$};
            \draw (1, 0) node[below]{1};
            \draw (0, 1) node[left]{1};
          \end{scope}
          \draw (A) node[below]{$A$}
          -- (B) node[left]{$B$}
          -- (C) node[above]{$C$}
          -- cycle;
          \draw[red, -latex] (A) -- (C) node[midway, right]{$\vecteur{AC}$};
          \draw[red, -latex] (C) -- ++($(B)-(A)$) node[midway, above right]{$-\vecteur{BA}$};
          \draw[red, -latex] ($(C)+(B)-(A)$) -- ++($(B)-(A)$) node[midway, above right]{$-\vecteur{BA}$};
          \draw (D) node[above left]{$D$};
        \end{tikzpicture}
      \end{center}
    \item \begin{enumerate}
        \item \emph{Calculer les coordonnées de $\vecteur{AC}$ et $\vecteur{BA}$, puis montrer que les coordonnées de $\vecteur{AC}-2\vecteur{BA}$ sont $\coord{-5}{5}$.}

          \begin{align*}
            \vecteur{AC}=\coord{x_C-x_A}{y_C-y_A}=\coord{1-2}{1-(-2)}=\coord{-1}{3}\\
            \vecteur{BA}=\coord{x_A-x_B}{y_A-y_B}=\coord{2-0}{-2-(-1)}=\coord{2}{-1}\\
          \end{align*}

          Donc les coordonnées de $\vecteur{AC}-2\vecteur{BA}$ sont :
          \begin{align*}
            \left(\vecteur{AC}-2\vecteur{BA}\right)
            =\coord{x_{\vecteur{AC}}-2\times x_{\vecteur{BA}}}{y_{\vecteur{AC}}-2\times y_{\vecteur{BA}}}
            =\coord{-1-2\times2}{3-2\times(-1)}
            =\coord{-5}{5}
          \end{align*}
        \item \emph{En déduire les coordonnées du point $D$.}
          Notons $D\coord{x}{y}$ les coordonnées de $D$. Alors, d'une part :
          \begin{align*}
            \vecteur{AD}\coord{x_D-x_A}{y_D-y_A}=\coord{x-2}{y-(-2)}=\coord{x-2}{y+2}
          \end{align*}
          Et d'autre part, on sait  que $\vecteur{AD}=\vecteur{AC}-2\vecteur{BA}$, donc que $\vecteur{AD}\coord{-5}{5}$.

          Donc $\coord{x-2}{y+2}=\coord{-5}{5}$, et :
          \[\begin{array}{rclp{1cm}rcl}
            x-2&=&-5 && y+2&=&5\\
            x&=&-5+2&&y&=&5-2\\
            x&=&-3&&y&=&3\\
        \end{array}\]
          Donc les coordonnées de $D$ sont $D\coord{-3}{3}$.
      \end{enumerate}
    \item \emph{Montrer que les droites $(AB)$ et $(CD)$ sont parallèles. Quelle est la nature du quadrilatère $ABCD$ ?}

      On a :
      \begin{align*}
        \vecteur{AB}\coord{0-2}{-1-(-2)}=\coord{-2}{1}\\
        \vecteur{CD}\coord{-3-1}{3-1}=\coord{-4}{2}
      \end{align*}
      Donc :
      \[
        \det\left( \vecteur{AB};\vecteur{CD} \right)=\begin{vmatrix}-2 & -4\\1&2\end{vmatrix}=-2\times2-(-4)\times1=-4+4=0
      \]
      Donc le déterminant est nul, et $\vecteur{AB}$ et $\vecteur{CD}$ sont colinéaires, et les droites $\left( AB \right)$ et $\left( CD \right)$ sont parallèles.
  \end{enumerate}
\end{exercice}

\begin{exercice}
  \begin{em}
    On considère les points $A\coord{-2}{0}$ et $B\coord{3}{4}$, et on cherche les coordonnées d'un point $C$ tel que :
    \begin{enumerate*}[(1)]
      \item son abscisse et son ordonnée soient égales ;
      \item les points $A$, $B$, $C$ soient alignés.
    \end{enumerate*}

    On nomme $C\coord{x}{x}$ les coordonnées de $C$.
  \end{em}

  \begin{enumerate}
    \item \emph{Montrer que les coordonnées de $\vecteur{AB}$ sont $\vecteur{AB}\coord{5}{4}$.}
      \begin{align*}
        \vecteur{AB}\coord{3-(-2)}{4-0}=\coord{5}{4}\\
      \end{align*}
  \end{enumerate}
  \begin{em}
    On admet que les coordonnées de $\vecteur{AC}$ sont $\vecteur{AC}\coord{x+2}{x}$.
  \end{em}
  \begin{enumerate}[resume]
    \item \emph{Montrer que les trois points sont alignés si et seulement si : $x-8=0$.}

      Les trois points sont alignés si et seulement si les vecteurs $\vecteur{AB}$ et $\vecteur{AC}$ sont colinéaires, c'est-à-dire si :
      \begin{align*}
        \det\left( \vecteur{AB};\vecteur{AC} \right)&=0\\
        \begin{vmatrix}5 & x+2\\4&x\end{vmatrix}&=0\\
        5\times x-(x+2)\times4&=0\\
        5x-4x-8&=0\\
        x-8&=0
      \end{align*}
    \item \emph{En déduire la valeur de $x$, puis les coordonnées de $C$.}

      Puisque $x-8=0$, alors $x=8$ et les coordonnées de $C$ sont $C\coord{8}{8}$.
  \end{enumerate}
\end{exercice}

\begin{exercice}
  \begin{em}
    On considère le triangle $ABC$, et $I$ le milieu de $[AC]$. On construit le point $J$ tel que $\vecteur{BJ}=\vecteur{AB}+\vecteur{AI}$.
  \end{em}

  \begin{enumerate}
    \item \emph{Faire une figure.}
      \begin{center}
        \begin{tikzpicture}[scale=.5,very thick]
          \coordinate (A) at (0, 0);
          \coordinate (B) at (3, 1);
          \coordinate (C) at (2, 4);
          \coordinate (I) at ($.5*(C)+.5*(A)$);
          \coordinate (J) at ($(B)+(B)-(A)+(I)-(A)$);
          \draw (A) node[below left ]{$A$}
          -- (B) node[below]{$B$}
          -- (C) node[above]{$C$}
          -- cycle;
          \draw (I) node{$\bullet$} node[above left]{$I$};
          \draw (J) node{$\bullet$} node[above right]{$J$};
          \draw[red, -latex] (B) -- ++($(B)-(A)$) node[midway, below]{$\vecteur{AB}$};
          \draw[red, -latex] ($(B)+(B)-(A)$) -- ++($(I)-(A)$) node[midway, right]{$\vecteur{AI}$};
        \end{tikzpicture}
      \end{center}
  \end{enumerate}
  \begin{em}
    On remarque qu'en appliquant la relation de Chasles, on obtient :
    \[\vecteur{IJ}=\vecteur{IA}+\vecteur{AB}+\vecteur{BJ}\]
  \end{em}
  \begin{enumerate}[resume]
    \item \emph{Montrer que $\vecteur{IJ}=2\vecteur{AB}$.}
      \begin{align*}
        \vecteur{IJ} &= \vecteur{IA} + \vecteur{AB} + \vecteur{BJ}\\
        &= \vecteur{IA} + \vecteur{AB} + \vecteur{AB} + \vecteur{AI}\\
        &= \vecteur{IA} + 2\vecteur{AB}  - \vecteur{IA}\\
        &=  2\vecteur{AB}  \\
      \end{align*}
    \item \emph{Que peut-on dire des droites $(IJ)$ et $(AB)$ ? Justifier.}

      Puisque $\vecteur{IJ}=2\vecteur{AB}$, alors les vecteurs $\vecteur{IJ}$ et $\vecteur{AB}$ sont colinéaires, et les droites $\left( IJ \right)$ et $\left( AB \right)$ sont parallèles.
  \end{enumerate}
\end{exercice}
\end{document}
