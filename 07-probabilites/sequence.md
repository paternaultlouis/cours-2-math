# Probabilités

Introduction : probleme-plusieurs-experiences.pdf

## Définitions

- Cours 1
- Exercices : 17, 21, 22 p. 309

## Évènements

- Cours : cours2.pdf (sauf arbres)
- Exercices : 27, 54, 57
- Cours : arbres
- Exercices : 55, 56

## Union et Intersection

- Cours : cours3.pdf
- Exercice : 68 (en ajoutant des questions avec des lettres).
- Cours : Contraire
  Exemple : On lance un dé faussé avec les proba : 0, 0,1, 0,1, ? ? ?. Calculer P(« Obtenir 3 ou plus »).
- Cours : Union et Intersection
  Exemple : On lance un dé à 100 faces. Calculer P(« Obtenir un nombre pair ou multiple de 5 »).
- Exercices : 29, 30, 31.
- Exercices : 72 (traduction français - opérations sur les évènements) ; 78, 79.
