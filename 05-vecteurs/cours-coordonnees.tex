%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2019-2022 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[11pt]{article}

\usepackage{2122-pablo}
\usepackage{2122-pablo-paternault}
\usepackage{2122-pablo-math}
\usepackage{2122-pablo-tikz}

\usepackage[
  a5paper, margin=5mm
]{geometry}

\usepackage{multicol}
\usepackage{emoji}

\begin{document}

\begin{multicols}{2}

  \begin{definition*}
    Le plan est muni d'un repère $(O, I, J)$ quelconque. Pour tout vecteur $\vecteur u$ du plan, il existe un unique point $M(x,y)$ tel que \emph{$\vecteur{u}=\vecteur{OM}$}.

    Ce couple est appelé \emph{coordonnées de $\vecteur{u}$}, et on note
    \emph{$\vecteur{u}(x;y)$} ou
    \emph{$\vecteur{u}\coord{x}{y}$}.
  \end{definition*}

  \columnbreak

  \begin{center}
    \begin{tikzpicture}[scale=0.6,very thick]
      \draw[dotted, gray] (0, 0) grid (8, 4);
      \draw[-latex] (0, 0) -- (8, 0);
      \draw[-latex] (0, 0) -- (0, 4);
      \draw (0, 0) node[below left]{$\mathcal{O}$};
      \draw (1, 0) node[below]{1};
      \draw (0, 1) node[left]{1};

      \draw[-latex] (1, 1) -- (4, 2) node[midway, above]{$\vecteur{u}$};
      \draw[-latex] (7, 1) -- (3, 4) node[midway, above]{$\vecteur{v}$};
    \end{tikzpicture}
  \end{center}
\end{multicols}

\begin{exemple}~
	\begin{enumerate}
		\item Lire les coordonnées des vecteurs $\vecteur{u}$ et $\vecteur{v}$ dans le repère ci-dessus.
		\item Tracer un représentant des vecteurs $\vecteur{a}(2;-1)$ et $\vecteur{b}(0;1)$.
	\end{enumerate}
\end{exemple}

\begin{propriete*}
  Soient $A(x_A,y_A)$ et $B(x_B,y_B)$ deux points du plan. Alors les coordonnées du vecteur $\vecteur{AB}$ sont \emph{$\coord{x_B-x_A}{y_B-y_A}$}.
\end{propriete*}

\begin{exemple}
  \begin{multicols}{2}
    Dans un repère, on donne $A\left( 1;1 \right)$, $B\left( 3;2 \right)$, $C\left( 2;0 \right)$, et $D\left( -3;-1 \right)$. Sans lecture graphique, déterminer les coordonnées des vecteurs $\vecteur{AB}$ et $\vecteur{CD}$, puis vérifiez graphiquement votre calcul.

    \begin{center}
      \begin{tikzpicture}[scale=0.6,very thick]
        \draw[dotted, gray] (-4, -2) grid (4, 2);
        \draw[-latex] (-4, 0) -- (4, 0);
        \draw[-latex] (0, -2) -- (0, 2);
        \draw (0, 0) node[below left]{$\mathcal{O}$};
        \draw (1, 0) node[below]{1};
        \draw (0, 1) node[left]{1};

        \draw (1, 1) node{$\bullet$} node[above left]{$A$};
        \draw (3, 2) node{$\bullet$} node[below right]{$B$};
        \draw (2, 0) node{$\bullet$} node[below]{$C$};
        \draw (-3, -1) node{$\bullet$} node[below left]{$D$};
      \end{tikzpicture}
    \end{center}
  \end{multicols}

  %\begin{align*}
  %    &\vecteur{AB}\coord{x_B-x_A}{y_B-y_A}
  %    =\coord{3-1}{2-1}
  %    =\coord{2}{1}\\
  %    &\vecteur{CD}\coord{x_D-x_C}{y_D-y_C}
  %    =\coord{-3-2}{-1-0}
  %    =\coord{-5}{-1}
  %\end{align*}
\end{exemple}

\begin{exemple}[\emoji{heart}]
  Dans le plan muni d'un repère, on considère les trois points $A\left( 2;7 \right)$, $B\left( 8;3 \right)$, $C\left( 5;-2 \right)$. Déterminer les coordonnées du point $D$ tel que $ABCD$ soit un parallélogramme.
\end{exemple}

\begin{propriete*}
  Soient $\vecteur{u}\coord{x}{y}$ et $\vecteur{v}\coord{x'}{y'}$ deux vecteurs. Les coordonnées de la somme $\vecteur{u}+\vecteur{v}$ sont :
  \[\blanc{\coord{x+x'}{y+y'}}\]
\end{propriete*}

\begin{exemple}
  On donne $\vecteur{u}\coord{2}{7}$ et $\vecteur{v}\coord{-1}{8}$. Quelles sont les coordonnées de $\vecteur{w}=\vecteur{u}+\vecteur{v}$ ?
\end{exemple}

\begin{propriete*}
  Soit $\vecteur{u}\coord{x}{y}$ un vecteur, et $k$ un nombre réel. Les coordonnées du vecteur $k\vecteur{u}$ sont :
  \[\blanc{\coord{kx}{ky}}\]
\end{propriete*}

\begin{exemple}
  On donne $\vecteur{u}\coord{2}{-1}$ et $\vecteur{v}\coord{-6}{0}$. Déterminer les coordonnées de $\vecteur{a}=2,2\vecteur{u}$ et $\vecteur{b}=-5\vecteur{v}$.
\end{exemple}

\begin{propriete*}
  La norme du vecteur $\vecteur{u}\coord{x}{y}$ est :

  \[\norme{\vecteur{u}}=\blanc{\sqrt{x^2+y^2}}\]
\end{propriete*}

\begin{exemple}
  Lequel des deux vecteurs $\vecteur{u}\coord{4}{-3}$ et $\vecteur{v}\coord{-2}{2}$ a la plus grande norme ?
\end{exemple}

\end{document}
