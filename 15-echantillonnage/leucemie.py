from random import random

"""
PROBA_MALADE = 0.00052
EFFECTIF_TOTAL = 5969
EFFECTIF_OBSERVE = 9
NOMBRE_SIMULATIONS = 10000

depasse = 0
for i in range(NOMBRE_SIMULATIONS):
    malades = 0
    for j in range(EFFECTIF_TOTAL):
        if random() < PROBA_MALADE:
            malades += 1
    if malades >= EFFECTIF_OBSERVE:
        depasse += 1
print(depasse)
"""

def echantillon(n):
    blanches = 0
    for i in range(n):
        if random() < .4:
            blanches = blanches + 1
    return blanches/n

def fluctuation(répétitions, seuil):
    éloigné = 0
    for i in range(répétitions):
        if abs(.4-echantillon(1000)) > seuil:
            éloigné = éloigné + 1
    return éloigné / répétitions
            
print(fluctuation(1000, .01))