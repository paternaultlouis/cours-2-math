from turtle import *

def cercle(x, y, rayon):
    up()
    goto(x, y-rayon)
    setheading(0)
    down()
    circle(rayon)

cercle(0, 0, 50)
cercle(0, 100, 50)

mainloop()
