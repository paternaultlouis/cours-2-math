Repérage

Je ne sais pas si les questions étaient mal formulées, ou le DM pas donné au mauvais moment, mais je ne suis pas satisfait du résultat : beaucoup d'élèves ont contourné la difficulté, soit en faisant des lectures graphiques, soit en utilisant la formule de la distance, qu'ils ont vu en cours juste avant le rendu du DM.
