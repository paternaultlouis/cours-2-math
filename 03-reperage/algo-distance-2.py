from math import *

def distance(A, B):
    return sqrt((A[0]-B[0])**2 + (A[1]-B[1])**2)

A = (3, 2)
B = (7, 5)
C = (3, -1)

print(distance(A, B))
print(distance(B, C))
