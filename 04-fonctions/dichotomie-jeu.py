import random
import itertools

def lit_nombre():
    while True:
        try:
            return int(input("Propose un nombre : "))
        except ValueError:
            print("Nombre invalide...")

cible = random.randint(1, 100)

print("Devine le nombre compris entre 1 et 100.")

for compteur in itertools.count(1):
    essai = lit_nombre()
    if essai < cible:
        print("Trop bas...")
    elif essai > cible:
        print("Trop haut...")
    else:
        print("Gagné en {} coups.".format(compteur))
        break
