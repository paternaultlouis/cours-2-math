# Repérage

## Coordonnées

- Intro (activité fiche)
- Cours : Repère

### Milieu d'un segment

- Activtié d'intro
- Cours
- Exercices 26 p. 159

### Distance entre deux points

- Activité d'intro
- Cours
- Exercice 47 p. 163

## Configurations

### Figures (1h)

- Cours : Quadrilatère : Fiche
- Cours : Triangles et quadrilatères : rappels pages 336 et 337 (avec exercices)
- Cours : I milieu de [AB] <=> A est le symétrique de B par rapport à I.

- Exercice type : 41 p. 162

### Projeté orthogonal (1h)

- Cours :
  - Définition : Point de la droite tel que perpendiculaire
  - Propriété : Point le plus proche sur la droite

## Trigonométrie (2h)

- Intro : Fiche
- Cours :
  - Définitions :
  - Propriété : cos²x+sin²x=1

- Exercice 64 p. 165

## Algorithmique (1h)

Repérage (fiche)
