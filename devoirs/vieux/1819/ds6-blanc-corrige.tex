%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2019 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{1920-pablo}
\usepackage{1920-pablo-devoir}
\usepackage{1920-pablo-paternault}
\usepackage[a5paper, margin=1.5cm]{geometry}

\setlength{\parindent}{0pt}

\title{Fonctions affines}
\date{}
\classe{2\up{de}12}
\dsnum{DS 6 --- Blanc}

\begin{document}

\maketitle

\begin{exercice}\label{ex:location}
	\begin{em}
Une agence de location de voitures propose deux contrats :
\begin{enumerate}[{Contrat} A :]
\item 37€, plus 0,25€ par kilomètre parcouru ;
\item 48€, plus 0,10€ par kilomètre parcouru.
\end{enumerate}
On souhaite savoir lequel des deux contrats est le plus avantageux.

On admet que pour le contrat $A$, le côut de la location en fonction du nombre de kilomètres est modélisé par la fonction $f:x\mapsto 0,25x+37$, et pour le contrat $B$ par la fonction $g:x\mapsto 0,10x+48$.
	\end{em}

\begin{enumerate}
\item \emph{Résolution graphique}
\begin{enumerate}
	\item \emph{Tracer les courbes des fonctions $f$ et $g$ sur le graphique ci-dessous.}
		Pour tracer la courbe d'une fonction affine, on calcule les coordonnées de deux points (quelconques, mais distincts), et on trace la droite passant par ces deux points.
		\begin{description}
			\item[Fonction $f$ :] Prenons par exemple $x=0$. Alors $f(0)=0,25\times0+37=37$. Donc le point $A(0;37)$ est un point de la courbe. De même, pour $x=80$, $f(80)=0,25\times80+37=57$ donc le point $B(80;57)$ est un point de la courbe.
			\item[Fonction $g$ :] Prenons par exemple $x=0$. Alors $g(0)=0,10\times0+48=48$. Donc le point $C(0;48)$ est un point de la courbe. De même, pour $x=100$, $g(100)=0,10\times100+48=58$ donc le point $D(100;58)$ est un point de la courbe.
		\end{description}
		Nous pouvons maintenant tracer les deux courbes (figure \ref{fig:droites}, page \pageref{fig:droites}).
		\begin{figure}\begin{center}
\begin{tikzpicture}[scale=.1, ultra thick]
\draw[step=5, dotted, gray] (0, 0) grid (100, 70);
\draw[->] (0, 0) -- (100, 0);
\draw[->] (0, 0) -- (0, 70);
\draw (0, 0) node[below left]{$\mathcal{O}$};
\foreach \i in {10, 20, ..., 100} {
\draw (\i, 0) node[below]{\i};
}
\foreach \i in {10, 20, ..., 70} {
\draw (0, \i) node[left]{\i};
}
	\coordinate (O) at (0,0);
	\coordinate (A) at (0,37);
	\coordinate (B) at (80,57);
	\coordinate (C) at (0,48);
	\coordinate (D) at (100,58);
	\coordinate (E) at ({11/0.15}, {.1*11/.15+48});
	\draw (A) node{$\bullet$} node[below right]{$A$};
	\draw (B) node{$\bullet$} node[above]{$B$};
	\draw (C) node{$\bullet$} node[above right]{$C$};
	\draw (D) node{$\bullet$} node[below left]{$D$};
	\draw (E) node{$\bullet$};
	\draw[red,domain=0:100] plot ({\x},{0.25*\x+37});
	\draw[blue,domain=0:100] plot ({\x},{0.10*\x+48});
	\draw[dashed] (E) -- (E|-O);
	\node[rotate=-90] (F) at (74, -6) {$\approx 73$};
\end{tikzpicture}
	\caption{Courbes de l'exercice \ref{ex:location}.}
\label{fig:droites}
		\end{center}\end{figure}
\item \emph{Répondre par lecture graphique : À partir de combien de kilomètres parcourus le contrat B est-il plus avantageux que le contrat A ?}
	Le point d'intersection des deux droites a pour coordonnées environ $(73; 56)$. De plus, on observe que la courbe de la fonction $f$ (contrat A, en rouge) est au dessus de celle de la fonction $g$ (contrat B, en bleu) \emph{après} ce point d'intersection. Donc le contrat A est plus cher après \SI{73}{km}. Donc le contrat B est plus avantageux à partir de \SI{73}{km}.
\end{enumerate}
\item \emph{Résolution algébrique : On souhaite répondre à la même question, mais par le calcul.}
  \begin{enumerate}
	  \item \emph{Montrer que le contrat B est plus avantageux que le contrat A si et seulement si : $0,15x-11\geq0$.}
		  
		  Le contrat B est plus avantageux que le contrat A s'il coûte moins cher, c'est-à-dire si :
		  \begin{align*}
			  f(x) &\geq g(x) \\
			  0,25x+37&\geq 0,10x+48\\
			  0,25x-0,10x+37-48&\geq 0\\
			  0,15x-11&\geq 0
		  \end{align*}
    \item \emph{Dresser le tableau de signes de la fonction $x\mapsto 0,15x-11$.}
    C'est une fonction affine, de coefficient directeur $a=0,15$ positif, et d'ordonnée à l'origine $b=-11$. On a donc $-\frac{b}{a}=-\frac{-11}{0,15}=\frac{220}{3}$. Le tableau de signes est donc :
\begin{center}
	\begin{tikzpicture}
	\tkzTabInit[lgt=3,espcl=3]{$x$/1,{$0,15x-11$/1}}
	{$-\infty$,$\frac{220}{3}$, $+\infty$}
	\tkzTabLine{,-,z,+}
	\end{tikzpicture}
\end{center}
    \item \emph{Conclure en lisant le tableau de signes : À partir de combien de kilomètres parcourus le contrat B est-il plus avantageux que le contrat A ?}
    Aux questions précédentes, nous avons dit que le contrat B est plus avantageux si $0,15x-11\geq0$. Donc d'après le tableau de signes, il est plus avantageux si $x\geq\frac{200}{3}$, c'est-à-dire si le nombre de kilomètres parcourus est supérieur à environ \SI{73}{km}.
  \end{enumerate}
\end{enumerate}
\end{exercice}

\begin{exercice}
	\begin{em}
On considère la fonction $f:x\mapsto 12x-3$, et la fonction $g$ dont on connait le tableau de signes suivant.
	\end{em}

  \begin{center}
    \begin{tikzpicture}
      \tkzTabInit[lgt=2,espcl=2]
      {$x$ /1,
        $g(x)$ /1
      }
      {$-\infty$, -1, $\infty$}%
      \tkzTabLine{, +, z ,-}
    \end{tikzpicture}
  \end{center}

\begin{enumerate}
	\item \emph{Dresser les tableaux de signe et de variations de $f$.}
	C'est une fonction affine qui a pour coefficient directeur $a=12$, positif, et pour ordonnée à l'origine $b=-3$.

	Puisque sont coefficient directeur est strictement positif, la fonction est strictement croissante. Donc son tableau de variations est :

\begin{center}
	\begin{tikzpicture}
	\tkzTabInit[lgt=2,espcl=3]{$x$/1,$f$/1}
	{$-\infty$,$+\infty$}
	\tkzTabVar{-/ , +/}
	\end{tikzpicture}
\end{center}

	D'autre part $-\frac{b}{a}=-\frac{-3}{12}=0,25$, donc son tableau de signes est :

\begin{center}
	\begin{tikzpicture}
	\tkzTabInit[lgt=2,espcl=2]{$x$/1,$f(x)$/1}
	{$-\infty$,{$0,25$}, $+\infty$}
	\tkzTabLine{,-,z,+}
	\end{tikzpicture}
\end{center}
	\item \emph{Compléter en utilisant l'un des quatre signes $<$, $>$, $=$ ou $?$ (s'il manque des informations pour répondre à la question).}
\begin{multicols}{2}
\begin{enumerate}
\item $f(-2)\ldots0$ D'après le tableau de signes de $f$, $f(-2)$ est négatif : $f(-2) < 0$.
\item $g(3)\ldots5$ D'après le tableau de signes de $g$, $g(3)$ est négatif. Il est donc plus petit que 5 : $g(3) < 5$.
\item $f(0)\ldots g(0)$ D'après les tableaux de signes, $f(0)$ est négatif, et $g(0)$ est positif, donc : $f(0) <  g(0)$.
\item $f(7)\ldots g(-1)$ D'après les tableaux de signes, $f(7)>0$, et $g(-1)=0$. Donc : $f(7) >  g(-1)$.
\end{enumerate}
\end{multicols}
\end{enumerate}
\end{exercice}

\begin{exercice}
	\begin{em}
  Voici le tableau de signes d'une fonction $f$.

  \begin{center}
    \begin{tikzpicture}
      \tkzTabInit[lgt=2,espcl=2]
      {$x$ /1,
        $f(x)$ /1
      }
      {-4, -2, 1, 5}%
      \tkzTabLine{, +, z ,- ,z,+}
    \end{tikzpicture}
  \end{center}

  Expliquer pourquoi il est impossible que le tableau suivant soit le tableau de variations de $f$.

  \begin{center}
    \begin{tikzpicture}[xscale=1,yscale=1]
      \tkzTabInit[lgt=1,espcl=2]
      {$x$ /1,
        $f$/1.5
      }
      {-4, -3, 5}%
      \tkzTabVar{+/2, -/, +/3}
    \end{tikzpicture}
  \end{center}
	\end{em}

	Il y a plusieurs incohérences ici. L'une d'entre elles est la suivante.

	D'après le tableau de variations, la fonction est croissante de $-3$ à $5$. Mais d'après le tableau de signes, la fonction est positive en $-3$, puis négative entre $-2$ et $1$, ce qui est impossible pour une fonction croissante. Donc les deux tableaux sont incompatibles.
\end{exercice}

\begin{exercice}
	\begin{em}
  On considère une fonction affine $f$, dont la courbe représentative passe par les points $A(3;7)$ et $B(-1; 12)$.
	\end{em}
  \begin{enumerate}
	  \item \emph{Montrer que l'équation de la fonction $f$ est : $f:x\mapsto -1,25x+10,75$.}
	  Puisque c'est une fonction affine, son expression est de la forme $f(x)=ax+b$, où $a$ et $b$ sont deux nombres à déterminer.

	  Le coefficient directeur $a$ se calcule par la formule $a=\frac{y_B-y_A}{x_B-x_A}=\frac{12-7}{-1-3}=\frac{5}{-4}=-1,25$. Donc l'expression de la fonction est $f(x)=-1,25x+b$. Déterminons la valeur de $b$.

	  Puisque le point $A$ est sur la droite, alors ses coordonnées vérifient l'équation de la fonction, et :
	  \begin{align*}
	  -1,25x_A+b&=y_A\\
	  -1,25\times3+b&=7\\
	  -3,75+b&=7\\
	  b&=7+3,75\\
	  b&=10,75
	  \end{align*}
	  Donc l'expression de la fonction est \fbox{$f(x)=-1,25x+10,75$}.
	  \item \emph{Quelles sont les variations de $f$ ? Justifier.}
	  Puisque le coefficient directeur de la fonction $f$ est $a=-1,25$, strictement négatif, alors la fonction est strictement décroissante.
	  \item \emph{Le point $C(100; -113)$ est-il sur la courbe de $f$ ? Justifier.}
	  Vérifions si les coordonnées de ce point vérifient l'équation de la fonction :
	  \begin{align*}
	  f(x_C)&=f(100)\\
	  &= -1,25\times 100+10,75\\
	  &= -125+10,75\\
	  &=-114,25\\
	  \end{align*}
	  Donc $f(x_C)\neq y_C$, et le point $C$ n'est pas sur la droite de la fonction $f$.
  \end{enumerate}
\end{exercice}

\end{document}
