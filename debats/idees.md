Sources :

- https://www.apmep.fr/IMG/pdf/APMEP_PLOT_19_Leroux_Lecorre.pdf
- https://publimath.univ-irem.fr/numerisation/WR/IWR97066/IWR97066.pdf

Idées :

- [x] Nombre de zones définies par n points reliés entre eux dans un cercle
- [ ] +x% -x% => Nouveau prix ?
- [x] Aller 10km/h ; retour 20km/h. Vitesse moyenne ?
- [x] Deux arêtes d'un cube parallèles ?
- [x] Si un quadrilatère a deux côtés parallèles et deux côtés de même longueur, alors c'est un parallélogramme.
- [x] L'aire et le périmètre d'un rectangle varient dans le même sens.
- [x] Quelle est l'aire de ce parallèlogramme ? (seuls deux côtés mesurés)
- [ ] Sudoku (simplifié : 4x4) sans solutions / avec plusieurs solutions => « je n'y arrive pas » différent de « ce n'est pas possible »
- [x] Une équation de la forme $ax+b=0$ (où $a$ et $b$ sont des nombres réels) a une solution.
- [ ] La somme de deux nombres entiers naturels est un entier naturel.
- [x] Le produit de deux nombres à virgule est un nombre à virgule.
- [ ] Le produit de deux nombres pair est un nombre pair ; le produit de deux nombres impairs est un nombre impair.
- [x] Une fonction est soit paire soit impaire. Une fonction ne peut pas être paire et impaire à la fois.
- [ ] Faites des conjectures.
- [ ] 1/(racine(a)+racine(b))=racine(a)-racine(b)
