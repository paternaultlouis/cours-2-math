# Fonctions affines

## Définition

- Activité d'introduction : activite-intro.tex
- Définition : Fonction affine, coefficient directeur, ordonnée à l'origine
- Propriété : (yB-yA)÷(xB-xA) est constant
- Exemple : Identifier coeff directeur et ordonnée à l'origine
- Exercices : 17 p. 105, 38 p. 107

## Tracé et lecture graphique

- Cours : C'est une droite.
- Cours : Tracé
  - Exercice 18 p. 105
  - Exercice 48 p. 108
- Lecture graphique
  - Avec coef directeur et ordonnée à l'origine
    - Exercice 50, 51 p. 108
  - Avec deux points
    - Exercice 22 p. 105
- Problème : 45 p. 107

## Variations

- Activité : activite-trace-variation.pdf
- Cours : Variations
- Exercices : 25 p. 105
- Algorithmique sur papier : algo-papier.pdf

## Signe

- Activité : activite-tableau-de-signes.pdf
- Cours : Tableau de signe
- Exercice : exercices-signe.pdf
- Exercices : 26 p. 105
- Algorithmique sur papier : algo-papier.pdf

## Signe d'un produit et d'un quotient

- Cours
- Exercices : 73 à 76 p. 111
- Problème : exo-trinome.pdf

## Algorithmique

algo-machine.pdf

## DM

dm6.pdf
